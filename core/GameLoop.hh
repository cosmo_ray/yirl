/*
**Copyright (C) <2013> <YIRL_Team>
**
**This program is free software: you can redistribute it and/or modify
**it under the terms of the GNU Lesser General Public License as published by
**the Free Software Foundation, either version 3 of the License, or
**(at your option) any later version.
**
**This program is distributed in the hope that it will be useful,
**but WITHOUT ANY WARRANTY; without even the implied warranty of
**MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**GNU General Public License for more details.
**
**You should have received a copy of the GNU Lesser General Public License
**along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef	GAMELOOP_HH
#define GAMELOOP_HH

#include	<string>
#include	<vector>
#include	<list>
#include	"../tool/parsing/parser.h"
#include	"Script.hh"
#include	"ILoop.hh"
#include	"MainEntityManager.hh"
#include	"MainEventStack.hh"
#include	"filesDefines.h"
#include	"DLLoader.hpp"

struct	TimeSetting
{
  enum Type{
    TURN_BASE,
    REAL_TIME
  };
  Type type;
  unsigned int val;
  bool redoLastAction;
};

/**
 * Main class of the program
 */
class	GameLoop
{
public:
	struct LibParam
	{
	MainEntityManager* mana;
	GameLoop* gameLoop;
	EntityStructPrototypes* current;
	};
  static GameLoop &getInstance();

  void	loadGame(const std::string &mooduleName);

  /**
   * when set to true stop the loop at his next iteration
   */
  void	 setEndGame(bool);

  bool	readRes();

  int	getResX();
  int	getResY();
  const TimeSetting *getTimeSetting();
  void setTimeType(TimeSetting::Type t);

  /**
   * return the name of the GraphicalLibrary to use
   */
  const std::string &getGraphicalLibrary();
 
  std::vector<Script *> &getScriptManager();
  /**
   * the function that load and start the game(the main loop himself)
   */
  void	start();

  Entity  *callByName(const char *name, int nbArg, va_list *ap);
  /**
   * Call a function
   * @FunctionEntity the function to call
   */
  Entity *call(FunctionEntity *func, va_list *ap);
private:
  /**
   * Constructor
   * Default value:
   *  - _endGame = false
   *  - _mainEventStack = MainEventStack singleton (@see MainEventStack)
   */
 GameLoop();
  ~GameLoop();

  /**
   * Load modules and configurations
   * Load the graphic library defined in configuration files and init it.
   */
  bool  init();
  /**
   * close the libs and free memory use by the Loop
   */
  void	close();

  /**
   * read the conf file for the time setting and init
   * _time
   */
  void	initTimeSetting();  
  /**
   * Pop all the event in _mainEventStack and exucute them
   */
  void  execEvenStack();

  std::vector<Script *>		_scriptManager;
  bool	_endGame;
  ILoop	*_loop;
  std::string	_graphicalLibrary;
  int	_resX;
  int	_resY;
  TimeSetting _time;
  Loader	_guiLoader;
  MainEventStack &_mainEventStack;
};

#endif
