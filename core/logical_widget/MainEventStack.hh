/*
**Copyright (C) <2013> <YIRL_Team>
**
**This program is free software: you can redistribute it and/or modify
**it under the terms of the GNU Lesser General Public License as published by
**the Free Software Foundation, either version 3 of the License, or
**(at your option) any later version.
**
**This program is distributed in the hope that it will be useful,
**but WITHOUT ANY WARRANTY; without even the implied warranty of
**MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**GNU General Public License for more details.
**
**You should have received a copy of the GNU Lesser General Public License
**along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef	MAIN_EVENT_STACK_HH
#define	MAIN_EVENT_STACK_HH

#ifdef LIB
#define WEAK __attribute__((weak))
#else
#define WEAK  //nothing :p
#endif

#include	"EventStack.hh"
/*Main event stack pour les widget du jeu*/
class MainEventStack : public EventStack
{
public:
  static MainEventStack &getInstance();
private:
  MainEventStack();
  virtual ~MainEventStack(){}
  MainEventStack(const MainEventStack &);
  MainEventStack &operator=(const MainEventStack &);

};

#endif
