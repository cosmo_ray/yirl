/*
**Copyright (C) <2013> <YIRL_Team>
**
**This program is free software: you can redistribute it and/or modify
**it under the terms of the GNU Lesser General Public License as published by
**the Free Software Foundation, either version 3 of the License, or
**(at your option) any later version.
**
**This program is distributed in the hope that it will be useful,
**but WITHOUT ANY WARRANTY; without even the implied warranty of
**MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**GNU General Public License for more details.
**
**You should have received a copy of the GNU Lesser General Public License
**along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef	CHECK_JSON_HH
#define	CHECK_JSON_HH

#include	<iostream>
#include	<string>
#include	<json/json.h>
#include	"MainEntityManager.hh"
#include	"CheckJson.hpp"
#include	"JsonImplParser.hh"

/**
 * this namespace regroup the functions use to parse a JSon,
 * save the entitys in a EntityManager and the entity's definitions in an EntityStructPrototypes
 */
namespace LoadJson
{
  /**
   * Parse and save the forward declaration of entity's structures
   */
  int loadFDec(Json::Value &root, EntityStructPrototypes &prototypes);
  /**
   * Parse and save the declaration of entity's structures
   */
  int loadDec(Json::Value &root, EntityStructPrototypes &prototypes);
  // /**
  //  * Parse and save the entitys in an EntityManager
  //  */
  // bool loadImpl(Json::Value &root, EntityStructPrototypes &prototypes, EntityManager &entityManager, bool resolveReferences = true);
  /**
   * call all the backward functions on a file and save entitys in an EntityManager
   * for the moment EntityStructPrototypes is a singloton, so the EntityStructPrototypes use in parameter for the backward function is the one get by the getInstance methode of EntityStructPrototypes class
   */
  bool loadFile(const std::string &fileName, EntityManager &entityManager, int options = 0);
}

#endif
