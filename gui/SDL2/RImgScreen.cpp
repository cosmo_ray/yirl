/*
**Copyright (C) <2013> <YIRL_Team>
**
**This program is free software: you can redistribute it and/or modify
**it under the terms of the GNU Lesser General Public License as published by
**the Free Software Foundation, either version 3 of the License, or
**(at your option) any later version.
**
**This program is distributed in the hope that it will be useful,
**but WITHOUT ANY WARRANTY; without even the implied warranty of
**MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**GNU General Public License for more details.
**
**You should have received a copy of the GNU Lesser General Public License
**along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <cstring>
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include "RImgScreen.hh"
#include "RImgScreenHlp.hpp"

void	RImgScreen::init()
{
  RImgScreenHlp **tmp = ((RImgScreenHlp **)&_helper);
  const char *tmpBg = getStringVal(_background);

  if (tmpBg)
    *tmp = new RImgScreenHlp(*this, tmpBg);
  else
    *tmp = NULL;
}

RImgScreen::~RImgScreen()
{
    delete reinterpret_cast<CWidget *>(_helper);
}

bool RImgScreen::render()
{
  RImgScreenHlp *wid = reinterpret_cast<RImgScreenHlp *>(_helper);
  
  if (!wid)
    return true;
  wid->fillBg(wid->img);
  return true;
}

void RImgScreen::resize()
{
  CWidget *wid = reinterpret_cast<CWidget *>(_helper);

  wid->resize();
}
