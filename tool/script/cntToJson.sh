dir=$1

for file in $(ls $dir/*.cnt)
do
	echo $file
	sed -rnf cntToJson.sed < $file | sed -re ':a; N; ba; s/()\n+([^\}\{])/\1,\n\2/g;' -ne 's/\{,\n/{\n/g  ;p' > $file.json
done
