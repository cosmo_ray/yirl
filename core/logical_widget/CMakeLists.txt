if (MINGW)
file(
  GLOB # Look for all files matching pattern in the directory (not subdirectory)
  srcs_files
  *.cpp
  ../CoreEvent.cpp
  ../Event.cpp)
else()
file(
  GLOB # Look for all files matching pattern in the directory (not subdirectory)
  srcs_files
  *.cpp)
endif()

add_library(
  widget_logical
  SHARED
  ${srcs_files}
)

install ( TARGETS widget_logical DESTINATION ${LIB_INSTALL_DIR} )
