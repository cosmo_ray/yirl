/*
**Copyright (C) <2013> <YIRL_Team>
**
**This program is free software: you can redistribute it and/or modify
**it under the terms of the GNU Lesser General Public License as published by
**the Free Software Foundation, either version 3 of the License, or
**(at your option) any later version.
**
**This program is distributed in the hope that it will be useful,
**but WITHOUT ANY WARRANTY; without even the implied warranty of
**MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**GNU General Public License for more details.
**
**You should have received a copy of the GNU Lesser General Public License
**along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef	CHECKCONFFILE_HH
#define	CHECKCONFFILE_HH

#include	<iostream>
#include	<string>
#include	<list>
#include	<algorithm>
#include	<unordered_map>
#include	"../tool/parsing/parser.h"
#include	"CheckDeclaration.hh"
#include	"CheckImplementation.hh"

#define	CHECK_CNT_BUFFER_SIZE	1024
/*Cette classe etant concue pour les .cnt elle n'est pas censer etre utiliser pour le long terme*/
/**!
** \file CheckConfFile
** \brief Fichier qui permet de charger les .cnt et v�rifie leur syntaxe
** \author YIRLV2TEAM
** namespace use to load .cnt file
**/

/**!
** \namespace checkCnT
**/
namespace	checkCnt
{

  enum	Mode
    {
      DEFINITION,
      INPLEMENTATION
    };

  struct	CheckCntInfo
  {
    CheckCntInfo(const std::string &fileName, EntityManager &entityManager);

    int fileId;
    char buffer[CHECK_CNT_BUFFER_SIZE];
    CheckDec	checkDec;
    CheckImpl	checkImpl;
    Mode	mode;
  };

  /**!
  ** \fn   bool		loadFile(const std::string &fileName, EntityManager &entityManager, int options);
  ** \brief charge le fichier avec le path en filename et place les entite cree dans l'entity manager
  ** \return true si cela reussi et false pas reussi
  **/
  bool		loadFile(const std::string &fileName, EntityManager &entityManager, int options);
  /**!
  ** \fn   void		jmpNextComments(CheckCntInfo &info);
  ** \brief Saute les prochains commentaires pour aller dans une partie declarer 
  ** \return void
  **/
  void		jmpNextComments(CheckCntInfo &info);
  /**!
  ** \fn   void		jmpComment(CheckCntInfo &info);
  ** \brief Saute le prochain commentaire
  ** \return void
  **/
  void		jmpComment(CheckCntInfo &info);
}


#endif
