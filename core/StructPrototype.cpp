/*
**Copyright (C) <2013> <YIRL_Team>
**
**This program is free software: you can redistribute it and/or modify
**it under the terms of the GNU Lesser General Public License as published by
**the Free Software Foundation, either version 3 of the License, or
**(at your option) any later version.
**
**This program is distributed in the hope that it will be useful,
**but WITHOUT ANY WARRANTY; without even the implied warranty of
**MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**GNU General Public License for more details.
**
**You should have received a copy of the GNU Lesser General Public License
**along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include	"debug.h"
#include	"EntityManager.hh"

EntityStructPrototypes::StructPrototype::StructPrototype(const std::string &structName)
  : _structName(structName)
  , _fatherContentLength(0)
{
  _staticMenber = new EntityManager;
}

EntityStructPrototypes::StructPrototype::StructPrototype(const char *structName)
  : _structName(structName)
  , _fatherContentLength(0)
{
  _staticMenber = new EntityManager;
}

EntityStructPrototypes::StructPrototype::~StructPrototype()
{
  // for (std::vector<EntityStructPrototypes::ExtandEntityType *>::iterator it = _content.begin();
  //      it != _content.end();
  //      ++it)
  //   {
  //     delete *it;
  //   }
  delete _staticMenber;
  _content.clear();
}

const EntityManager &EntityStructPrototypes::StructPrototype::getStaticMenber() const
{
  return(*_staticMenber);
}

const std::string &EntityStructPrototypes::StructPrototype::getName() const
{
  return (_structName);
}

#include	<iostream>
const EntityStructPrototypes::ExtandEntityType *EntityStructPrototypes::StructPrototype::getMenber(const std::string &name) const
{
  std::vector<EntityStructPrototypes::ExtandEntityType *>::const_iterator  it = _content.begin();
  const std::vector<EntityStructPrototypes::ExtandEntityType *>::const_iterator  end = _content.end();
  std::size_t found = name.find('.');
  std::string subString;
  std::string structName;
  char	buf[1024];

  LOG_INFO("look for %s\n", name.c_str());
  if (haveFather())
    {
      for (int i = 0; getFather(i); ++i) {
	const EntityStructPrototypes::ExtandEntityType *ret = getFather(i)->getMenber(name);
	if (ret) {
	  return (ret);
	}
      }
    }
  if (found != std::string::npos)
    {
      LOG_INFO("%s\n", (*it)->getContentName().c_str());
      buf[name.copy(buf, name.size() - found - 1, found + 1)] = '\0';
      subString = buf;
      buf[name.copy(buf, found, 0)] = '\0';
      structName = buf;
      while (it != end)
	{
	  if ((*it)->getContentName() == structName)
	    {
	      return ((*it)->getArrayInfo());
	    }
	  ++it;
	}
    }
  else
    {
      while (it != end)
	{
	  LOG_INFO("%s\n", (*it)->getContentName().c_str());
	  if ((*it)->getContentName() == name)
	    {
	      return (*it);
	    }
	  ++it;
	}
    }
  return (NULL);
}

bool EntityStructPrototypes::StructPrototype::haveFather() const
{
  return (_fathers.size());
}

unsigned int	EntityStructPrototypes::StructPrototype::getFatherContentLength() const
{
  return (_fatherContentLength);
}

unsigned int	EntityStructPrototypes::StructPrototype::getContentTotalLength() const
{
  return (getContent().size() + getFatherContentLength());
}

void EntityStructPrototypes::StructPrototype::addFather(EntityStructPrototypes::StructPrototype *father)
{
  _fathers.push_back(father);
  _fatherContentLength += (father->getContentTotalLength());
}

const EntityStructPrototypes::StructPrototype *EntityStructPrototypes::StructPrototype::getFather(unsigned int i) const
{
  if (i >= _fathers.size())
    return (NULL);
  return (_fathers[i]);
}

const EntityStructPrototypes::StructPrototype *EntityStructPrototypes::StructPrototype::getFather(EntityStructPrototypes::StructPrototype *father) const
{
  for (unsigned int i = 0; i < _fathers.size(); ++i)
    {
      if (father == _fathers[i])
	return (_fathers[i]);
    }
  return (NULL);
}

const EntityStructPrototypes::StructPrototype *EntityStructPrototypes::StructPrototype::getFather(const std::string &name) const
{
  for (unsigned int i = 0; i < _fathers.size(); ++i)
    {
      if (name == _fathers[i]->getName())
	return (_fathers[i]);
    }
  return (NULL);
}

template<>
void EntityStructPrototypes::StructPrototype::addMenber<NON_STATIC_MENBER>(EntityType type, const char *name, const char *structName)
{
  DPRINT("add elem: %s of type %s\n", name, entityTypeToString(type));
  _content.push_back(new ExtandEntityType(type, name, false ,structName));
}

template<>
void EntityStructPrototypes::StructPrototype::addMenber<STATIC_MENBER>(EntityType type, const char *name, const char *structName)
{
  DPRINT("add elem: %s of type %s\n", name, entityTypeToString(type));
  _content.push_back(new ExtandEntityType(type, name, true ,structName));
  _staticMenber->addEntity(structName, name);
}

template<>
void EntityStructPrototypes::StructPrototype::addMenber<NON_STATIC_MENBER>(EntityType type, const char *name, ExtandEntityType *arrayInfo)
{
  DPRINT("add elem: %s of type %s\n", name, entityTypeToString(type));
  _content.push_back(new ExtandEntityType(type, name, false, arrayInfo));
}

template<>
void EntityStructPrototypes::StructPrototype::addMenber<STATIC_MENBER>(EntityType type, const char *name, ExtandEntityType *arrayInfo)
{
  DPRINT("add static elem: %s of type %s\n", name, entityTypeToString(type));
  _content.push_back(new ExtandEntityType(type, name, true, arrayInfo));
  _staticMenber->addEntity(entityTypeToString(type), name);
}

template<>
void EntityStructPrototypes::StructPrototype::addMenber<NON_STATIC_MENBER>(EntityType type, const char *name)
{
  DPRINT("add elem: %s of type %s\n", name, entityTypeToString(type));
  _content.push_back(new ExtandEntityType(type, name, false));
}

template<>
void EntityStructPrototypes::StructPrototype::addMenber<STATIC_MENBER>(EntityType type, const char *name)
{
  DPRINT("add static elem: %s of type %s\n", name, entityTypeToString(type));
  _content.push_back(new ExtandEntityType(type, name, true));
  _staticMenber->addEntity(entityTypeToString(type), name);
}

template<>
void EntityStructPrototypes::StructPrototype::addMenber<NON_STATIC_MENBER>(EntityType type, const char *name, FunctionInfo	*functionInfo)
{
  DPRINT("add elem: %s of type %s\n", name, entityTypeToString(type));
  _content.push_back(new ExtandEntityType(type, name, false, functionInfo));
}

template<>
void EntityStructPrototypes::StructPrototype::addMenber<STATIC_MENBER>(EntityType type, const char *name, FunctionInfo	*functionInfo)
{
  DPRINT("add static elem: %s of type %s\n", name, entityTypeToString(type));
  _content.push_back(new ExtandEntityType(type, name, true, functionInfo));
  _staticMenber->addEntity(entityTypeToString(type), name);
}

const std::vector<EntityStructPrototypes::ExtandEntityType *> &EntityStructPrototypes::StructPrototype::getContent() const
{
  return (_content);
}
