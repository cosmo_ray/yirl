/*
**Copyright (C) <2013> <YIRL_Team>
**
**This program is free software: you can redistribute it and/or modify
**it under the terms of the GNU Lesser General Public License as published by
**the Free Software Foundation, either version 3 of the License, or
**(at your option) any later version.
**
**This program is distributed in the hope that it will be useful,
**but WITHOUT ANY WARRANTY; without even the implied warranty of
**MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**GNU General Public License for more details.
**
**You should have received a copy of the GNU Lesser General Public License
**along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef	MAIN_ENTITY_MANAGER_HH
#define	MAIN_ENTITY_MANAGER_HH

#include	<iostream>
#include	<string>
#include	<list>
#include	<algorithm>
#include	<unordered_map>
#include	"EntityManager.hh"
#include	"entity.h"
#include	"../tool/parsing/parser.h"
#include	"EntityStructPrototypes.hh"
#include	"CheckConfFile.hh"

class	MainEntityManager : public EntityManager
{
public:
  static MainEntityManager	&getInstance();
  void				loadFile(std::string fileName);
  

  enum	Mode
    {
      DEFINITION,
      INPLEMENTATION
    };
  MainEntityManager();
  ~MainEntityManager() {}
  //MainEntityManager &operator=(const MainEntityManager &);

private:
  MainEntityManager(const MainEntityManager &);

  int	_file_id;
  // variable who store the "word" use inside the declaration of a struct
  std::string	_structDefDictionary[NBR_ENTITYTYPE];
  std::list<Entity *>	_parentsEntitys;
  CheckDec	*_checkDec;

private:
};

#endif
