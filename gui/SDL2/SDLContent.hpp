/*
**Copyright (C) <2013> <YIRL_Team>
**
**This program is free software: you can redistribute it and/or modify
**it under the terms of the GNU Lesser General Public License as published by
**the Free Software Foundation, either version 3 of the License, or
**(at your option) any later version.
**
**This program is distributed in the hope that it will be useful,
**but WITHOUT ANY WARRANTY; without even the implied warranty of
**MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**GNU General Public License for more details.
**
**You should have received a copy of the GNU Lesser General Public License
**along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <vector>
#include <utility>
#include <iostream>
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include "CWidget.hh"

#define DEFAULT_SIZE 64

class SDLContent : public CWidget
{
public:
  // std::list<std::pair <SDL_Surface */*sprite*/, int/*id*/> > sprites;
  std::vector<SDL_Surface *> sprites;
  
public:
  SDLContent(const Widget &wid)
    :CWidget(wid)
  {
    nbSprite = DEFAULT_SIZE;
    sprites.resize (nbSprite, NULL);
    // getting the window from the SDL_Global from CWidget.hh
  }

  ~SDLContent()
  {
    for (std::vector<SDL_Surface*>::iterator it = this->sprites.begin(); it != this->sprites.end(); ++it)
      SDL_FreeSurface(*it);
  }

  void addSprite(const std::string &path, int id)
  {
    if (id >= nbSprite) {
      sprites.resize (id + 1, NULL);
      nbSprite = id + 1;
    }
    if (sprites[id] != NULL)
      SDL_FreeSurface(sprites[id]);      
    sprites[id] = IMG_Load(path.c_str());
  }

  SDL_Surface *getSprite(int id)
  {
    if (id >= nbSprite)
      return NULL;
    return sprites[id];
  }

private:
  unsigned int nbSprite;
};
