/*
**Copyright (C) <2013> <YIRL_Team>
**
**This program is free software: you can redistribute it and/or modify
**it under the terms of the GNU Lesser General Public License as published by
**the Free Software Foundation, either version 3 of the License, or
**(at your option) any later version.
**
**This program is distributed in the hope that it will be useful,
**but WITHOUT ANY WARRANTY; without even the implied warranty of
**MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**GNU General Public License for more details.
**
**You should have received a copy of the GNU Lesser General Public License
**along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include	<iostream>
#include	<algorithm>  
#include	<unistd.h>
#include	<SDL2/SDL.h>
#include	<SDL2/SDL_ttf.h>
#include	<SDL2/SDL_image.h>
#include	"Gui.hh"
#include	"debug.h"
#include	"yirlAPI.hh"
#include	"keyHandeler.hh"
#include	"CWidget.hh"
#include	"GameLoop.hh"

SDL_Global sg;

namespace {
  int	getEvent(SDL_Event* event, const TimeSetting *ts)
  {
    if (ts->type == TimeSetting::Type::REAL_TIME)
      return SDL_PollEvent(event);
    return SDL_WaitEvent(event);
  }
}

SDL_Global &getSg()
{
  return (sg);
}

void	Gui::init()
{
  printf("\ninit sdl\n");
  /* Initialisation simple */
  if (SDL_Init(SDL_INIT_EVERYTHING) != 0 )
    {
      DPRINT_ERR("Échec de l'initialisation de la SDL (%s)\n",SDL_GetError());
      return ;
    }
  if(TTF_Init()==-1) {
    DPRINT_ERR("TTF_Init: %s\n", TTF_GetError());
    return ;
  }
  /* Création de la fenêtre */
  sg.pWindow = SDL_CreateWindow("YIRL isn't a rogue like", SDL_WINDOWPOS_UNDEFINED,
				SDL_WINDOWPOS_UNDEFINED,
				getResX(),
				getResY(),
				SDL_WINDOW_SHOWN);
  if( ! sg.pWindow )
    {
      DPRINT_ERR("Erreur de création de la fenêtre: %s\n",SDL_GetError());
    }    
    // load font.ttf at size 16 into font
  sg.font=TTF_OpenFont("./gui/SDL2/font/FOO.ttf", 16);
  if(!sg.font) {
    DPRINT_ERR("TTF_OpenFont: %s\n", TTF_GetError());
    return;
  }
  // Render for the main windows
  sg.renderer = SDL_CreateRenderer(sg.pWindow, -1, SDL_RENDERER_TARGETTEXTURE);
  if (! sg.renderer)
    {
      DPRINT_ERR("Get render from window: %s\n", TTF_GetError());
      return;
    }

    // initializing Flags for PNG Images
    int imgFlags = IMG_INIT_PNG|IMG_INIT_JPG|IMG_INIT_TIF;

    // Simple check of the Flags
    if(!(IMG_Init(imgFlags)&imgFlags))
      std::cerr << "SDL_image could not initialize! SDL_image Error: " << IMG_GetError() << std::endl;

  // fill the window with a black rectangle
  // SDL_Rect   rect = sg.getRect();

  SDL_RenderClear(sg.renderer);
  SDL_RenderPresent(sg.renderer);
}


void	Gui::destroy()
{
  TTF_Quit();
  SDL_DestroyWindow(sg.pWindow);
  SDL_Quit();
}

/**
 * Permet de récupérer les events de la SDL et les convertis en Event pour Yirl
 */
int	Gui::getTouch(const TimeSetting *ts)
{
  SDL_Event event;

  if (ts->type == TimeSetting::Type::REAL_TIME) {
    usleep(ts->val);
  }
  while (getEvent(&event, ts)) // Récupération des actions de l'utilisateur
    {
      switch(event.type)
      {
        case SDL_QUIT: // Clic sur la croix
          return 'q';
        case SDL_KEYUP: // Relâchement d'une touche
          if ( event.key.keysym.sym == SDLK_f ) { // Touche f 
            // Alterne du mode plein écran au mode fenêtré
            if ( sg.fullscreen == 0 ) {
              sg.fullscreen = 1;
              SDL_SetWindowFullscreen(sg.pWindow,SDL_WINDOW_FULLSCREEN);
            } else if (sg.fullscreen == 1 ) {
              sg.fullscreen = 0;
              SDL_SetWindowFullscreen(sg.pWindow,0);
            }
          } else {
            return (convertToYKEY(event.key.keysym.sym));
          }
          break;
      }
    }
  return 0;
}

