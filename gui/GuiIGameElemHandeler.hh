/*
**Copyright (C) <2013> <YIRL_Team>
**
**This program is free software: you can redistribute it and/or modify
**it under the terms of the GNU Lesser General Public License as published by
**the Free Software Foundation, either version 3 of the License, or
**(at your option) any later version.
**
**This program is distributed in the hope that it will be useful,
**but WITHOUT ANY WARRANTY; without even the implied warranty of
**MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**GNU General Public License for more details.
**
**You should have received a copy of the GNU Lesser General Public License
**along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef	GUIIGAMEELEMHANDELER_HH
#define	GUIIGAMEELEMHANDELER_HH

#include	<list>
#include	"IGui.hh"

/**!
**\class This abstract class contain generic functions to manipulae a lisr a of IGameElem
**/
class	GuiIGameElemHandler
{
public:
/**!
**\brief Ajout un element de jeux a la map
**\param L'element ajouté
**/
  virtual bool	addElem(IGameElem*);
/**!
**\brief Supprime un element de jeux
**\l'element à supprimer
**/
  virtual IGameElem	*suppElem(IGameElem*);

protected:
  std::list<IGameElem *> _elems; /*!< Liste des elements de jeux*/
};

#endif /* GUIIGAMEELEMHANDELER_HH */
