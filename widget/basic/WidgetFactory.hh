/*
**Copyright (C) <2013> <YIRL_Team>
**
**This program is free software: you can redistribute it and/or modify
**it under the terms of the GNU Lesser General Public License as published by
**the Free Software Foundation, either version 3 of the License, or
**(at your option) any later version.
**
**This program is distributed in the hope that it will be useful,
**but WITHOUT ANY WARRANTY; without even the implied warranty of
**MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**GNU General Public License for more details.
**
**You should have received a copy of the GNU Lesser General Public License
**along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef	RENDER_FACTORY_HH
#define	RENDER_FACTORY_HH

#include	"Widget.hh"

#define	INPLEM_GET_ELEM(ELEM)	\
  Widget *get##ELEM(StructEntity *elem) {	\
    return (new R##ELEM(elem));	}		\
  Widget *get##ELEM(StructEntity *elem, unsigned int x, unsigned int y, unsigned int w, unsigned int h) {	\
    return (new R##ELEM(elem, x, y, w, h));	}		\

namespace WidgetFactory
{
}

#endif
