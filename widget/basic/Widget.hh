/*
**Copyright (C) <2013> <YIRL_Team>
**
**This program is free software: you can redistribute it and/or modify
**it under the terms of the GNU Lesser General Public License as published by
**the Free Software Foundation, either version 3 of the License, or
**(at your option) any later version.
**
**This program is distributed in the hope that it will be useful,
**but WITHOUT ANY WARRANTY; without even the implied warranty of
**MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**GNU General Public License for more details.
**
**You should have received a copy of the GNU Lesser General Public License
**along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef		GAME_ELEM_HH_
# define	 GAME_ELEM_HH_

# include	<stdarg.h>
# include	"entity.h"
# include	"MainEventStack.hh"
# include	"GuiEventStack.hh"

/**
 * Curses Binding...
 * i don't know where this is define
 */
#define UP_ARROW 259
#define DOWN_ARROW 258
#define RIGHT_ARROW 261
#define LEFT_ARROW 260
#define SHIFT_TAB 353

/**
 * Widget types
 */
#define MENU 0
#define	IMG_SCREEN 1
#define	TEXT_SCREEN 2
#define	CONTENER 3
#define	TEXT_EDIT 4
#define	MAP_ELEM 5
#define	MAP_CONTENER 6

/** Abstract Widget.
 *
 */
class	Widget
{
public:
  Widget(int type, StructEntity *gameElem);
  Widget(int type, StructEntity *gameElem, unsigned int x, unsigned int y, unsigned int w, unsigned int h);
  virtual ~Widget() {};
  virtual bool render() = 0;
  
  enum InputStatue
    {
      NOTHANDLE,
      NOACTION,
      ACTION
    };
  virtual InputStatue tryHandleInput(int ch) = 0;
  virtual Widget    *tryFindElem(StructEntity *elemEntity);

  bool operator==(StructEntity *gameElem)
  {
    return (getElem() == gameElem);
  }

  /**
   * @return the type of the widget(Contener/Menu...)
   */
  int	getType() const;
  unsigned int getX() const;
  unsigned int getY() const;
  unsigned int getH() const;
  unsigned int getW() const;
  StructEntity *getElem()
  {
    return (_elem);
  }

  void	setX(unsigned int);
  void	setY(unsigned int);
  void	setW(unsigned int);
  void	setH(unsigned int);
  virtual void resize() = 0;

protected:

  void init();
  void finishInit();
  void uninit();
  /** Link handeling.
   * This function is to use to "go to a link"
   */
  void handleEnter(Entity *entery, ...);
  void handleActionLink(Entity *link, va_list *ap);
  void handleDirectCall(Entity *link);
  void handleIndirectCall(Entity *link);
  
  int	_type;
  unsigned int _x;
  unsigned int _y;
  unsigned int _w;
  unsigned int _h;
  // // Max X (not sure to remenber why i use this)
  // unsigned int _mx;
  // // Max Y (not sure to remenber why i use this)
  // unsigned int _my;
  StructEntity *_elem;
  MainEventStack &_mainEventStack;
  GuiEventStack &_guiEventStack;
};

#endif
