/*
**Copyright (C) <2013> <YIRL_Team>
**
**This program is free software: you can redistribute it and/or modify
**it under the terms of the GNU Lesser General Public License as published by
**the Free Software Foundation, either version 3 of the License, or
**(at your option) any later version.
**
**This program is distributed in the hope that it will be useful,
**but WITHOUT ANY WARRANTY; without even the implied warranty of
**MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**GNU General Public License for more details.
**
**You should have received a copy of the GNU Lesser General Public License
**along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef	ELEM_ALOCATOR_HH
#define ELEM_ALOCATOR_HH

#include	<vector>
#include	"WidgetFactory.hh"
#include	"Widget.hh"

// NBR WIGET_EXACTELLY
#define	NBR_RENDER 7
/**!
**\class Alloue de la memoire pour les widgets
**/
class	ElemAlocator
{
public:
  static ElemAlocator	&getInstance();
/**!
**\brief Allout une Widget pour un gameElem
**\param Le game Elemen a allouer
**\return La 
**/
  Widget *alloc(StructEntity *gameElem);
  /**!
**\brief Allout une Widget pour un gameElem
**\param Le game Elemen a allouer et sa position
**\return La 
**/
  Widget *alloc(StructEntity *gameElem, unsigned int x, unsigned int y, unsigned int w, unsigned int h);

private:
  std::vector<Widget *(*)(StructEntity *)> _renderTab;
  std::vector<Widget *(*)(StructEntity *, unsigned int, unsigned int, unsigned int, unsigned int)> _renderTabWithParam;
  const char *_renderAssociationTable[NBR_RENDER] = {
    "Menu",
    "ImgScreen",
    "TextScreen",
    "Contener",
    "TextEdit",
    "Map",
    "MapContener"
  };

  ElemAlocator();
  ~ElemAlocator();
  ElemAlocator(const ElemAlocator &);
  ElemAlocator &operator=(const ElemAlocator &);
};

#endif
