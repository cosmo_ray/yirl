/*
**Copyright (C) <2013> <YIRL_Team>
**
**This program is free software: you can redistribute it and/or modify
**it under the terms of the GNU Lesser General Public License as published by
**the Free Software Foundation, either version 3 of the License, or
**(at your option) any later version.
**
**This program is distributed in the hope that it will be useful,
**but WITHOUT ANY WARRANTY; without even the implied warranty of
**MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**GNU General Public License for more details.
**
**You should have received a copy of the GNU Lesser General Public License
**along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef		GUICURSES_HH_
# define	 GUICURSES_HH_

# include	<string>
# include	<vector>
# include	<unistd.h>
# include	"debug.h"
# include	"Gui.hh"
# include	"ILoop.hh"
# include	"entity.h"
# include	"Widget.hh"
# include	"GuiEventStack.hh"
# include	"GuiEvent.hh"
# include	"Contener.hh"
# include	"MainEntityManager.hh"
class		Loop : public ILoop
{
public:

  Loop();
  virtual ~Loop();

  /**
   * read the GuiEventStack, handle it, get input and fill MainEventStack with it.
   * @return: true if an event ocure
   */
  virtual bool	doTurn(void);
  virtual bool init();
private:
  GuiEventStack &_guiEventStack;
  ElemAlocator	&_alocator;
  // std::vector<Widget *(*)(StructEntity *)> _renderTab;
  // const char *_renderAssociationTable[NBR_RENDER] = {
  //   "Menu",
  //   "TextScreen"
  // };

  inline bool render();
 /**!
  **\brief Execute les actions de la GUIT
  **/
  void    execGuiStack();
   /**!
  **\brief set l'element principal de la GUI actuelle
  **/
  bool    setCurrent(StructEntity *gameElem);
  Widget	*_currentElem;
};

#endif		// GUICURSES_HH_
