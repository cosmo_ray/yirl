/*
**Copyright (C) <2013> <YIRL_Team>
**
**This program is free software: you can redistribute it and/or modify
**it under the terms of the GNU Lesser General Public License as published by
**the Free Software Foundation, either version 3 of the License, or
**(at your option) any later version.
**
**This program is distributed in the hope that it will be useful,
**but WITHOUT ANY WARRANTY; without even the implied warranty of
**MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**GNU General Public License for more details.
**
**You should have received a copy of the GNU Lesser General Public License
**along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include	<string>
#include	<fstream>
#include	<streambuf>
#include	"CheckJson.hh"
#include	"EntityStructPrototypes.hh"
#include	"debug.h"
#include	"global.h"

namespace LoadJson
{
  bool loadFile(const std::string &fileName, EntityManager &entityManager, int)
  {
    DPRINT_INFO("%s OK\n", fileName.c_str());
    static EntityStructPrototypes &prototypes = *getPrototypeInstance();
    std::ifstream t/*(fileName, std::ifstream::in)*/;
    DPRINT_INFO("IFSTREAM CREATE\n");
    t.open(fileName);
    DPRINT_INFO("File Open\n");
    Json::Value root;   // will contains the root value after parsing.
    DPRINT_INFO("Root Ok\n");
    Json::Reader reader;
    DPRINT_INFO("Reader Ok\n");
    
    bool parsingSuccessful = reader.parse(t, root );
    if ( !parsingSuccessful )
      {
	DPRINT_ERR("Failed to parse the configuration file %s\n%s\n", fileName.c_str(),  reader.getFormatedErrorMessages().c_str());
	return false;
      }
    loadFDec(root, prototypes);
    loadDec(root, prototypes);
    loadImpl(root, prototypes, entityManager);
    return true;
  }
}
