/*
**Copyright (C) <2013> <YIRL_Team>
**
**This program is free software: you can redistribute it and/or modify
**it under the terms of the GNU Lesser General Public License as published by
**the Free Software Foundation, either version 3 of the License, or
**(at your option) any later version.
**
**This program is distributed in the hope that it will be useful,
**but WITHOUT ANY WARRANTY; without even the implied warranty of
**MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**GNU General Public License for more details.
**
**You should have received a copy of the GNU Lesser General Public License
**along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef	RULE_H
#define	RULE_H

#include	<string.h>
#include	"init.h"

#define	NUMBERS	"0123456789"
#define	LALPHA	"qwertyuiopasdfghjklzxcvbnm"
#define	UALPHA	"QWERTYUIOPASDFGHJKLZXCVBNM"

/**
 * check if the next caracter is null
 */
int	parser_check_null(int file_id);

/**
 * check if the next caracter is equal to c
 */
int	parser_check_char(int file_id, char c);

/**
 * check next char whitout jmp at begin of the func
 */
int	parser_check_char_nj(int file_id, char c);

/**
 * check if the next caracter is equal to a caracter in str
 */
char	parser_check_chars(int file_id, const char *str);

/**
 * check if the next string is equal to the next str
 */
int	parser_check_str(int file_id, const char *str);

/**
 * check if the next caracter is between a and b include.
 */
int	parser_check_interval_char(int file_id, char a, char b);
int	parser_check_interval_char_nj(int file_id, char a, char b);

/**
 * return the nexts characters who's include betwin a and b
 * @param buf: the buffer in which the captured word is stored, this buffer must have been alocated. 
 * @return the given buf or NULL if the first non jumpable caracter isn't include betwin a and b
 */
char	*parser_get_interval_string(int file_id, char a, char b, char *buf);
char	*parser_get_interval_string_nj(int file_id, char a, char b, char *buf);

/**
 * return the next word
 *
 * @param buf: the buffer in which the captured word is stored, this buffer must have been alocated. 
 * @return the given buf
 */
char	*parser_get_next_word(int file_id, char *buf);
char	*parser_get_next_word_nj(int file_id, char *buf);

/**
 * check if the next caracter is numeric.
 */
int	parser_is_numeric(int file_id);


/**
 * return the nexts alfa  word (include betwin 'a' and 'z' and 'A' and 'B')
 * @param buf: the buffer in which the captured word is stored, this buffer must have been alocated. 
 * @return the given buf or NULL if the first non jumpable caracter if it's not an alpha
 */
char	*parser_get_alfa_str(int file_id, char *buf);
char	*parser_get_alfanumeric_str(int file_id, char *buf);
char	*parser_get_int_str(int file_id, char *buf);
char	*parser_get_float_str(int file_id, char *buf);


int	parser_is_end_file(int file_id);

void	parser_add_jumpable_char(int file_id, char c);
void	parser_reset_jumpable_char(int file_id);
void	parser_set_jumpable_chars(int file_id, const char *str);
#endif
