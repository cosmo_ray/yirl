/*
**Copyright (C) <2013> <YIRL_Team>
**
**This program is free software: you can redistribute it and/or modify
**it under the terms of the GNU Lesser General Public License as published by
**the Free Software Foundation, either version 3 of the License, or
**(at your option) any later version.
**
**This program is distributed in the hope that it will be useful,
**but WITHOUT ANY WARRANTY; without even the implied warranty of
**MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**GNU General Public License for more details.
**
**You should have received a copy of the GNU Lesser General Public License
**along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include	<string>
#include	<stdio.h>
#include	<stdlib.h>

#ifdef __unix__

static void	printFirstModule()
{
  system("echo first module: `cat conf/first_Module`");
}

int	changeFirstModule(const char *str)
{
  std::string	cmd;

  if (str == NULL)
    {
      printFirstModule();
      goto exit;
    }

  cmd = std::string("echo '") + str + std::string("' > conf/first_Module");
  system(cmd.c_str());
 exit:
  return (0);
}

static void	printWidLib()
{
  system("echo Widget library in use: `cat conf/widget | grep lib`");
}

int	changeWidLib(const char *str)
{
  std::string	cmd;

  if (str == NULL)
    {
      printWidLib();
      goto exit;
    }

  cmd = std::string("echo '") + str + std::string("' > conf/widget");
  system(cmd.c_str());
  system("echo guiDescription >> conf/widget");
 exit:
  return (0);
}


#endif
