/*
**Copyright (C) <2013> <YIRL_Team>
**
**This program is free software: you can redistribute it and/or modify
**it under the terms of the GNU Lesser General Public License as published by
**the Free Software Foundation, either version 3 of the License, or
**(at your option) any later version.
**
**This program is distributed in the hope that it will be useful,
**but WITHOUT ANY WARRANTY; without even the implied warranty of
**MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**GNU General Public License for more details.
**
**You should have received a copy of the GNU Lesser General Public License
**along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef MAP_ELEM_HH_
#define MAP_ELEM_HH_

#include	<string>
#include	<vector>
#include	"Widget.hh"

/** Widget contening text screen
 *
 * This class is made to permite to create content of type Mapelem
 * A Mapelem is a simple screen contening text and sometime a background as we can see in VM.
 */

class Map : public Widget
{
public:
  Map(StructEntity *gameElem);
  Map(StructEntity *gameElem, unsigned int x, unsigned int y, unsigned int w, unsigned int h);
  virtual ~Map();
 /**!
**\brief Tente de voir sur l'input en parametre fait une acteur sur un element
**\param Un entier de la valeur de l'input
**\return Le statut de l'input apres passage dans le conteneur
**/
  InputStatue tryHandleInput(int ch);
    /**!
  **\brief Affiche les elements contenues
  **/
  bool render() = 0;
  /**!
  **\brief redimension de l'element
  **/
  virtual void resize() = 0;
  /**!
  **\brief recupere la longeur de la case I
  **\param I etant la case
  **\return renvoie la taille
  **/
  int	getCaseLen(int i);
  /**!
  **\brief Renvoi identifiant de l'element a la position en paramtre
  **\param pos est la position dans la map et i un index
  **/
  int	getMapId(int i, unsigned int pos);
  /**!
   **\brief Renvoi identifiant de l'element a la position en paramtre
  **\param x et y est la position dans la map et i un index
  **/
  int	getMapId(int i, unsigned int x, unsigned int y);
  /**
  **\brief tente de recuperer l'action d'un element en identifiant
  **\param L'element actuel currentElem et un identifiant ID
  **\return renvoie un pointeur sur la fonction ou null si ca echoue
  **/

  /**
   **\brief return true if we need to refresh the map elem at i of pos
   **/
  bool	hasToRend(int i, unsigned int pos);

  void	setRefresh(int i, unsigned int pos, int val);
  
  FunctionEntity *tryGetActionId(Entity *currentElem, int id);
  Entity	*getElemAt(unsigned int pos);
  Entity	*getElemAt(unsigned int x, unsigned int y);

  enum	CameraType {
    FIX = 0,
    FOLLOW_CARACTER = 1,
    CANVAS = 2, // TODO: rename in english

    LAST_ELEM
  };
  
protected:
  Entity	*_elems;
  Entity	*_current;
  unsigned int	_wMap;
  unsigned int	_hMap;
  Entity	*_basicElem;
  CameraType	_cType;
  
  static const	int	MOVE_ID = 0;
  /**
   * Methode call in constructors contening all the code of the constructors
   * This methode is use to uniformise constructors code
   */
  void init();
};

#endif
