/*
**Copyright (C) <2013> <YIRL_Team>
**
**This program is free software: you can redistribute it and/or modify
**it under the terms of the GNU Lesser General Public License as published by
**the Free Software Foundation, either version 3 of the License, or
**(at your option) any later version.
**
**This program is distributed in the hope that it will be useful,
**but WITHOUT ANY WARRANTY; without even the implied warranty of
**MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**GNU General Public License for more details.
**
**You should have received a copy of the GNU Lesser General Public License
**along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include <cstring>
#include "RTextScreen.hh"
#include "RImgScreenHlp.hpp"

extern SDL_Global sg;

void	RTextScreen::init()
{
  RImgScreenHlp **tmp = ((RImgScreenHlp **)&_helper);
  const char *tmpBg = getStringVal(_background);

  *tmp = new RImgScreenHlp(*this, tmpBg);
}

RTextScreen::~RTextScreen()
{
    delete reinterpret_cast<CWidget *>(_helper);
}

bool RTextScreen::render()
{
  RImgScreenHlp *wid = reinterpret_cast<RImgScreenHlp *>(_helper);
  resetAff();
  const char *txt = toAff_.c_str();
  int	len = strlen(txt);
  int text_width , text_height;
  SDL_Color color;
  int	y = wid->rect().y;
  int	x = wid->rect().x;
  // nbr of charact + '\0'
  char	buff[CARACT_PER_LINE + 1];
  color.r = 0;
  color.g = 0;
  color.b = 0;
  color.a = 250;
  if (wid->img)
    wid->fillBg(wid->img);
  else
    wid->fillBg(sg.renderer, 255,255,255,255);
  SDL_Rect	rect = wid->rect();
  SDL_RenderDrawRect(sg.renderer, &rect);
  SDL_RenderPresent(sg.renderer);
  
  for (int i = 0; i < len; i += CARACT_PER_LINE)
    {
      if (i + CARACT_PER_LINE > len)
	{
	  strncpy(buff, txt + i, (i + CARACT_PER_LINE) - len);
	  buff[(i + CARACT_PER_LINE) - len] = 0;
	}
      else
	{
	  strncpy(buff, txt + i, CARACT_PER_LINE);
	  buff[CARACT_PER_LINE] = 0;
	}
      SDL_Surface *textSurface = TTF_RenderUTF8_Solid(sg.font, buff, color);
      SDL_Texture* text = SDL_CreateTextureFromSurface(sg.renderer, textSurface);
      text_width = textSurface->w;
      text_height = textSurface->h;
      SDL_FreeSurface(textSurface);
      // printf("%s\nx:%d y:%d\nw:%dh:%d\n", txt, x, y, text_width, text_height);
      SDL_Rect renderQuad = {x , y, text_width, text_height };
      SDL_RenderCopy(sg.renderer, text, NULL, &renderQuad);
      SDL_DestroyTexture(text);
      y += text_height;
    }

  SDL_RenderPresent(sg.renderer);
  return (true);
}

void RTextScreen::resize()
{
  CWidget *wid = reinterpret_cast<CWidget *>(_helper);

  wid->resize();
}
