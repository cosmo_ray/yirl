/*
**Copyright (C) <2013> <YIRL_Team>
**
**This program is free software: you can redistribute it and/or modify
**it under the terms of the GNU Lesser General Public License as published by
**the Free Software Foundation, either version 3 of the License, or
**(at your option) any later version.
**
**This program is distributed in the hope that it will be useful,
**but WITHOUT ANY WARRANTY; without even the implied warranty of
**MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**GNU General Public License for more details.
**
**You should have received a copy of the GNU Lesser General Public License
**along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "Mapping.hh"

template <typename T>
Mapping<T>::Mapping()
{
}

template <typename T>
Mapping<T>::~Mapping()
{
}

template <typename T>
char* Mapping<T>::mapFile(const char* path)
{
  return ((char*)static_cast<T*>(this)->mapping(path));
}

template <typename T>
bool Mapping<T>::unMapFile()
{
  return (static_cast<T*>(this)->unmapping());
}
#ifdef __unix__    
#include "../linux/SpecMapping.hpp"
#else
 #include "../windows/SpecMapping.h"
#endif
// #include <iostream>

// int main()
// {
//   Mapping<SpecMapping> *test = new SpecMapping;
//   char *file = test->mapFile("test");
//   test->unMapFile();
// }
