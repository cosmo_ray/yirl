/*
**Copyright (C) <2013> <YIRL_Team>
**
**This program is free software: you can redistribute it and/or modify
**it under the terms of the GNU Lesser General Public License as published by
**the Free Software Foundation, either version 3 of the License, or
**(at your option) any later version.
**
**This program is distributed in the hope that it will be useful,
**but WITHOUT ANY WARRANTY; without even the implied warranty of
**MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**GNU General Public License for more details.
**
**You should have received a copy of the GNU Lesser General Public License
**along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include	<curses.h>

#ifndef		DEFINE_ROG_HH_
# define	DEFINE_ROG_HH_

//
//	Touche evenement :
//

# define	UP_KEY		KEY_UP
//# define	UP_KEY	56
# define	DOWN_KEY	KEY_DOWN
//# define	DOWN_KEY 50
# define	LEFT_KEY	KEY_LEFT
//# define	LEFT_KEY 52
# define	RIGHT_KEY	KEY_RIGHT
//# define	RIGHT_KEY 54
# define	USE_KEY		KEY_ENTER
//# define	USE_KEY 53
// # define	UP_LEFT		NONE
// # define	UP_LEFT 55
// # define	UP_RIGHT	NONE
// # define	UP_RIGHT 57
// # define	DOWN_LEFT	NONE
// # define	DOWN_LEFT 49
// # define	DOWN_RIGHT	NONE
// # define	DOWN_RIGHT 51
// # define	SKILL_1		NONE
// # define	SKILL_1 113
// # define	SKILL_2		NONE
// # define	SKILL_2 119
// # define	SKILL_3		NONE
// # define	SKILL_3 101
// # define	CHG_STYLE	NONE
// # define	CHG_STYLE 100
// # define	USE_STAIRE	NONE
// # define     USE_STAIRE 48
// # define	CLOSE_GAME	NONE
// # define	CLOSE_GAME 27
// # define	OPEN_HELP	NONE
// # define	OPEN_HELP 104
// # define	OPEN_BAG	NONE
// # define	OPEN_BAG 105
// # define	NORM_WIN	NONE
// # define	NORM_WIN 10

//
//	Positionnement des fenetre :
//

# define	WIN_START_X 0
# define	WIN_START_Y 0
# define	MAP_START_X 1
# define	MAP_START_Y 1
# define	HELP_START_X 1 
# define	HELP_START_Y 1
# define	STAT_START_X 1
# define	STAT_START_Y 81
# define	EVENT_START_X 31
# define	EVENT_START_Y 1
# define	BAG_START_X 1
# define	BAG_START_Y 1
# define	KEY_START_X 31
# define	KEY_START_Y 81

# define	WIN_END_X 40
# define	WIN_END_Y 100
# define	MAP_END_X 30
# define	MAP_END_Y 80
# define	HELP_END_X 30 
# define	HELP_END_Y 80
# define	STAT_END_X 30
# define	STAT_END_Y 100
# define	EVENT_END_X 40
# define	EVENT_END_Y 80
# define	BAG_END_X 30
# define	BAG_END_Y 80
# define	KEY_END_X 40
# define	KEY_END_Y 100


# define	VERTI_LIM ((y == WIN_START_Y) || (y == 80) || (y == WIN_END_Y))

# define	HORIS_LIM ((x == WIN_START_X) || (x == 30) || (x == WIN_END_X))

# define	ANGLE_WIN (((x == WIN_START_X) && (y == WIN_START_Y)) ||\
			  ((x == WIN_END_X) && (y == WIN_START_Y)) ||\
			  ((x == 30) && (y == WIN_START_Y)) ||\
			  ((x == 30) && (y == WIN_END_Y)) ||\
			  ((x == WIN_START_X) && (y == 80)) ||\
			  ((x == 30) && (y == 80)) ||\
			  ((x == WIN_END_X) && (y == 80)) ||\
			  ((x == WIN_START_X) && (y == WIN_END_Y)) ||\
			  ((x == WIN_END_X) && (y == WIN_END_Y)))

//
//	Legend de la map :
//

# define	OPEN_DOOR	'='
# define	CLOSE_DOOR	'+'
# define	WALL		'#'
# define	FLOOR		'.'
# define	PNJ_PEACE	'P'
# define	PNJ_HOSTI	'H'
# define	CHEST		'$'
# define	UP_STAIR	'>'
# define	DOWN_STAIR	'<'
# define	WATER		'~'
# define	PLAYER		'@'
# define	BRIDGE		'B'
# define	RAT		'R'
# define	GOBLIN		'G'

#endif		// DEFINE_ROG_HH_
