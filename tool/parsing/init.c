/*
**Copyright (C) <2013> <YIRL_Team>
**
**This program is free software: you can redistribute it and/or modify
**it under the terms of the GNU Lesser General Public License as published by
**the Free Software Foundation, either version 3 of the License, or
**(at your option) any later version.
**
**This program is distributed in the hope that it will be useful,
**but WITHOUT ANY WARRANTY; without even the implied warranty of
**MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**GNU General Public License for more details.
**
**You should have received a copy of the GNU Lesser General Public License
**along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include	<string.h>
#include  <stdio.h> 
#include	<unistd.h>
#include	<stdlib.h>
#include	<sys/types.h>
#include	<sys/stat.h>
#include	<fcntl.h>
#include	"init.h"

fileBuffers	bufferManager;

static int	parser_find_unalocate_place(void)
{
  int	i;

  for (i = 0; i < bufferManager.size; ++i)
    {
      if (bufferManager.buffers[i] == NULL)
	return (i);
    }
  return (-1);
}

static	int	parser_get_place()
{
  int	i;
 
 if ((i = parser_find_unalocate_place()) != -1)
    return (i);
  bufferManager.size += 1;
  if (bufferManager.buffers == NULL)
    {
	printf("BUFFERS IS NULL in parser get place \n");
      if ((bufferManager.buffers = malloc(sizeof(*(bufferManager.buffers)))) == NULL)
	return (-1);
      return (0);
    }
  if ((bufferManager.buffers = realloc(bufferManager.buffers, sizeof(*(bufferManager.buffers)) * bufferManager.size)) == NULL)
    return (-1);
  return (bufferManager.size - 1);
}

static int	parser_add_to_bufferManager(fileBuffer	*buffer)
{
  int i = parser_get_place();
printf("int i de parser add to buffer: %i\n", i);
  if (i != -1)
    bufferManager.buffers[i] = buffer;
  return (i);
}

int	parser_free_ragequit(int file_id)
{
printf("Parseur RAGEQUIT\n");
  parser_close_file(file_id);
  return (-1);
}

/**
 * Init the parser's buffer
 * @see fileBuffers struct
 */
int	parser_init_lib()
{
  bufferManager.size = 0;
  bufferManager.buffers = NULL;
  return (0);
}

int	parser_open_file(const char *fileName)
{
  fileBuffer	*buffer;  

  if ((buffer = malloc(sizeof (*buffer))) == NULL)
  {
    printf("FAIL EXIT: fail malloc buffer in parser open file");
    exit (-1);
  }
  if ((buffer->fd = open(fileName, O_RDONLY)) == -1)
  {
    printf("FAIL EXIT: fail open in parser open file");
    free(buffer);
    return (-1);
  }
  buffer->nb_buffer = 1;
  memset(buffer->jumpable_char, 0, JUMPABLE_CHAR_BUFSIZE);
  buffer->index = 0;
  buffer->old_buffer = 0;
  buffer->change_buffer = 0;
  if ((buffer->buffers = malloc(sizeof (*(buffer->buffers)))) == NULL)
    exit (-1);
  buffer->cur_buf = buffer->buffers[0];
  //printf("going to the end..maybe ragequit test nyaaa\n");
  if ((buffer->buff_size = read(buffer->fd, buffer->cur_buf, BUFSIZE)) == -1)
    return (parser_free_ragequit(parser_add_to_bufferManager(buffer)));
  return (parser_add_to_bufferManager(buffer));
}

int	parser_read_next_block(int file_id)
{
  fileBuffer	*buffer = bufferManager.buffers[file_id];

  buffer->nb_buffer += 1;
  if ((buffer->buffers = realloc(buffer->buffers, sizeof (*(buffer->buffers)) * buffer->nb_buffer)) == NULL)
    exit (-1);
  buffer->cur_buf = buffer->buffers[buffer->nb_buffer - 1];
  if ((buffer->buff_size = read(buffer->fd, buffer->cur_buf, BUFSIZE)) == -1)
    return (parser_free_ragequit(file_id));
  buffer->index = 0;
  buffer->change_buffer += 1;
  return (buffer->buff_size);
}

void	parser_close_file(int i)
{
  if ((i < 0) || (i > bufferManager.size) || (bufferManager.buffers[i] == NULL))
    return;
  free(bufferManager.buffers[i]->buffers);
  close(bufferManager.buffers[i]->fd);
  free(bufferManager.buffers[i]);
  bufferManager.buffers[i] = NULL;
 }


void	parser_close_lib(void)
{
  int	i;

  for (i = 0; i < bufferManager.size; ++i)
    {
      parser_close_file(i);
    }
  if (bufferManager.buffers != NULL)
    free(bufferManager.buffers);
}

int	parser_restor_buffer(fileBuffer *buffer)
{
  if (!buffer->change_buffer)
    {
      if (buffer->old_buffer != -1)
	buffer->index = buffer->old_buffer;
      buffer->old_buffer = -1;
      return 0;
    }
  for (; buffer->change_buffer != 0; buffer->change_buffer -= 1)
    {
      buffer->nb_buffer -= 1;
    }
  if (buffer->nb_buffer)
    {
      if ((buffer->buffers = realloc(buffer->buffers, sizeof (*(buffer->buffers)) * buffer->nb_buffer)) == NULL)
	exit (-1);
    }
  buffer->cur_buf = buffer->buffers[buffer->nb_buffer - 1];
  buffer->index = buffer->old_buffer;
  buffer->old_buffer = -1;
  return (0);
}

int	parser_validate_buffer(fileBuffer *buffer)
{
  buffer->change_buffer = 0;
  buffer->old_buffer = -1;
  return (1);
}

void	parser_incr_index(fileBuffer *buffer)
{
  if (buffer->old_buffer == -1)
    buffer->old_buffer = buffer->index;
  buffer->index += 1;
}
