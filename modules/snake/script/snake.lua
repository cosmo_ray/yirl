NULL = 0
STRUCT = 0
YINT = 1
YFLOAT = 2
YSTRING = 3
REF = 4
ARRAY = 5
FUNCTION = 6
GEN = 7
STATIC = 8

BLOCK = 1
ANSWER = 2

nbrApple = 0
score = 1

function setRefresh(elem, val)
   local refresh = getEntityMenber(elem, "refresh")
   setEntityIntValue(refresh, 1)
end

function pushApple(map)
   local globalSnake = getEntityByName("globalSnake")
   local apple = getEntityMenber(globalSnake, "apple")
   local elems = getEntityMenber(map, "elems")
   local w = getEntityIntValue(getEntityMenber(map, "w"))
   local h = getEntityIntValue(getEntityMenber(map, "h"))
   local pos = math.random(w*h)
   local menber = getArrayMenber(elems, pos)
   setEntityIntValue(getEntityMenber(getReferencedObj(apple), "pos"), pos)
   setRefresh(getReferencedObj(apple), 1);
   arrayPushBack(menber, apple)
   nbrApple = nbrApple + 1
end

function pushBody(mapRef, pos, bodyToAdd)
   local map = getReferencedObj(mapRef)
   local elems = getEntityMenber(map, "elems")
   local pos = getEntityIntValue(pos)
   local menber = getArrayMenber(elems, pos)

   setRefresh(getReferencedObj(bodyToAdd), val);
   setEntityRefValue(getEntityMenber(getReferencedObj(bodyToAdd), "map"), map)
   setEntityIntValue(getEntityMenber(getReferencedObj(bodyToAdd), "pos"), pos)
   arrayPushBack(menber, bodyToAdd)
   map = getReferencedObj(getEntityMenber(getReferencedObj(bodyToAdd), "map"));
end

function moveBody(head)
   local globalSnake =  getEntityByName("globalSnake")
   local body = getEntityMenber(globalSnake, "body")
   if getLen(body) == 0 then
      return
   end
   local i = 1

   local menber = getArrayMenber(body, getLen(body) - 1)
   menber = getReferencedObj(menber)
   local lastMenber = menber
   menber = getArrayMenber(body, 0)
   menber = getReferencedObj(menber)
   while i < getLen(body) do
      local menber2 = getArrayMenber(body, i)
      menber2 = getReferencedObj(menber2)
      call(getEntityMenber(globalSnake, "basicMove"), menber, menber2);
      menber = menber2
      i = i + 1
   end
   call(getEntityMenber(globalSnake, "basicMove"), lastMenber, head);
end

function createBody()
   local sToAdd = ("body" .. score)
   local bodyToAdd = addEntity("MapElem", sToAdd)
   setEntityIntValue(getEntityMenber(bodyToAdd, "id"), 7)
   setFunction(getEntityMenber(bodyToAdd, "answer"), "lose")
   setEntityIntValue(getEntityMenber(bodyToAdd, "flags"), 3)
   return bodyToAdd
end

function eatApple(elem, arg1)
   local globalSnake =  getEntityByName("globalSnake")
   local body = getEntityMenber(globalSnake, "body")
   score = score + 1
   local mapRef = getEntityMenber(elem, "map")
   local map = getReferencedObj(mapRef)
   local w = getEntityIntValue(getEntityMenber(map, "w"))
   local h = getEntityIntValue(getEntityMenber(map, "h"))
   local pos = math.random(w*h)
   local elems = getEntityMenber(map, "elems")
   local dest = getArrayMenber(elems, pos)
   dest = getReferencedObj(getArrayMenber(dest, 0))
   local bodyToAdd = createBody()
   local refToAdd = createRef(bodyToAdd, NULL)
   arrayPushBack(body, refToAdd)
   pushBody(mapRef, getEntityMenber(arg1, "pos"), refToAdd)
   call(getEntityMenber(globalSnake, "basicMove"), elem, dest)
end

function answer(elem, arg1)
   call(getEntityMenber(elem, "answer"), elem, arg1)
end

function snakeAction(elem, arg1)
   local map = getReferencedObj(getEntityMenber(elem, "map"))
   local elems = getEntityMenber(map, "elems")
   local scoreE = getEntityByName("globalSnake.score")
   setEntityIntValue(scoreE, score)
   math.randomseed(os.time())
   if (nbrApple == 0) then
      pushApple(map)
   end
   local flags = getEntityMenber(arg1, "flags")
   flagsVal = getEntityIntValue(flags)
   newArg = arg1
   if yAnd(flagsVal, ANSWER) ~= 0 then
      posElem = getEntityMenber(arg1, "pos")
      newPos = getEntityIntValue(posElem)
      posElem = getEntityMenber(elem, "pos")
      oldPos = getEntityIntValue(posElem)
      newArg = getArrayMenber(elems, newPos)
      newArg = getReferencedObj(getArrayMenber(newArg, 0))
      answer(arg1, elem)
   else
      moveBody(elem)
   end
   globalSnake =  getEntityByName("globalSnake")
   call(getEntityMenber(globalSnake, "basicMove"), elem, newArg)
end

function lose(elem, arg1)
   pushChangeWidgetEvent(getEntityByName("snakeMap"), getEntityByName("loseScreen"))
end

function snakeInit(elem)
   callByName("positionCaraAtStartingPoint", elem, getEntity("MapElemActionable", "pj"))
   callByName("setRealTime")
   --playSound("./Sound/Kalimba.mp3")
end
