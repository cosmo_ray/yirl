/*
**Copyright (C) <2013> <YIRL_Team>
**
**This program is free software: you can redistribute it and/or modify
**it under the terms of the GNU Lesser General Public License as published by
**the Free Software Foundation, either version 3 of the License, or
**(at your option) any later version.
**
**This program is distributed in the hope that it will be useful,
**but WITHOUT ANY WARRANTY; without even the implied warranty of
**MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**GNU General Public License for more details.
**
**You should have received a copy of the GNU Lesser General Public License
**along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef	SCRIPTLUA_H
#define	SCRIPTLUA_H

#include	"lua.hpp"
#include	"Script.hh"

class ScriptLua : public Script
{
public:
  ScriptLua(const std::string &mod, const std::string &name);
  virtual  ~ScriptLua();
  virtual  Entity *call(FunctionEntity *, va_list *);
  virtual  Entity *callByName(const char *name, int nbArg, va_list *ap);
private:
  lua_State *_l;

  void init(const std::string &mod, const std::string &name);
  void registreCall();
};

#endif
