/*
**Copyright (C) <2013> <YIRL_Team>
**
**This program is free software: you can redistribute it and/or modify
**it under the terms of the GNU Lesser General Public License as published by
**the Free Software Foundation, either version 3 of the License, or
**(at your option) any later version.
**
**This program is distributed in the hope that it will be useful,
**but WITHOUT ANY WARRANTY; without even the implied warranty of
**MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**GNU General Public License for more details.
**
**You should have received a copy of the GNU Lesser General Public License
**along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/**
 * @file ILoop.hh
 * @brief This file contain the abstract class ILoop
 *
 * @see ILoop.hh
 */
#ifndef	ILOOP_HH
#define	ILOOP_HH

/**
 * ILoop is an abstract class which is implemented by the Widget library, which implement the action of the widget on a Turn
 */
class	ILoop
{
public:
  ILoop(){}
  virtual ~ILoop() {}

  /**
   * @brief: init the lib
   * @return: true on sucess
   */
  virtual bool init() = 0;
  /**
   * @brief: do a "turn"
   * @return: true if an evenment has been add to the mainEventStack
   */
  virtual bool doTurn() = 0;
private:

  bool	_isBlocking;
};

#endif
