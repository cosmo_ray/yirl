#include "MgrSound.h"
#include <fmodex/fmod.h>
#include <string>
#include <fstream>

using namespace std;

/*
  constructeur et initialisateur de FmodEx
*/
MgrSound::MgrSound(){
  FMOD_System_Create(&sys);
  FMOD_System_Init(sys, 32, FMOD_INIT_NORMAL, NULL);
}

/*
  destructeur
*/
MgrSound::~MgrSound(){
  FMOD_System_Release(sys);
  //FMOD_Sound_Release(sound);
}

/*
  pour lire un morceau
*/
int MgrSound::play(const string &filename){
  FMOD_System_CreateStream(sys, filename.c_str(), FMOD_HARDWARE | FMOD_LOOP_OFF | FMOD_2D, 0, &sound);
  FMOD_System_PlaySound(sys, FMOD_CHANNEL_FREE, sound, 0, &channel);
  
  return 0;
}

/*
  mettre un canal en pause
*/
void MgrSound::pause(){
  FMOD_Channel_SetPaused(channel, true);
}

/*
  reprendre la lecture sur un canal
*/
void MgrSound::resume(){
  FMOD_Channel_SetPaused(channel, false);
}

/*
  récupérer le nom de l'artiste du morceau courant
*/
string MgrSound::getArtist(){
  FMOD_TAG         tag;
  FMOD_Sound_GetTag(sound, "ARTIST", 0, &tag);
  string s((char*)tag.data);
  
  return s;
}

/*
  récupérer le titre du morceau courant
*/
string MgrSound::getTitle(){
  FMOD_TAG         tag;
  FMOD_Sound_GetTag(sound, "TITLE", 0, &tag);
  string s((char*)tag.data);
  
  return s;
}

/*
  récupérer la durée du morceau courant
*/
unsigned int MgrSound::getLength(){
  unsigned int i;
  FMOD_Sound_GetLength(sound, &i, FMOD_TIMEUNIT_MS  );
  return i;
}

/*
  récupérer la position courante dans le morceau courant
*/
unsigned int MgrSound::getCurrentPos(){
  unsigned int i;
  FMOD_Channel_GetPosition(channel, &i, FMOD_TIMEUNIT_MS  );
  return i;
}

/*
  pour avancer de i millisecondes
*/
void MgrSound::avancer(unsigned int i){
  unsigned int c=getCurrentPos();
  unsigned int l=getLength();
  if(c+i<l)
    FMOD_Channel_SetPosition(channel, c+i,FMOD_TIMEUNIT_MS );
}

/*
  pour reculer de i millisecondes
*/
void MgrSound::reculer(unsigned int i){
  unsigned int c=getCurrentPos();
  if(c-i>0)
    FMOD_Channel_SetPosition(channel, c-i,FMOD_TIMEUNIT_MS );
}

/*
  pour fixer le volume (entre 0.0 et 1.0)
*/
void MgrSound::setVolume(float f){
  FMOD_Channel_SetVolume(channel, f);
}

/*
  pour mettre à jour le système (nécessaires aux callbacks)
*/
void MgrSound::update(){
  FMOD_System_Update(sys);
}

// end of file
