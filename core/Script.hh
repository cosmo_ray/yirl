/*
**Copyright (C) <2013> <YIRL_Team>
**
**This program is free software: you can redistribute it and/or modify
**it under the terms of the GNU Lesser General Public License as published by
**the Free Software Foundation, either version 3 of the License, or
**(at your option) any later version.
**
**This program is distributed in the hope that it will be useful,
**but WITHOUT ANY WARRANTY; without even the implied warranty of
**MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**GNU General Public License for more details.
**
**You should have received a copy of the GNU Lesser General Public License
**along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef	SCRIPT_H
#define	SCRIPT_H

#include	<string>
#include	<cstdarg>
#include	"filesDefines.h"
#include	"entity.h"

class Script
{
public:
Script(const	std::string &mod, const	std::string &name)
  :_module(mod), _name(name) {}
  virtual  ~Script(){}
  virtual  Entity *call(FunctionEntity *, va_list *) = 0;
  virtual  Entity *callByName(const char *name, int nbArg, va_list *ap) = 0;
  
protected:
  std::string	getFileName(const std::string &mod, const std::string &name)
  {
    return (std::string(MODULE_DIRECTORY + mod + "/script/" + name + ".lua"));
  }

  const	std::string	_module;
  const	std::string	_name;

private:
};

#endif
