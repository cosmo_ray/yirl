/*
**Copyright (C) <2013> <YIRL_Team>
**
**This program is free software: you can redistribute it and/or modify
**it under the terms of the GNU Lesser General Public License as published by
**the Free Software Foundation, either version 3 of the License, or
**(at your option) any later version.
**
**This program is distributed in the hope that it will be useful,
**but WITHOUT ANY WARRANTY; without even the implied warranty of
**MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**GNU General Public License for more details.
**
**You should have received a copy of the GNU Lesser General Public License
**along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include	"yirlAPI.hh"
#include	"GameLoop.hh"
#include	"ModuleLoader.hh"
#include	"ChangeWidgetEvent.hh"
#include	"GuiEventStack.hh"
#include	"debug.h"
#include	<cstring>
#include	<string>

typedef std::list<std::pair<std::string, std::pair<std::size_t, std::size_t> > > lstf;

MainEntityManager	*mana = NULL;
GameLoop	        *game = NULL;

void	setMana(MainEntityManager *aMana)
{
#ifndef __unix__
  mana = aMana;
#else
  (void)aMana;
#endif
}

void	setGame(GameLoop *gameL)
{
#ifndef __unix__
  	game = gameL;
#else
  (void)gameL;
#endif

}

bool	loadModule(const char *moduleName)
{
  return (ModuleLoader::loadGame(moduleName, GameLoop::getInstance().getScriptManager()));
}

Entity	*getObject(const char *objType, const char *objName)
{
  return (mana->getEntity(objType, objName));
}

Entity	*getEntityByName(const char *objName)
{
  return (mana->getEntityByName(objName));
}

Entity	*addEntity(const char *objType, const char *objName)
{
  return (mana->addEntity(objType, objName));
}

void	endGame(bool end)
{
  game->setEndGame(end);
}

const char	*getGraphicalLibName()
{
  return (game->getGraphicalLibrary().c_str());
}

void		pushChangeWidgetEvent(StructEntity *cur, StructEntity *newWidget)
{
  GuiEventStack::getInstance().push(new ChangeWidgetEvent(cur, newWidget));
}

static const char *getFuncValueString(FunctionEntity *func)
{
  if (func->value)
    return (func->value);
  else
    return ("null");
}


Entity  *vCallByName(const char *name, int nbArg, va_list *ap)
{
  if (!name)
    return NULL;
  return (game->callByName(name, nbArg, ap));
}

Entity  *callByName(const char *name, int nbArg, ...)
{
  va_list ap;
  Entity *ret;

  if (!name)
    return NULL;
  va_start(ap, nbArg);
  ret = vCallByName(name, nbArg, &ap);
  va_end(ap);
  return ret;
}

Entity	*callWithVA(FunctionEntity *func, va_list *ap)
{
  if (func != NULL && func->value != NULL)
    return (game->call(func, ap));
  return NULL;
}

Entity	*call(FunctionEntity *func, ...)
{
  va_list ap;
  Entity *ret;
  
  if (func == NULL || func->value == NULL)
    {
      if (func) {
	//DPRINT_ERR("missing information for a call: func: %p, func value: %s", func, getFuncValueString(func));
      } else { 
	//DPRINT_ERR("missing information for a call: func is NULL");	
      }
      return (NULL);
    }
  va_start(ap, func);
  ret = callWithVA(func, &ap);
  va_end(ap);
  return ret;
}
 
MainEntityManager	*getMainEntityManager()
{
  return mana;
}

/**
 * Init the main Entity Manager
 */
void	initMana()
{
  mana = new MainEntityManager();
}

/**
 * Save the game loop pointer into local variable
 * @param gameL	the game loop (@see GameLoop)
 */
void	initGame(GameLoop *gameL)
{
  game = gameL;
}

std::string	*formatString(const char *str)
{
  //-------------------------------------
  // this part cut off the elements supposed to be entities,
  // and placed as [entityName] in a string. 
  //-------------------------------------
  std::string *fstr = new std::string(str);
  lstf tab;
  
  std::size_t fstart = fstr->find("[");
  while (fstart!=std::string::npos)
    {
      std::size_t fend = fstr->find("]", fstart);
      tab.push_back(std::make_pair(fstr->substr(fstart +1, fend - fstart -1),
  				   std::make_pair(fstart, fend - fstart + 1)));
      fstart = fstr->find("[",fstart+1);
    }

  //-------------------------------------
  // this part format all the entities found with the first part in string,
  // and replace them into the string occurences.
  //-------------------------------------

  lstf::iterator it;
  std::size_t len = 0;
  Entity *entity;
  char aBuf[1024];

  for (it=tab.begin(); it!=tab.end(); ++it)
    {
      it->second.first -= len;
      entity = getEntityByName(it->first.c_str());
      if (entity != NULL)
	/*int safe = */entityToString(entity, aBuf, 1024); //commentaire car cela bloque la compilation sous windows car variable non utiliser
      else
	strcpy(aBuf, "(null)");
      fstr->replace(it->second.first, it->second.second /*can be replaced by 'safe'*/, aBuf);
      if (((int)(len+=it->second.second-strlen(aBuf)))<0)
        len*=-1;
    }
  return fstr;
}


/**
 * Assign the new value to the entity
 * @param entity
 * @param value
 * @return return <value>
 */
void setFunctionPrototype(Entity *entity, const EntityStructPrototypes::ExtandEntityType *value)
{
  const EntityStructPrototypes::FunctionInfo *funcInfo = value->getFunctionInfo();
  std::list<EntityStructPrototypes::StructType>::const_iterator	it;
  const std::list<EntityStructPrototypes::StructType>::const_iterator	end = funcInfo->arguments.end();
  unsigned int	nArgs = funcInfo->arguments.size();
  EntityType *args = new EntityType[nArgs];
  unsigned int	i = 0;

  for (it = funcInfo->arguments.begin(); it != end; ++it)
    {
      args[i] = it->first;
    }

  setFunctionArgs(entity, nArgs, args);
}

const TimeSetting *getTimeSetting()
{
  return game->getTimeSetting();
}

void setTimeType(int t)
{
  return game->setTimeType((TimeSetting::Type)t);
}

int	getResX()
{
  return (game->getResX());
}

int	getResY()
{
  return (game->getResY());
}
