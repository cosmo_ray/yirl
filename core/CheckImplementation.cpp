/*
**Copyright (C) <2013> <YIRL_Team>
**
**This program is free software: you can redistribute it and/or modify
**it under the terms of the GNU Lesser General Public License as published by
**the Free Software Foundation, either version 3 of the License, or
**(at your option) any later version.
**
**This program is distributed in the hope that it will be useful,
**but WITHOUT ANY WARRANTY; without even the implied warranty of
**MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**GNU General Public License for more details.
**
**You should have received a copy of the GNU Lesser General Public License
**along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include	"MainEntityManager.hh"
#include	"EntityManager.hh"
#include	"yirlAPI.hh"
#include	"debug.h"
#include	"global.h"
CheckImpl::CheckImpl(checkCnt::CheckCntInfo &info, EntityManager &entityManager)
  : _info(info)
  , _entityManager(entityManager)
{
}

CheckImpl::~CheckImpl()
{
}

bool		CheckImpl::checkImplDeclareEntity()
{
  const std::string	typeEntity(_info.buffer);

  parser_set_jumpable_chars(_info.fileId, "\t ");
  if (parser_get_alfanumeric_str(_info.fileId, _info.buffer) == NULL)
    {
      DPRINT_ERR("Entity name isn't alphanumeric\n");
      return (false);
    }
  _entityManager.addEntity(typeEntity.c_str(), _info.buffer);
  DPRINT("check entity creation: %s %s\n", typeEntity.c_str(), _info.buffer);
  parser_set_jumpable_chars(_info.fileId, " .\t\n;");  
  return (true);
}

StructEntity	*CheckImpl::getParentStruct()
{
  std::list<Entity *>::reverse_iterator it = _parentsEntitys.rbegin();
  std::list<Entity *>::reverse_iterator end = _parentsEntitys.rend();

  while (it != end)
    {
      if ((*it)->type == STRUCT)
	return (reinterpret_cast<StructEntity *>(*it));
      ++it;
    }
  return (NULL);
}

bool		CheckImpl::checkImplSetArray(Entity *entity)
{
  if (!parser_check_str(_info.fileId, "init"))
    return (false);
  if (!parser_check_char(_info.fileId, '('))
    return (false);
  if (parser_get_int_str(_info.fileId, _info.buffer) == NULL)
    return (false);
  parser_check_char(_info.fileId, ')');
  const EntityStructPrototypes::ExtandEntityType *aInfo = getPrototypeInstance()->getPrototype(getParentStruct()->structName)->getMenber(entity->name)->getArrayInfo();
  ((ArrayEntity *)entity)->contentType = aInfo->getType();
  
  if (aInfo->getType() != ARRAY)
    manageArray(entity, atoi(_info.buffer), STRUCT); //the last argument is not use anyway
  else
    manageArray(entity, atoi(_info.buffer), aInfo->getArrayInfo()->getType());
  DPRINT("creat an array with a size of %s to %s\n", _info.buffer, entity->name);  
  return (true);
}

bool		CheckImpl::checkImplSetRef(Entity *entity)
{
  Entity	*other;

  parser_get_next_word(_info.fileId, _info.buffer);
  if ((other = _entityManager.getEntityByName(_info.buffer)) == NULL)
    {
      DPRINT_ERR("could not find %s in %s\n", _info.buffer, entity->name);
      return (false);
    }
  DPRINT("set ref: %s to %s\n", _info.buffer, entity->name);
  setRef(entity, other);
  return (true);
}

bool		CheckImpl::checkSetValueEntity(Entity *entity)
{
  switch (entity->type)
    {
    case YINT :
      if (parser_get_int_str(_info.fileId, _info.buffer) == NULL)
	break;
      setInt(entity, atoi(_info.buffer));
      DPRINT("set int: %d to %s\n", reinterpret_cast<IntEntity *>(entity)->value, entity->name);
      return (true);
    case YFLOAT :
      if ((parser_get_float_str(_info.fileId, _info.buffer) == NULL)
	&& (parser_get_int_str(_info.fileId, _info.buffer) == NULL))
	break;
      setFloat(entity, atof(_info.buffer));
      DPRINT("set float: %f to %s\n", atof(_info.buffer), entity->name);
      return (true);
    case YSTRING :
      if (!parser_check_char(_info.fileId, '"'))
	break;
      parser_set_jumpable_chars(_info.fileId, "\"");
      parser_get_next_word(_info.fileId, _info.buffer);
      reinterpret_cast<StringEntity *>(entity)->value = strdup(_info.buffer);
      DPRINT("set string: \"%s\" to %s\n", reinterpret_cast<StringEntity *>(entity)->value, entity->name);
      parser_set_jumpable_chars(_info.fileId, ";\n \t");
      parser_check_char(_info.fileId, '"');
      return (true);
    case REF :
      return (checkImplSetRef(entity));
    case ARRAY :
      return (checkImplSetArray(entity));
    case FUNCTION :
      return (setFunction(entity));
    case STATIC :
      return (setStatic(entity));
    default:
      break;
    }
  DPRINT_ERR("error in assignment\n");
  return (false);
}

bool	CheckImpl::setStatic(Entity *)
{
  // Entity *toSet = reinterpret_cast<StaticEntity *>(entity)->value;
  // return (checkSetValueEntity(toSet));
  parser_get_next_word(_info.fileId, _info.buffer);
  return (false);
}

bool	CheckImpl::setFunction(Entity *entity)
{
  parser_get_next_word(_info.fileId, _info.buffer);
  Entity *father = getFather(reinterpret_cast<Entity *>(entity));
  const EntityStructPrototypes::StructPrototype *proto;
  if (father->type == STRUCT)
    {
      const EntityStructPrototypes::ExtandEntityType *funEntityInf;
      proto = reinterpret_cast<const EntityStructPrototypes::StructPrototype *>(reinterpret_cast<StructEntity *>(father)->prototype);
      funEntityInf = proto->getMenber(entity->name);
      if (funEntityInf == NULL || funEntityInf->getFunctionInfo() == NULL)
	{
	  DPRINT_ERR("%s is not a function inside %s\n", _info.buffer, proto->getName().c_str());
	  if (!funEntityInf) {
	    DPRINT_ERR("func info is NULL");
	  } else {
	    DPRINT_ERR("funEntityInf->getFunctionInfo()");
	  }
	  parser_set_jumpable_chars(_info.fileId, ";\n \t");
	  return (false);
	}
      ::setFunction(entity, _info.buffer);
      ::setFunctionPrototype(entity, funEntityInf);
      DPRINT("set Function: %s to %s\n", getFunctionVal(entity), getName(entity));
    }
  parser_set_jumpable_chars(_info.fileId, ";\n \t");
  return (true);
}

bool	CheckImpl::checkImplSetArrayValue(Entity *entity)
{
  parser_get_int_str(_info.fileId, _info.buffer);
  if (!parser_check_char(_info.fileId, ']'))
    {
      DPRINT_ERR("syntaxe error\n");
      return (false);
    }
  if (!parser_check_char(_info.fileId, '='))
    {
      DPRINT_ERR("syntaxe error\n");
      return (false);
    }
  DPRINT("set X to %s at %s\n", entity->name, _info.buffer);
  return (checkSetValueEntity(::getEntity(entity, atoi(_info.buffer))));
}

bool		CheckImpl::checkImplSetEntityElem()
{
  const std::string *type;
  _parentsEntitys.clear();

  if ((type = _entityManager.getType(_info.buffer)) != NULL)
    {
      const char *curType;
      curType = type->c_str();
      Entity *entity = _entityManager.getEntity(curType,
						_info.buffer);
      _parentsEntitys.push_back(entity);
      parser_set_jumpable_chars(_info.fileId, ".;\n \t");
      while (parser_check_char_nj(_info.fileId, '.'))
	{
	  parser_get_alfanumeric_str(_info.fileId, _info.buffer);
	  //parser_get_next_word(_info.fileId, _info.buffer);
	  if ((entity = findEntity(entity, _info.buffer)) == NULL) // TODO: creat error handeling
	    {
	      DPRINT_ERR("\"%s\" was not declared inside %s\n",
			 _info.buffer, curType);
	      return false;
	    }
	  _parentsEntitys.push_back(entity);
	}
      parser_set_jumpable_chars(_info.fileId, " \t\n;");
      if ((entity->type == ARRAY) &&
	  parser_check_char(_info.fileId, '['))
	return (checkImplSetArrayValue(entity));
      if (parser_check_char(_info.fileId, '='))
	return (checkSetValueEntity(entity));
    }
  else
    {
      DPRINT_ERR("\"%s\" was not declared\n", _info.buffer);
      return (false);
    }
   return (false);
}

bool		CheckImpl::check()
{
  parser_set_jumpable_chars(_info.fileId, " \t\n;.");
  if (parser_get_alfanumeric_str(_info.fileId, _info.buffer) != NULL) // the next word contain only alphanum char
    {
      if (getPrototypeInstance()->isStructExisting(_info.buffer)) // then it's a declaration
	return (checkImplDeclareEntity());
      else // set an element in the instance
	return (checkImplSetEntityElem());
    }
  else // error
    {
      parser_set_jumpable_chars(_info.fileId, ";\n");
      parser_get_next_word(_info.fileId, _info.buffer);
      DPRINT_ERR("invalide command in implementation: %s\n", _info.buffer);
      parser_set_jumpable_chars(_info.fileId, " \t\n;");
      return (false);
    }
  return (true);
}
