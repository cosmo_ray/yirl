/*
**Copyright (C) <2013> <YIRL_Team>
**
**This program is free software: you can redistribute it and/or modify
**it under the terms of the GNU Lesser General Public License as published by
**the Free Software Foundation, either version 3 of the License, or
**(at your option) any later version.
**
**This program is distributed in the hope that it will be useful,
**but WITHOUT ANY WARRANTY; without even the implied warranty of
**MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**GNU General Public License for more details.
**
**You should have received a copy of the GNU Lesser General Public License
**along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef RMAP_CONTENER_HH_
#define RMAP_CONTENER_HH_

#include	"MapContener.hh"

class RMapContener : public MapContener
{
public:
  RMapContener(StructEntity *gameElem)
    : MapContener(gameElem)
  {
  }

  RMapContener(StructEntity *gameElem, unsigned int x, unsigned int y, unsigned int w, unsigned int h)
    : MapContener(gameElem, x, y, w, h)
  {
  }

private:
};

#endif
