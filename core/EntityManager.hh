/*
**Copyright (C) <2013> <YIRL_Team>
**
**This program is free software: you can redistribute it and/or modify
**it under the terms of the GNU Lesser General Public License as published by
**the Free Software Foundation, either version 3 of the License, or
**(at your option) any later version.
**
**This program is distributed in the hope that it will be useful,
**but WITHOUT ANY WARRANTY; without even the implied warranty of
**MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**GNU General Public License for more details.
**
**You should have received a copy of the GNU Lesser General Public License
**along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef	ENTITY_MANAGER_HH
#define	ENTITY_MANAGER_HH

#include	<unordered_map>
#include	<string>
#include	"EntityStructPrototypes.hh"
#include	"entity.h"

class	EntityManager
{
public:
  EntityManager();
  virtual ~EntityManager();

  bool		isEntityExisting(const char *objType, const char *objName);
  /**
   * @return the findest entity or NULL
   */
  Entity	*getEntity(const char *objType, const char *objName);
  Entity	*addEntity(const char *objType, const char *objName);
  bool		removeEntityByName(const char *objName);

  /**
   * Look for the entity named objName and return it.
   * @return the finded entity or NULL
   */
  Entity	*getEntityByName(const char *objName) const;
  /**
   * Look in all the tree for the type of the given name.
   */
  const	std::string *getType(const char *objName) const;

protected:

private:
  // <typeKeyValue, <EntityName, Entity>>
  std::unordered_map<std::string, std::unordered_map<std::string, Entity *> >	_entitys;

  Entity	*addBasicType(const char *objType, const char *objName, EntityType type);
  /**
   * Alloc the size for the sub-entities needed for an entity of type objType.
   * After allocating, we assign them to the father entity (@see asignStruct)
   * @return table of allocated sub-entities
   */
  Entity	**allocStruct(const char *objType, Entity *father);
  Entity	**asignStruct(Entity *father, Entity **ret, const EntityStructPrototypes::StructPrototype *proto);
  void		addInerithContent(Entity *father, Entity **ret, const EntityStructPrototypes::StructPrototype *proto);
  EntityManager(const EntityManager &);
  EntityManager &operator=(const EntityManager &);
};

#endif
