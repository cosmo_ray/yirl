file ( GLOB util_SRCS *.c )

add_library ( util STATIC ${util_SRCS} )

install ( TARGETS util DESTINATION ${LIB_INSTALL_DIR} )
