/*
**Copyright (C) <2013> <YIRL_Team>
**
**This program is free software: you can redistribute it and/or modify
**it under the terms of the GNU Lesser General Public License as published by
**the Free Software Foundation, either version 3 of the License, or
**(at your option) any later version.
**
**This program is distributed in the hope that it will be useful,
**but WITHOUT ANY WARRANTY; without even the implied warranty of
**MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**GNU General Public License for more details.
**
**You should have received a copy of the GNU Lesser General Public License
**along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef	EntityStructPrototypes_hh
#define	EntityStructPrototypes_hh

#include	<unordered_map>
#include	<string>
#include	<vector>
#include	<utility>
#include	<list>
#include	"entity.h"

#define	NON_STATIC_MENBER 0
#define	STATIC_MENBER true

class EntityManager;

class	EntityStructPrototypes
{
public:
  EntityStructPrototypes();
  typedef std::pair<EntityType, std::string *>	StructType;

  struct FunctionInfo
  {
    ~FunctionInfo();
    StructType	returnType;
    std::list<StructType>	arguments;
  };

  class	ExtandEntityType
  {
  public:
    ExtandEntityType(EntityType type, const std::string &name, bool isStatic);
    ExtandEntityType(EntityType type, const std::string &name, bool isStatic, const std::string &structName);
    ExtandEntityType(EntityType type, const std::string &name, bool isStatic, ExtandEntityType *arrayInfo);
    ExtandEntityType(EntityType type, const std::string &name, bool isStatic, FunctionInfo *functionInfo);
    ~ExtandEntityType();
    bool operator==(const ExtandEntityType &other);
    bool isStatic() const;
    EntityType	getType() const;
    const std::string &getContentName() const;
    const std::string &getStructName() const;
    const ExtandEntityType *getArrayInfo() const;
    const FunctionInfo *getFunctionInfo() const;
  private:
    ExtandEntityType(const ExtandEntityType &other);
    ExtandEntityType &operator=(const ExtandEntityType &);

    EntityType	_basicType;
    std::string _name;
    std::string _structName;
    ExtandEntityType *_arrayInfo;
    FunctionInfo *_functionInfo;
    bool	_isStatic;
  };
  
  class	StructPrototype
  {
  public:
    StructPrototype(const char *_structName);
    StructPrototype(const std::string &structName);
    ~StructPrototype();
    template<bool TYPE = false>
    void addMenber(EntityType type, const char *name, const char *structName);
    template<bool TYPE = false>
    void addMenber(EntityType type, const char *name, ExtandEntityType *arrayInfo);
    template<bool TYPE = false>
    void addMenber(EntityType type, const char *name);
    template<bool TYPE = false>
    void addMenber(EntityType type, const char *name, FunctionInfo *functionInfo);

    bool haveFather() const;
    void addFather(StructPrototype *);
    /**
     * @return the content length of all father content
     */
    unsigned int	getFatherContentLength() const;
    /**
     * @return the content length of all father content plus this content
     */    
    unsigned int	getContentTotalLength() const;
    const StructPrototype *getFather(StructPrototype *) const;
    const StructPrototype *getFather(const std::string &name) const;
    const StructPrototype *getFather(unsigned int i) const;
    const ExtandEntityType *getMenber(const std::string &name) const;
    const std::string &getName() const;
    const std::vector<ExtandEntityType *> &getContent() const;
    const EntityManager &getStaticMenber() const;

  private:
    std::string	_structName;
    std::vector<StructPrototype *> _fathers;
    std::vector<ExtandEntityType *> _content;
    unsigned int	_fatherContentLength;
    EntityManager *_staticMenber;

    void addMenber(const ExtandEntityType &other);
    bool operator==(const StructPrototype &other);
    StructPrototype &operator=(const StructPrototype &other);
    StructPrototype();
    StructPrototype(const StructPrototype &);
  };

public:
  static EntityStructPrototypes &getInstance();

  /**
   *
   * @return: return true if the structure existe
   */
  bool	isStructExisting(const std::string &structName) const;

  /**
   *
   * @return: return true if the structure existe and respect the prototype
   */
  bool	checkStructPrototype(const Entity &);

  /**
   *
   * @return: return a pointer to the findest StructPrototype or NULL
   */
  StructPrototype	*getPrototype(const char *structName);

  /**
   *
   * @return: return true if the structure dosn'r exist prototypes and has been add. 
   */
  StructPrototype	*addStruct(const std::string &structName);
  StructPrototype	*addStruct(const char *structName);
  void			rmStruct(const char *structName);

private:
  std::unordered_map<std::string, StructPrototype *> _prototypes;

  ~EntityStructPrototypes();
  EntityStructPrototypes(const EntityStructPrototypes &);
  EntityStructPrototypes &operator=(const EntityStructPrototypes &);
};
#endif
