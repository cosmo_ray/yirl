/*
**Copyright (C) <2013> <YIRL_Team>
**
**This program is free software: you can redistribute it and/or modify
**it under the terms of the GNU Lesser General Public License as published by
**the Free Software Foundation, either version 3 of the License, or
**(at your option) any later version.
**
**This program is distributed in the hope that it will be useful,
**but WITHOUT ANY WARRANTY; without even the implied warranty of
**MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**GNU General Public License for more details.
**
**You should have received a copy of the GNU Lesser General Public License
**along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef	__DDLOADER_HPP__
#define	__DDLOADER_HPP__

#include	"debug.h"
#include	<iostream>
/*Classe qui charge les librairie externe*/
/*Template du CRTP*/
template<typename T>
class DLLoader
{
  bool	open;
public:
  DLLoader()
  {
    open = false;
  }
  ~DLLoader()
  {
    if (open == true)
      closeLib();
  }

  template<typename U, typename V>
  U *loadingLib(const char* path, V args);
  void	closing();
  /**
   * Template U est le type d'objet retourner et V les argument pour le recuperer
   * Load a library ang get the entry point, cast it depending on the type pass to the method
   * @param path  the path of the lib
   * @param args  the args to be pass to the entry point lib function
   * @return  the return value (cast to U type) from the lib entry point called with arguments args
   */
  template<typename U, typename V>
  U*	loadLib(const char* path, V args)
  {
    open = true;
    return static_cast<T*>(this)->template loadingLib< U , V >(path, args);
  }
  
  void	closeLib()
  {
    if (!open)
      return ;
    open = false;
    // i reput closong here because in UnixLoader.hh it's closing, and i don't have you file to respact it, so if you want to change the name. do it in unixLoader too plz
    static_cast<T*>(this)->closing();
  }
};

 #ifdef __unix__
 #include	"../linux/Loader.h"
 #else
 #include	"../windows/Loader.h"
 #endif
#endif
