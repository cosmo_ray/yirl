/*
**Copyright (C) <2013> <YIRL_Team>
**
**This program is free software: you can redistribute it and/or modify
**it under the terms of the GNU Lesser General Public License as published by
**the Free Software Foundation, either version 3 of the License, or
**(at your option) any later version.
**
**This program is distributed in the hope that it will be useful,
**but WITHOUT ANY WARRANTY; without even the implied warranty of
**MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**GNU General Public License for more details.
**
**You should have received a copy of the GNU Lesser General Public License
**along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef	UNIX_LOADER_HH_
#define	UNIX_LOADER_HH_

#include	<string>
#include	<iostream>
#include	<dlfcn.h>

static inline void	set_fail_flag()
{
  std::cerr << dlerror() << std::endl;
}

class Loader : public DLLoader<Loader>
{
  void			*_handle;

public:
  Loader(){}
  ~Loader(){}

  /**
   * Load a library ang get the entry point, cast it depending on the type pass to the method
   * @param path  the path of the lib
   * @param args  the args to be pass to the entry point lib function
   * @return  the return value (cast to U type) from the lib entry point called with arguments args
   */
  template<typename U, typename V>
  U* loadLib(const char* path, V args)
  {
    U *_lib;
    U *(*libfunc) (V);
    void	*tmp;

    _handle = dlopen(path, RTLD_GLOBAL | RTLD_NOW);
    if (_handle == NULL)
      {
        set_fail_flag();
        return NULL;
      }
    tmp = dlsym(_handle, "entry_point");
    if (tmp == NULL)
      {
        set_fail_flag();
        return NULL;
      }
    libfunc = reinterpret_cast<U *(*)(V)>(tmp);
    _lib = libfunc(args);
    return _lib;
  }

  void	closing()
  {
    if (dlclose(_handle))
      set_fail_flag();
  }
};

#endif
