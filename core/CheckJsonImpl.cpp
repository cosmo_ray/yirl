/*
**Copyright (C) <2013> <YIRL_Team>
**
**This program is free software: you can redistribute it and/or modify
**it under the terms of the GNU Lesser General Public License as published by
**the Free Software Foundation, either version 3 of the License, or
**(at your option) any later version.
**
**This program is distributed in the hope that it will be useful,
**but WITHOUT ANY WARRANTY; without even the implied warranty of
**MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**GNU General Public License for more details.
**
**You should have received a copy of the GNU Lesser General Public License
**along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/**
 * @file CheckJsonImpl.cpp
 * @brief this file is the implementation of the parsing of the json file to get entities implementations.
 *	It use the class JsonImplParser
 * @see JsonImplParser.hh
 */

#include	<string>
#include	<fstream>
#include	<streambuf>
#include	<algorithm>
#include	"CheckJson.hh"
#include	"EntityStructPrototypes.hh"
#include	"debug.h"
#include	"yirlAPI.hh"
#include	"JsonImplParser.hh"
#include	"global.h"

StructEntity*		copyStructEntity(StructEntity* src, StructEntity* dest);

/**
 * Generate a random string of length len
 * @param len length of the string
 * @return a generated string of length ::len:: where each character is randomly created
 */
const std::string generateRandomString(unsigned int len)
{
  srand(0);
  auto random_char = []() -> char
    {
      const char charset[] = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
      return charset[rand() % (sizeof(charset) - 1)];
    };
  std::string str(len, 0);
  std::generate_n(str.begin(), len, random_char);
  return str;
}

namespace LoadJson
{
  /**
   * Constructor of the class JsonImplParser
   * @param em	Instance of EntityManager, allow to create, destroy, manage entities
   * @param root Json Value containing the entities implementations
   */
  JsonImplParser::JsonImplParser(EntityManager &em, const Json::Value &root)
    : _em(em), _esp(*getPrototypeInstance()), _root(root)
  {
  }

  /**
   * Constructor of the class JsonImplParser
   * @param em	Instance of EntityManager, allow to create, destroy, manage entities
   * @param em	Instance of EntityStructPrototypes, allow to manage the structures of entities
   * @param root Json Value containing the entities implementations
   */
  JsonImplParser::JsonImplParser(EntityManager &em,
				 EntityStructPrototypes &esp,
				 const Json::Value &root)
    : _em(em), _esp(esp), _root(root)
  {
  }

  /**
     * Look for the entity ::name:: in EntityManager or if not found in the root Json value. If the entity is found on the Json file, the found entity is first create via setEntity.
     * @return The entity if found, NULL otherwise
     */
  Entity*		JsonImplParser::findEntityByName(const std::string& name)
  {
    Entity* entity = _em.getEntityByName(name.c_str());

    if (entity != NULL) {
      LOG("Entity '%s' found in EntityManager", tryGetEntityName(entity));
      return entity;
    } else if (_root[name].empty() == false) {
      LOG("Entity '%s' found in Json, setting entity in EntityManager", name.c_str());
      entity = setEntity(name, _root[name]);
      return entity;
    }

    //TODO: Ajouter ce lien a la liste des liaisons. Utiliser une autres méthodes pour créer l'entité par le même type
    return NULL;
  }

  /**
   * Look into the attribute _parentList to find the closest struct of the current entity
   * @return The closest parent which is a structure
   */
  StructEntity*	JsonImplParser::getParentStruct()
  {
    std::list<Entity *>::reverse_iterator it = _parentList.rbegin();
    std::list<Entity *>::reverse_iterator end = _parentList.rend();

    while (it != end) {

      if ((*it)->type == STRUCT) {
        StructEntity* parent = reinterpret_cast<StructEntity *>(*it);
        LOG("Parent struct : %s", tryGetStructEntityName(T_E(parent)));
        return (parent);
      }

      it++;
    }
    return (NULL);
  }

  /**
   * Copy the string from value into the entity value
   * @param entity	the entity we want to set the value
   * @param value	the actual value to set to the entity
   * @param entityName	Not used in this methods
   * @return false if value is not a string, true otherwise
   */
  bool	JsonImplParser::setValueToEntity(StringEntity* entity, const Json::Value& value, const std::string&)
  {
    if (!value.isString())
      return false;

    setString((Entity *)entity, value.asCString());
    LOG_INFO("Set string entity '%s' to \"%s\"", tryGetEntityName((Entity*)entity), getStringVal((Entity*)entity));
    return true;
  }
  
  /**
   * Copy the function from value into the entity value
   * @param entity	the entity we want to set the value
   * @param value	the actual value to set to the entity
   * @param entityName	Not used in this methods
   * @return false if value is not a string, true otherwise
   */
  bool	JsonImplParser::setValueToEntity(FunctionEntity* entity, const Json::Value& value, const std::string&)
  {
    if (!value.isString())
      return false;

    LOG("Set function entity '%s' to \"%s\"", entity->name, value.asCString());
    // setFunctionPrototype((Entity *)entity, NULL);
    Entity *father = getFather(reinterpret_cast<Entity *>(entity));
    const EntityStructPrototypes::StructPrototype *proto;

    if (checkType(father, STRUCT)) {
      const EntityStructPrototypes::ExtandEntityType *funEntityInf;
      proto = reinterpret_cast<const EntityStructPrototypes::StructPrototype *>(reinterpret_cast<StructEntity *>(father)->prototype);
      funEntityInf = proto->getMenber(entity->name);

      if (funEntityInf == NULL || funEntityInf->getFunctionInfo() == NULL) {
	LOG_ERR("%s is not a function inside %s", value.asCString(), proto->getName().c_str());
	return false;
      }

      ::setFunction(reinterpret_cast<Entity *>(entity), value.asCString());
      ::setFunctionPrototype(reinterpret_cast<Entity *>(entity), funEntityInf);
      LOG("set Function: %p aka %s to %s sturct proto",
	  entity,
	  getFunctionVal(reinterpret_cast<Entity *>(entity)),
	  tryGetEntityName(reinterpret_cast<Entity *>(entity)));
    }
    return true;
  }

  std::string	getNewUnicRefName(const std::string &name)
  {
    if (!getEntityByName(name.c_str()))
      return (name);
    return (getNewUnicRefName(name + "_"));
  }

  /**
   * Look for the reference or create it if declared as sub entity and set it to the Entity
   * @param entity	the entity we want to set the value
   * @param value	the actual value to set to the entity
   * @param entityName	Used to give a name to the sub-entity if needed
   * @return True if reference entity is found and entity has the REF type or false
   */
  bool	JsonImplParser::setValueToEntity(RefEntity* entity,
					 const Json::Value& value,
					 const std::string& entityName)
  {
    Entity* ref = NULL;
    std::string reference;
    Entity* cur = getReferencedObj((Entity*)entity);
      
    LOG_INFO("Setting RefEntity to entity %s", entity->name);
    if (cur != NULL) {
	
      //LOG_WARN("The value is already set for the reference \"%s\". Actual value :\t%s", tryGetEntityName((Entity *)entity), tryGetEntityName(cur));
    }
    if (value.isObject()) {
      LOG_INFO("Json value is an object");

      if (!entityName.empty()) {
      	reference = entityName;
      } else if (!value["_type"].empty() 
		 && (!value["_name"].empty() 
		     || getName((Entity *)entity) != NULL)) {

        reference = (value["_name"].empty())
          ? getNewUnicRefName(("ref_" + std::string(getName((Entity *)entity))))
          : value["_name"].asString();
      } else {
      	DPRINT_WARN("Generating a random string for the reference name!\nYou should avoid it!");
      	reference = "ref_" + generateRandomString(4);
      }

      LOG_INFO("Setting the entity with name '%s'", reference.c_str());
      ref = setEntity(reference, value);

      if (ref == NULL) {
	LOG_INFO("Error while setting the entity '%s'", reference.c_str());
	return false;
      }

    } else if (value.isString()) {
      LOG("Value json is a string.");
      LOG("Looking for the entity named '%s'", value.asCString());
      ref = findEntityByName(value.asString());

      if (ref == NULL) {
	LOG_WARN("Entity '%s' not found.", value.asCString());
	LOG("Adding the link to the references queue.");
	_references.push_back(std::make_pair(entity, value.asString()));
	return false;
      }

    }

    if (ref == NULL) {
      LOG_ERR("Error while setting entity !");
      return false;
    }

    LOG("END: set ref %s to %s", tryGetEntityName(ref), entity->name);
    return setRef(T_E(entity), ref) != NULL;
  }
  
    /**
   * Set the array from the json to the entity
   * @param entity	the entity we want to set the value
   * @param value	the actual value to set to the entity, this value can be an json array ('[]') or a json object ('{}')
   * @param entityName	Used to give a name to the sub-entity if needed
   * @return True if reference entity is found and entity has the REF type or false
   */
  bool	JsonImplParser::setValueToEntity(ArrayEntity* entity,
					 const Json::Value& value,
					 bool shouldExtand)
  {
    unsigned int oldLen = 0; 
    LOG("Start setting array from json to entity");
    LOG("Entity name: %s\tjsonValue size: %d",
	tryGetEntityName((const Entity*)entity),
	value.size());

    if (entity->len && shouldExtand)
      oldLen = entity->len;
    const EntityStructPrototypes::StructPrototype *tmpSP = _esp.getPrototype(getParentStruct()->structName);
    const EntityStructPrototypes::ExtandEntityType *tmpEET = tmpSP->getMenber(getName((const Entity*)entity));
    const EntityStructPrototypes::ExtandEntityType *aInfo = tmpEET->getArrayInfo();

    entity->contentType = aInfo->getType();
    LOG("content Type is '%s'", entityTypeToString(getContentType((const Entity*)entity)));

    if (getContentType((const Entity*)entity) != ARRAY)
      manageArrayEntity(entity, value.size() + oldLen, YINT);
    else
      manageArrayEntity(entity, value.size() + oldLen, aInfo->getArrayInfo()->getType());

    if (value.isArray()) {
      unsigned int i;

      for (i = 0; i < value.size(); i++) {
	// Made a modification here for exetanded array
	Entity* s_entity = getEntity((Entity *)entity, i + oldLen);
	LOG("%d : %s\t%s", i, tryGetEntityName(s_entity), value[i].toStyledString().c_str());
	setValueEntity(s_entity, value[i]);
      }

    } else if (value.isObject()) {
      unsigned int i;
      Json::Value::const_iterator it;

      for (it = value.begin(), i = 0;
	   it != value.end() && i < value.size();
	   ++it, ++i) {
	  // Made a modification here for exetanded array
	  Entity* tmp = getEntity((Entity*)entity,i);
	  LOG("%d : %s is %s", i, it.key().asCString(), entityTypeToString(tryGetType(tmp)));
	  setValueEntity(tmp, *it, it.key().asString());
	}

    }

    return false;
  }

  namespace {
    bool shouldItExtand(std::list<std::string> &tryOpenLst, const char *str) {
      std::list<std::string>::iterator it = tryOpenLst.begin();
      const std::list<std::string>::iterator end = tryOpenLst.end();

      it = find(it, it, str);
      if (it != end)
	return true;
      return false;
    }
  }
  
  bool	JsonImplParser::setValueToEntity(StructEntity* entity,
					 const Json::Value& value,
					 std::list<std::string> *tryOpenLst)
  {
      LOG("setValueToEntity %s", tryGetEntityName((Entity*)entity));
      bool shouldExtand = false;
      
    for (Json::Value::const_iterator i = value.begin(); i != value.end(); ++i) {

      if (i.key().asCString()[0] == '_')
	continue;
      LOG("key %s : %s", i.key().asCString(), (*i).toStyledString().c_str());
      Entity *sent = findEntity((Entity*)entity, i.key().asCString());

      if (tryGetType(sent) == ARRAY && tryOpenLst) {
	if (shouldItExtand(*tryOpenLst,  i.key().asCString())) {
	  std::cout << i.key().asCString() << std::endl;
	  std::cout << "old array " << T_A(sent)->len << std::endl;
	  shouldExtand = true;
	}
      }

      if (sent != NULL) {
	_parentList.push_back(sent);
	setValueEntity(sent, (*i), "", shouldExtand);
	_parentList.pop_back();
      } else {
	LOG_ERR("Could not find %s in %s",
		i.key().asCString(),
		tryGetEntityName((Entity*)entity));

	return false;
      }
    }

    return true;
  }

  /**
   * Set the array from the json to the entity
   * @param entity	the entity we want to set the value
   * @param value	the actual value to set to the entity, this value is a json object ('{}')
   * @param entityName	Used to give a name to the sub-entity if needed
   * @return True  :: to be defined
   */
  bool	JsonImplParser::setValueToEntity(StructEntity* entity,
					 const Json::Value& value,
					 const std::string& )
  {
    setValueToEntity(entity, value, NULL);
  }

  /**
   * Set json to the static entity
   * @param entity	the entity we want to set the value to
   * @param value	the actual value we want to set
   * @entityName	Used to give a name to the sub-entity if needed
   * @return True :: to be defined
   */
  bool	JsonImplParser::setValueToEntity(StaticEntity* entity,
					 const Json::Value &value,
					 const std::string& entityName)
  {
    LOG("Currently never used !");
    return true;
  }

  bool	JsonImplParser::setValueEntity(Entity* entity,
				       const Json::Value &value,
				       const std::string& entityName)
  {
    setValueEntity(entity, value, entityName, false);
  }

  
  /**
   * Get and set the value to the entity depending on its type
   * @param entity	the entity we want to set the value
   * @param value	the actual value to set to the entity
   * @param entityName	Used to give a name to the sub-entity if needed
   * @return True if reference entity is found and entity has the REF type or false
   */
  bool	JsonImplParser::setValueEntity(Entity* entity,
				       const Json::Value &value,
				       const std::string& entityName,
				       bool shouldExtand)
  {
    LOG("Rooting the entity with its type '%s'",
	entityTypeToString(tryGetType(entity)));

    switch (tryGetType(entity))
      {
      case YSTRING:
      	LOG("Entity %s is a YSTRING", tryGetEntityName(entity));
      	setValueToEntity(reinterpret_cast<StringEntity*>(entity), value, entityName);
      	break;
      case REF:
      	LOG("Entity %s is a REF", tryGetEntityName(entity));
      	setValueToEntity(reinterpret_cast<RefEntity*>(entity), value, entityName);
      	break;
      case ARRAY:
      	LOG("Entity %s is a ARRAY", tryGetEntityName(entity));
      	setValueToEntity(reinterpret_cast<ArrayEntity*>(entity), value, shouldExtand);
      	break;
      case YINT :
      	LOG("Entity %s is a YINT", tryGetEntityName(entity));
      	setInt(entity,value.asInt());
      	break;
      case YFLOAT :
      	LOG("Entity %s is a YFLOAT", tryGetEntityName(entity));
      	setFloat(entity, value.asDouble());
      	break;
      case FUNCTION :
      	LOG("Entity %s is a FUNCTION", tryGetEntityName(entity));
      	setValueToEntity(reinterpret_cast<FunctionEntity*>(entity), value, entityName);
      	break;
      case GEN :
      	LOG_WARN("Entity %s is a GEN, not handle yet", tryGetEntityName(entity));
      	break;
      case STRUCT :
      	LOG_WARN("Entity %s is a STRUCT, not handle yet", tryGetEntityName(entity));
      	setValueToEntity(reinterpret_cast<StructEntity*>(entity), value, entityName);
      	break;
      case STATIC :
      	LOG_WARN("Entity %s is a STATIC, not handle yet", tryGetEntityName(entity));
	setValueToEntity(reinterpret_cast<StaticEntity*>(entity), value, entityName);
      	break;
      default:
      	LOG_WARN("Entity type not handle yet !");
      	break;
      }

    return true;
  }

  /**
   * Set the Basic Entity from the Json Value with its attributes.
   * We can extend other entity for the creation of the entity.
   * @param name	Name of the entity to set
   * @param value	Value to parse to set the entity
   * @param shouldExtand if the entity is an array and set to true, extand th current array instead of creating a new one
   * @return Setted entity
   */
  Entity*	JsonImplParser::setEntity(const std::string& name,
					  const Json::Value &value)
  {
    LOG("START setting Entity %s: %s", name.c_str(), value.toStyledString().c_str());
    const Json::Value typeJson = value["_type"];
    const Json::Value entityToExtend = value["_extend"];
    const Json::Value jsonTryOpen = value["_tryopen"];
    std::list<std::string> tryOpenLst;
    Entity* entity;

    if (_em.isEntityExisting(typeJson.asCString(), name.c_str())) {
      entity = _em.getEntity(typeJson.asCString(), name.c_str());
    } else {
      entity = _em.addEntity(typeJson.asCString(), name.c_str());
    }

    if (!entityToExtend.isNull() && entityToExtend.isString()) {
      LOG("The entity '%s' will extend the entity '%s'",
	  name.c_str(), entityToExtend.asCString());
      Entity* tmp = findEntityByName(entityToExtend.asCString());
      if (checkType(tmp, STRUCT)) {
      	copyStructEntity((StructEntity*)tmp,
			 (StructEntity*)entity);
      }
    }

    if (!jsonTryOpen.isNull() && jsonTryOpen.isArray()) {
      for (int i = 0; i < jsonTryOpen.size(); ++i) {
    	if (jsonTryOpen[i].isString()) {
	  std::cout << "try open" << std::endl;
	  std::string tmp;
    	  tmp = jsonTryOpen[i].asString();
	  tryOpenLst.push_back(tmp);
    	} else
    	  LOG_WARN("_tryopen should contain a string's array");
      }
    }

    _parentList.push_back(entity);
    /**
     * We have to get every value for every attributes
     * This part need the entity and the json value for the entity
     */

    if (setValueToEntity((StructEntity*)entity, value, &tryOpenLst) == false)
      return NULL;
    _parentList.pop_back();
    LOG("FINISH setting entity %s\n", name.c_str());
    return entity;
  }

  void		JsonImplParser::resolveLinks()
  {
    std::list<std::pair<RefEntity*, std::string>>::iterator	it = _references.begin();
    std::list<std::pair<RefEntity*, std::string>>::iterator	end = _references.end();

    while (it != end)
      {
	LOG("RESOLVELINKS: "  "Try to resolve references %s",
	    (*it).second.c_str());
    	Entity* e = _em.getEntityByName((*it).second.c_str());
    	if (e == NULL) {
	  LOG("RESOLVELINKS: " "Set ref entity %s to the entity %s",
	      tryGetEntityName((Entity*)(*it).first), tryGetEntityName(e));
    	  setRef(T_E((*it).first), e);
    	}
	it++;
      }
  }

  /**
   * This method load the implementations of a json configuration file.
   * To add implementation in a json file insert a impl object with all implementations inside it.
   * @param root	The root json file object
   * @param esp	Instance of EntityStructPrototypes, allow to get informations about entities' structures
   * @param em	Instance of EntityManager, allow to create, access entities in the project.
   * @param resolveLinks	Tells if the links between entity have to be resolve now or have to be kept for future call with resolving
   */
  bool loadImpl(Json::Value &root,
		EntityStructPrototypes &esp,
		EntityManager &em, bool resolveReferences)
  {
    std::cout << "enter load impl" << std::endl;
    unsigned int index = 0;
    LOG("ENTER %s", STR(loadImpl(root, esp, em)));
    const Json::Value implementations = root["impl"];
    JsonImplParser parser(em, esp, implementations);
    Json::Value::const_iterator end = implementations.end();
    LOG("impl size: %d", implementations.size());
    for (Json::Value::const_iterator it = implementations.begin();
	 it != end; ++it)  // Iterates over the sequence elements.
      {
	std::cout << it.key().asString() << std::endl;
	if ((*it).isObject()) {
	  Entity* tmp = parser.setEntity(it.key().asString(), *it);

	  if (!tmp)
	    return false;

	  // Json::Value val = parser.entityToJson(tmp);
	  LOG("tmp : %s", tryGetEntityName(tmp));
	}
	++index;
      }

    if (resolveReferences == true) {
      parser.resolveLinks();
    }

    LOG("END %s", "loadImpl");
    return (true);
  }
}
