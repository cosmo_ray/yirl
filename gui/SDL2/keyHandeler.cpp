/*
**Copyright (C) <2013> <YIRL_Team>
**
**This program is free software: you can redistribute it and/or modify
**it under the terms of the GNU Lesser General Public License as published by
**the Free Software Foundation, either version 3 of the License, or
**(at your option) any later version.
**
**This program is distributed in the hope that it will be useful,
**but WITHOUT ANY WARRANTY; without even the implied warranty of
**MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**GNU General Public License for more details.
**
**You should have received a copy of the GNU Lesser General Public License
**along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include	"keyHandeler.hh"
#include	"Widget.hh"

int	convertToYKEY(SDL_Keycode key)
{
  switch (key)
    {
    case SDLK_UP:
      return (UP_ARROW);
    case SDLK_DOWN:
      return (DOWN_ARROW);
    case SDLK_LEFT:
      return (LEFT_ARROW);
    case SDLK_RIGHT:
      return (RIGHT_ARROW);
    case SDLK_RETURN:
      return ('\n');
    case SDLK_TAB:
      return ('\t');
    default:
      return (-1);
    }
}
