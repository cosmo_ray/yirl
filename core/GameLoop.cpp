/*
**Copyright (C) <2013> <YIRL_Team>
**
**This program is free software: you can redistribute it and/or modify
**it under the terms of the GNU Lesser General Public License as published by
**the Free Software Foundation, either version 3 of the License, or
**(at your option) any later version.
**
**This program is distributed in the hope that it will be useful,
**but WITHOUT ANY WARRANTY; without even the implied warranty of
**MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**GNU General Public License for more details.
**
**You should have received a copy of the GNU Lesser General Public License
**along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include	<iostream>
#include	"ScriptLua.hh"
#include	"GameLoop.hh"
#include	"ModuleLoader.hh"
#include	"CallEvent.hh"
#include	"yirlAPI.hh"
#include	"debug.h"
#include	<utility>
#include	"global.h"

/**
 * No Namespace name on purpose.
 */
namespace
{
  /**
   * @return the first word in the file conf/first_Module
   */
  std::string getFirstModule()
  {
    DPRINT_INFO("ENTER getFirstModule");
    char buff[225];
    int fileId = parser_open_file("conf/first_Module");
    if (fileId < 0) {
      DPRINT_ERR("can not open file conf/first_Module\n");
      return ("");
    }
    parser_add_jumpable_char(fileId, '\n');
    parser_get_next_word(fileId, buff);
    parser_close_file(fileId);
    DPRINT_INFO("getFirstModule:\n\treturn '%s'", buff);
    return (buff);
  }

  /**
   * Get the Graphic library name in the file ./conf/gui and concat it with '.so'
   * @return the first word in file conf/gui and concat it with '.so'
   */
  std::string getGraphicalLibrary()
  {
    DPRINT_INFO("in get graphi\n");
    char buff[225];
    int fileId = parser_open_file("conf/gui");
    if (fileId < 0) {
      DPRINT_ERR("can not open file conf/gui\n");
      return ("");
    }
    DPRINT_INFO("check file ID %d", fileId);
    parser_set_jumpable_chars(fileId, "\n ");
    DPRINT_INFO("check file ID %d", fileId);
    parser_get_next_word(fileId, buff);
    parser_close_file(fileId);
    DPRINT_INFO("out get graphi");
    return (std::string(buff) + LIB_EXT);
  }

  /**
   * @return the first word in the file conf/widget concatened with LIB_EXT
   * @see LIB_EXT
   */
  std::string getWidgetLibrary()
  {
    LOG_INFO("in get graphi");
    char buff[225];
    int fileId = parser_open_file("conf/widget");
    if (fileId < 0) {
      DPRINT_ERR("can not open file conf/widget\n");
      return ("");
    }
    LOG_INFO("check file ID %d", fileId);
    parser_set_jumpable_chars(fileId, "\n ");
    parser_get_next_word(fileId, buff);
    parser_close_file(fileId);
    LOG_INFO("out get graphi");
    return (std::string(buff) + LIB_EXT);
  }

  /**
   * Open the file conf/widget starting from the executable directory
   * @return The second word from the file conf/widget
   */
  std::string getWidgetModule()
  {
    char buff[225];
    LOG_INFO("ENTER getWidgetModule");

    int fileId = parser_open_file("conf/widget");
    if (fileId < 0) {
      DPRINT_ERR("can not open file conf/widget\n");
      return ("");
    }
    LOG_INFO("OPEN GRAPHICAL MODULE %d", fileId);
    parser_set_jumpable_chars(fileId, "\n ");
    parser_get_next_word(fileId, buff);
    parser_get_next_word(fileId, buff);
    parser_close_file(fileId);
    LOG_INFO("\tReturn buff containing %s", buff);
    return std::string(buff);
  }
}

void GameLoop::initTimeSetting()
{
  DPRINT_INFO("GameLoop::getTimeSetting");
  char buff[225];
  int fileId = parser_open_file("conf/time");

  _time.type = TimeSetting::Type::TURN_BASE;
  _time.val = 0;
  _time.redoLastAction = false;
  
  if (fileId < 0) {
    DPRINT_INFO("can not open file conf/time\n");
    return;
  }
  parser_add_jumpable_char(fileId, '\n');
  parser_get_next_word(fileId, buff);
  if (!strcmp("realtime", buff)) {
    parser_get_next_word(fileId, buff);
    _time.val = atoi(buff);
    if (_time.val > 0)
      _time.type = TimeSetting::Type::REAL_TIME;
    parser_get_next_word(fileId, buff);
    if (!strcmp("redoLastAction", buff))
	_time.redoLastAction = true;	
  }
  parser_close_file(fileId);
  DPRINT_INFO("getFirstModule\n");
  return ;
}

void	GameLoop::setTimeType(TimeSetting::Type t)
{
  _time.type = t;
}


bool	GameLoop::readRes()
{
  char buff[225];
  int fileId = parser_open_file("conf/resolutions");
  
  if (fileId < 0)
    return false;

  parser_set_jumpable_chars(fileId, "x*");
  _resX = atoi(parser_get_next_word(fileId, buff));
  _resY = atoi(parser_get_next_word(fileId, buff));
  parser_close_file(fileId);
  return true;
}


int	GameLoop::getResX()
{
  return (_resX);
}

int	GameLoop::getResY()
{
  return (_resY);
}

/**
 * @return the singleton instance of GameLoop.
 */
GameLoop &GameLoop::getInstance()
{
  static GameLoop ret;
  return (ret);
}

/**
 * Constructor
 * Default value:
 *  - _endGame = false
 *  - _mainEventStack = MainEventStack singleton (@see MainEventStack)
 */
GameLoop::GameLoop()
  : _endGame(false)
  , _mainEventStack(MainEventStack::getInstance())
{
}

GameLoop::~GameLoop()
{
}

void GameLoop::setEndGame(bool value)
{
  this->_endGame = value;
}

const std::string &GameLoop::getGraphicalLibrary()
{
  return (_graphicalLibrary);
}

std::vector<Script *> &GameLoop::getScriptManager()
{
  return (_scriptManager);
}

Entity *GameLoop::call(FunctionEntity *func, va_list *ap)
{
  Entity *ret;
  std::vector<Script *>::iterator	it = _scriptManager.begin();
  const std::vector<Script *>::iterator	end =  _scriptManager.end();

  LOG("try to call %p", func);
  while (it != end) {
      if ((ret = (*it)->call(func, ap)) != NULL)
	return (ret);
      ++it;
    }
  return (NULL);
}

Entity  *GameLoop::callByName(const char *name, int nbArg, va_list *ap)
{
  Entity *ret;
  std::vector<Script *>::iterator	it = _scriptManager.begin();
  const std::vector<Script *>::iterator	end =  _scriptManager.end();

  LOG("try to call %s", name);
  while (it != end) {
    if ((ret = (*it)->callByName(name, nbArg, ap)) != NULL)
	return (ret);
      ++it;
    }
  return NULL;
}


bool GameLoop::init()
{
  _resX = 0;
  _resY = 0;
  allocPrototypeInstance();
  initMana();
  DPRINT_INFO("ENTER %s", STR(GameLoop::init()));
  parser_init_lib();
  readRes();
  DPRINT_INFO("GOING LOAD GRAPHIC");
  if (!ModuleLoader::loadGame(getWidgetModule(), _scriptManager))
    return (false);
  DPRINT_INFO("END LOAD GRAPHIC");
  if (!ModuleLoader::loadGame(getFirstModule(), _scriptManager))
    return (false);
  initTimeSetting();
  _graphicalLibrary = getGraphicalLibrary();
  DPRINT_INFO("NAME OF THE GRAPHIC LIB LOADED: %s", getWidgetLibrary().c_str());
  LibParam param;
  param.mana = getMainEntityManager();
  param.gameLoop = this;
  param.current = getPrototypeInstance();
  _loop = _guiLoader.loadLib<ILoop, LibParam>(getWidgetLibrary().c_str(),param);
  parser_close_lib();

  if (_loop == NULL) {
    DPRINT_WARN("NO GUI INCLUDE");
    return (false);
  }
  DPRINT_INFO("Enter in _loop->init");
  return (_loop->init());
}

const TimeSetting *GameLoop::getTimeSetting()
{
  return (&_time);
}


void GameLoop::close()
{
  _guiLoader.closeLib();
   delete _loop;
}

void GameLoop::execEvenStack()
{
  Event *currentEvent;
  while (!_mainEventStack.empty())
  {
    currentEvent = _mainEventStack.pop();
    LOG("%d", (CoreEvent::Type)currentEvent->getType());
    switch ((CoreEvent::Type)currentEvent->getType())
      {
      case CoreEvent::CALL:
        ::callWithVA(reinterpret_cast<CallEvent *>(currentEvent)->getToCall(), reinterpret_cast<CallEvent *>(currentEvent)->getAP());
      }
    delete currentEvent;
  }
}

void GameLoop::start()
{
  if (init() != true)
    return ;
  DPRINT_INFO("Init finnish");
  while (!_endGame)
    {
      if (_loop->doTurn())
	  {
	    DPRINT_INFO("eventStart");
	    execEvenStack();
	    DPRINT_INFO("eventend");
	  }
    }
  close();
}
