/*
**Copyright (C) <2013> <YIRL_Team>
**
**This program is free software: you can redistribute it and/or modify
**it under the terms of the GNU Lesser General Public License as published by
**the Free Software Foundation, either version 3 of the License, or
**(at your option) any later version.
**
**This program is distributed in the hope that it will be useful,
**but WITHOUT ANY WARRANTY; without even the implied warranty of
**MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**GNU General Public License for more details.
**
**You should have received a copy of the GNU Lesser General Public License
**along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include	"TextEdit.hh"
#include	"debug.h"
namespace
{
  inline bool isInputable(int c)
  {
    return ((c >= ' ' && c <= '~'));
  }
}

TextEdit::TextEdit(StructEntity *gameElem)
  : Widget (TEXT_EDIT, gameElem)
{
  init();
}

TextEdit::TextEdit(StructEntity *gameElem, unsigned int x, unsigned int y, unsigned int w, unsigned int h)
  : Widget (TEXT_EDIT, gameElem, x, y, w, h)
{
  init();
}

TextEdit::~TextEdit()
{
}

void TextEdit::init()
{
  _next = findEntity((Entity *)_elem, "next");
  _str = getStringVal(findEntity((Entity *)_elem, "text"));
}

Widget::InputStatue TextEdit::tryHandleInput(int c)
{
  //DPRINT_INFO("%d touch push\n", c);
  if (isInputable(c))
    {
      _str += c;
      return (NOACTION);
    }
  else if (c == 127)
    {
      _str.pop_back();
      return (NOACTION);
    }
  else if (c == '\n')
    {
      Entity *textLink = getReferencedObj(_next);
      Entity *txt = creatStringEntity(_str.c_str(), NULL);
      DPRINT_INFO("pushing the entity %p contening the string %s\n", txt, getStringVal(txt));
      handleEnter(textLink, txt);
      destroyStringEntity(txt);
      return (ACTION);
    }
  return (NOTHANDLE);
}
