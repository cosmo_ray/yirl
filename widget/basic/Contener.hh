/*
**Copyright (C) <2013> <YIRL_Team>
**
**This program is free software: you can redistribute it and/or modify
**it under the terms of the GNU Lesser General Public License as published by
**the Free Software Foundation, either version 3 of the License, or
**(at your option) any later version.
**
**This program is distributed in the hope that it will be useful,
**but WITHOUT ANY WARRANTY; without even the implied warranty of
**MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**GNU General Public License for more details.
**
**You should have received a copy of the GNU Lesser General Public License
**along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef	CONTENER_HH_
#define	CONTENER_HH_

#include	<iostream>
#include	"ElemAlocator.hh"
#include	"Widget.hh"

/**!
**\class classe de base pour conteneur
**/
class Contener : public Widget
{
public:
  enum Position
    {
      BACK,
      FRONT
    };

  enum Type
    {
      VERTICAL,
      HORIZONTAL
    };

  struct Content
  {
    Content(Widget *aCnt, unsigned int op)
      : cnt(aCnt), occupedPourcent(op){}
    Content(Widget *aCnt)
      : cnt(aCnt), occupedPourcent(0){}
    Widget *cnt;
    unsigned int occupedPourcent;
    unsigned int cumulatedPourcent;
    bool operator==(Widget *aCnt)
    {
      return ((*this) == aCnt->getElem());
    }
    bool operator==(const Content &other)
    {
      return ((*this) == other.cnt);
    }
    bool operator==(StructEntity *gameElem)
    {
      return (*cnt == gameElem);
    }
  };

  Contener(StructEntity *gameElem, bool)
    : Widget(CONTENER, gameElem), _current(NULL)
  {
  }

  Contener(StructEntity *gameElem)
    : Widget(CONTENER, gameElem), _current(NULL)
  {
    init();  
  }

  Contener(StructEntity *gameElem, unsigned int x, unsigned int y, unsigned int w, unsigned int h, bool)
    : Widget(CONTENER, gameElem, x, y, w, h), _current(NULL)
  {
  }

  Contener(StructEntity *gameElem, unsigned int x, unsigned int y, unsigned int w, unsigned int h)
    : Widget(CONTENER, gameElem, x, y, w, h), _current(NULL)
  {
    init();
  }

  virtual ~Contener();
  /**!
   **\brief Methode call in constructors contening all the code of the constructors, this methode is use to uniformise constructors code
   */
  void	init();

  /**
   * @param content the struct entity of type "Content"
   */
  void	addContent(StructEntity *content, Position p);

  /**
   * @param gameElem the game elem to rend
   */
  void	addContent(StructEntity *gameElem, unsigned int op ,Position p);
/**!
**\brief remplace le contenue d'une entite par une autre
**\param en 1er parametre l'entite de destination, en second la source
**/
  void	replaceContent(StructEntity *oldElem, StructEntity *newElem);
  /**!
  **\brief retire le contenue d'un gameElem
  **\param le gameElem qui se fait retirer
  **/
  void	removeContent(Widget *gameElem);
  /**!
  **\brief Retire le contenue de gameElem
  **/
  void	removeContent(StructEntity *gameElem);
  /**!
  **\brief Affiche les elements contenues
  **/
  virtual bool render();
  /**!
  ** \brief Redimensionne la fenetre
  **/
  virtual void resize();

protected:
/**!
**\brief Tente de voir sur l'input en parametre fait une acteur sur un element
**\param Un entier de la valeur de l'input
**\return Le statut de l'input apres passage dans le conteneur
**/
  virtual InputStatue tryHandleInput(int ch);
  /**!
  **\brief Recherche l'elementent en parametre dans le conteneur
  **\param L'element recherché
  **\return Pointeur sur l'element
  **/
  virtual Widget    *tryFindElem(StructEntity *elemEntity);
  std::list<Content>::iterator findElem(StructEntity *elemEntity);
  InputStatue selectNextElem();
  /** !
   ** \brief resize the widget and the following of the pointer iterator
   **/
  void	resizing(std::list<Content>::iterator &toAdd);
   /** !
   ** \brief resize the widget and the following of the pointer iterator
   **/
  void	vResizing(Content &toAdd);
   /** !
   ** \brief resize the widget and the following of the pointer iterator
   **/
  void	hResizing(Content &toAdd);
   /** !
   ** \brief resize the widget and the following of the pointer iterator
   **/
  void  globalResizing(Content &toAdd);
  std::list<Content> _contents;
  Widget *_current;
  Type	_t;
  //Widget *_contents[MAX_CONTENT];
};

#endif
