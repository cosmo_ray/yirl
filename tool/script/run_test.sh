#!/bin/sh

parent_dir=.
log_file=$parent_dir/log.txt
ok_log=$parent_dir/logs/no_error.log
file=$parent_dir/logs/$(date +%y%m%d_%H%M).log
execution_sh_file=$parent_dir/yirl.sh

# Compile project
make LINE_FLAGS="-D DEBUG -D LOG_NO_DATE" -f $parent_dir/Makefile

rm $logfile
sh $execution_sh_file
mv $log_file $file
diff $file $ok_log
