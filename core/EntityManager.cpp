/*
**Copyright (C) <2013> <YIRL_Team>
**
**This program is free software: you can redistribute it and/or modify
**it under the terms of the GNU Lesser General Public License as published by
**the Free Software Foundation, either version 3 of the License, or
**(at your option) any later version.
**
**This program is distributed in the hope that it will be useful,
**but WITHOUT ANY WARRANTY; without even the implied warranty of
**MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**GNU General Public License for more details.
**
**You should have received a copy of the GNU Lesser General Public License
**along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include <iostream>
#include <string.h>
#include "EntityManager.hh"
#include "debug.h"
#include "global.h"

#include "yirlAPI.hh"

EntityManager::EntityManager()
{
}

bool	EntityManager::isEntityExisting(const char *objType, const char *objName)
{
  return ((_entitys.count(objType))
	  && (_entitys[objType].count(objName)));
}

Entity	*EntityManager::getEntity(const char *objType, const char *objName)
{
  if (isEntityExisting(objType, objName))
    return _entitys[objType][objName];
  return NULL;
}

void	EntityManager::addInerithContent(Entity *father, Entity **ret, const EntityStructPrototypes::StructPrototype *proto)
{
  const EntityStructPrototypes::StructPrototype *tmp;

  for (unsigned int i = 0; (tmp = proto->getFather(i)) != NULL; ++i) {
    DPRINT_INFO("loop addInerithContent: %s\n", tmp->getName().c_str());
    asignStruct(father, ret, tmp);
  }
}

// TODO: add an index for the begin of the loop in the argument
Entity	**EntityManager::asignStruct(Entity *father, Entity **ret, const EntityStructPrototypes::StructPrototype *proto)
{
  const std::vector<EntityStructPrototypes::ExtandEntityType*> &content = proto->getContent();
  unsigned int len = proto->getContentTotalLength();
  // char buf[10240];

  DPRINT_INFO("assign struct: %s\n", proto->getName().c_str());
  addInerithContent(father, ret, proto);
  for (unsigned int i = proto->getFatherContentLength(); i < len; ++i) {
    unsigned int i2 = i - proto->getFatherContentLength(); 
    LOG("asign %s at %d pos\n", content[i2]->getContentName().c_str(), i);
    if (content[i2]->isStatic()) {
      LOG("asign a static entity");
      ret[i] = genericCreatEntity(STATIC, father, YINT);
      reinterpret_cast<StaticEntity *>(ret[i])->value = proto->getStaticMenber().getEntityByName(content[i2]->getContentName().c_str());
    } else {
      LOG("asign a non static entity");
      if (content[i2]->getType() != ARRAY)
	ret[i] = genericCreatEntity(content[i2]->getType(), father, YINT);
      else
	ret[i] = genericCreatEntity(content[i2]->getType(), father, content[i2]->getArrayInfo()->getType());

      switch (content[i2]->getType()) {
	case STRUCT:
	  reinterpret_cast<StructEntity *>(ret[i])->structName = content[i2]->getStructName().c_str();
	  reinterpret_cast<StructEntity *>(ret[i])->values = allocStruct(content[i2]->getStructName().c_str(), ret[i]);
	  reinterpret_cast<StructEntity *>(ret[i])->len
	    = getPrototypeInstance()->getPrototype(content[i2]->getStructName().c_str())->getContentTotalLength();
	  break;
	default:
	  break;
	}
    }

    LOG("finishing set");
    setEntityBasicInfo(ret[i]
		       , content[i2]->getContentName().c_str()
		       , ret[i]->type
		       , father);
  }

  return ret;
}

Entity	**EntityManager::allocStruct(const char *objType, Entity *father)
{
  // check if the entity is a basic type

  const EntityStructPrototypes::StructPrototype *proto = getPrototypeInstance()->getPrototype(objType);
  int		len = proto->getContentTotalLength();
  Entity	**ret = new Entity *[len];

  DPRINT("creat a %s of %d elems\n", objType, len);
  return (asignStruct(father, ret, proto));
}

/**
 * Add a new basic type. A basic type is a type of entity who doesn't have any parent.
 * @param objType	the name of the type to add
 * @param objName	the name of the entity to create
 * @param type		the type of the entity
 */
Entity	*EntityManager::addBasicType(const char *objType, const char *objName, EntityType type)
{
  if (type == ARRAY) {
    LOG_WARN("ARRAY not handle if not in a struct\n");
  }
  Entity *toCreate = genericCreatEntity(type, NULL, YINT);

  _entitys[objType][objName] = ((Entity *)toCreate);
  return (toCreate);
}

Entity	*EntityManager::addEntity(const char *objType, const char *objName)
{
  int type;

  if (isEntityExisting(objType, objName)) {
    LOG_ERR("this entity already exist %s of type %s", objName, objType);
    return NULL;
  }

  LOG_INFO("add entity %s of type %s\n", objName, objType);
  // Check if the entity to add is a Structure or a basic type
  if ((type = stringToEntityType(objType)) != -1)
    return (addBasicType(objType, objName, static_cast<EntityType>(type)));
  
  // Create the entity
  StructEntity *structure = (StructEntity *)genericCreatEntity(STRUCT, NULL, YINT);
  const EntityStructPrototypes::StructPrototype *proto = getPrototypeInstance()->getPrototype(objType);
  
  setEntityBasicInfo((Entity *)structure, objName, STRUCT, NULL);
  structure->structName = strdup(objType);
  structure->len = proto->getContentTotalLength();
  structure->values = allocStruct(objType, (Entity *)structure);
  structure->prototype = proto;
  _entitys[objType][objName] = ((Entity *)structure);
  return reinterpret_cast<Entity *>(structure);
}

const	std::string *EntityManager::getType(const char *objName) const
{
  std::unordered_map<std::string, std::unordered_map<std::string, Entity *> >::const_iterator it = _entitys.begin();
  const std::unordered_map<std::string, std::unordered_map<std::string, Entity *> >::const_iterator end = _entitys.end();

  while (it != end)
    {
      if (it->second.find(objName) != it->second.end())
	return (&(it->first));
      
      ++it;
    }
  
  return NULL;
}

Entity	*EntityManager::getEntityByName(const char *objName) const
{
  std::unordered_map<std::string, std::unordered_map<std::string, Entity *> >::const_iterator it = _entitys.begin();
  const std::unordered_map<std::string, std::unordered_map<std::string, Entity *> >::const_iterator end = _entitys.end();
  std::unordered_map<std::string, Entity *>::const_iterator toFind;
  std::string tmp = objName;
  size_t dotPos = tmp.find('.');

  if (dotPos != std::string::npos) {
    std::string entityToFind = tmp.substr(0, dotPos);
    std::string subElemToFind = tmp.substr(dotPos + 1, tmp.size() - dotPos);
    Entity *ret;
    
    while (it != end) {
      if ((toFind = it->second.find(entityToFind.c_str())) != it->second.end()) {
	ret = findEntity((toFind->second), subElemToFind.c_str());
	return ret;
      }
      ++it;
    }
  } else {
    while (it != end) {
      if ((toFind = it->second.find(objName)) != it->second.end()) {
	return toFind->second;
      }    
      ++it;
    }
  }
  
  DPRINT_INFO("This entity doesn't exist %s\n", objName);
  return NULL;
}

bool EntityManager::removeEntityByName(const char *objName)
{
  std::unordered_map<std::string, std::unordered_map<std::string, Entity *> >::iterator it = _entitys.begin();
  const std::unordered_map<std::string, std::unordered_map<std::string, Entity *> >::iterator end = _entitys.end();
  std::unordered_map<std::string, Entity *>::iterator toFind;

  while (it != end) {
    if ((toFind = it->second.find(objName)) != it->second.end()) {
      destroyEntity(toFind->second);
      it->second.erase(toFind);
      return true;
    }
    ++it;
  }

  DPRINT_ERR("This entity doesn't exist %s\n", objName);
  return false;
}

EntityManager::~EntityManager()
{
  std::unordered_map<std::string, std::unordered_map<std::string, Entity *> >::iterator it = _entitys.begin();
  const std::unordered_map<std::string, std::unordered_map<std::string, Entity *> >::iterator end = _entitys.end();
  
  while (it != end) {
    std::unordered_map<std::string, Entity *>::iterator it2 = it->second.begin();
    std::unordered_map<std::string, Entity *>::iterator end2 = it->second.end();
    while (it2 != end2) {
      destroyEntity(it2->second);
      ++it;
    }
    it->second.clear();
    ++it;
  }

  _entitys.clear();
}
