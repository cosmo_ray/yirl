/*
**Copyright (C) <2013> <YIRL_Team>
**
**This program is free software: you can redistribute it and/or modify
**it under the terms of the GNU Lesser General Public License as published by
**the Free Software Foundation, either version 3 of the License, or
**(at your option) any later version.
**
**This program is distributed in the hope that it will be useful,
**but WITHOUT ANY WARRANTY; without even the implied warranty of
**MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**GNU General Public License for more details.
**
**You should have received a copy of the GNU Lesser General Public License
**along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef	CWIDGET_HH
#define	CWIDGET_HH

#include	<curses.h>
#include	"Widget.hh"
#include	"debug.h"

class	CWidget
{
public:
  CWidget(const Widget &wid)
    :_wid(wid)
  {
    int h = _wid.getH() * LINES / 1000;
    int w = _wid.getW() * COLS / 1000;
    int x = _wid.getX() * COLS / 1000;
    int y = _wid.getY() * LINES / 1000;
    win = newwin(h, w, y, x);
    wborder(win, '|', '|', '-','-','+','+','+','+');
    DPRINT_INFO("New Win:\nH:%d W:%d X:%d Y:%d\n", h, w, x, y);
  }
  ~CWidget()
  {
    delwin(win);
    DPRINT_INFO("Delete win\n");
  }

  void resize()
  {
    int h = _wid.getH() * LINES / 1000;
    int w = _wid.getW() * COLS / 1000;
    int x = _wid.getX() * COLS / 1000;
    int y = _wid.getY() * LINES / 1000;

    wresize(win, h, w);
    mvwin(win, y, x);
    DPRINT_INFO("resize:\nH:%d W:%d X:%d Y:%d\n", h, w, x, y);
  }
  WINDOW *win;
protected:
  const Widget &_wid;
private:
  CWidget(const CWidget &);
  CWidget &operator=(const CWidget &);
};

#endif
