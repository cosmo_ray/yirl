/*
**Copyright (C) <2013> <YIRL_Team>
**
**This program is free software: you can redistribute it and/or modify
**it under the terms of the GNU Lesser General Public License as published by
**the Free Software Foundation, either version 3 of the License, or
**(at your option) any later version.
**
**This program is distributed in the hope that it will be useful,
**but WITHOUT ANY WARRANTY; without even the implied warranty of
**MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**GNU General Public License for more details.
**
**You should have received a copy of the GNU Lesser General Public License
**along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef	IGUI_HH
#define	IGUI_HH

#include	<list>
#include	"IGameElem.hh"
/*ENUM pour les input. Ce sont les valeurs communes au core et à la GUI*/
enum e_input
{
	UP,
	DOWN,
	RIGHT,
	LEFT,
	ACTION,
	RETURN,
	START
};

/**!
**\class Interface pour les librairie graphique
**/
class	IGui
{
public:
  virtual ~IGui() {}
  //
  //
  //FONCTION QUI JOUE SUR LA FENETRE ACTUELLE
  //
  //
  /**!
  **\brief fonction qui rafraichie la fenetre
  **/
  virtual bool	draw() = 0;
  /*
  fonction qui cree un monde de base
  renvoie l'idendifiant du monde
  */
  virtual int	generateWolrd(std::list<IGameElem*> world) = 0;
  /*set pour la taille de la carte*/
  virtual bool	setMapSize(int x, int y) = 0;
  /*set pour la taille de la fenetre*/
  virtual bool	setWindowSize(int x, int y) = 0;
  /*Mise à jour d'un ou plusieurs elements graphiques, argument potentiel:
  - Liste des elements à update
  */
  virtual bool	updateElems(std::list<IGameElem*> elems) = 0;
  /**
   * Add a graphical element.
   */
  virtual bool	addElem(IGameElem* elem) = 0;
  /**
   * Remove a graphical element.
   * This function dosn't free elem, so it has to be made after
   */
  virtual IGameElem *suppElem(IGameElem* elem) = 0;

  //
  //
  //
  // FONCTION QUI JOUE SUR LES FENETRES CACHER (SAUVEGARDE)
  //
  //
  //
  /*sauvegarde le monde actuel et renvoit son identifiant*/
  virtual	int	saveWorld() = 0;
  /*changer le monde selon l'identifiant*/
  virtual	bool	loadWolrd(int id) = 0;
  /*supprime le monde selon l'identifiant*/
  virtual	bool	suppworld(int id) = 0;
  /*met à jour des elements dans le monde selon identifiant*/
  virtual	bool	updateWolrd(int id, std::list<IGameElem*> elems) = 0;
  /*ajoute des element au monde de l'identifiant en parametre*/
  virtual	bool	addElemWolrd(int id, std::list<IGameElem*> elems) = 0;
  /*supprime des element dans le monde de l'identifiant en parametre*/
  virtual	bool	suppElemWorld(int id, std::list<IGameElem*> elems) = 0;
};

#endif
