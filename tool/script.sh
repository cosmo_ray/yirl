#!/bin/bash


for file in $@; do
    echo -e "\tParam : $file"
    cat $1/header.txt > tmp_file
    cat $file >> tmp_file
    mv tmp_file $file
done
