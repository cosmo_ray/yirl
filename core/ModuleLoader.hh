/*
**Copyright (C) <2013> <YIRL_Team>
**
**This program is free software: you can redistribute it and/or modify
**it under the terms of the GNU Lesser General Public License as published by
**the Free Software Foundation, either version 3 of the License, or
**(at your option) any later version.
**
**This program is distributed in the hope that it will be useful,
**but WITHOUT ANY WARRANTY; without even the implied warranty of
**MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**GNU General Public License for more details.
**
**You should have received a copy of the GNU Lesser General Public License
**along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef		MODULELOADER_HH
#define		MODULELOADER_HH

#include	<iostream>
#include	<string>
#include	<vector>
#include	<list>
#include	"../tool/parsing/parser.h"
#include	"ScriptLua.hh"
#include	"MainEntityManager.hh"
#include	"filesDefines.h"
#include	"CheckJson.hh"
#include	"yirlAPI.hh"


/**
 * load a module
 */
namespace ModuleLoader
{
  /**
   * Options of loadGame and load Content
   */
  enum LoadFlag
    {
      NOTHING = 0,
      NO_SCRIPT = 1 << 0,
      NO_CNT = 1 << 1,
      NO_IMPL = 1 << 2,
    };

  bool loadGame(const std::string &moduleName,  std::vector<Script *>& _scriptManager, int options = 0);
}

#endif
