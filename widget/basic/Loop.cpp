/*
**Copyright (C) <2013> <YIRL_Team>
**
**This program is free software: you can redistribute it and/or modify
**it under the terms of the GNU Lesser General Public License as published by
**the Free Software Foundation, either version 3 of the License, or
**(at your option) any later version.
**
**This program is distributed in the hope that it will be useful,
**but WITHOUT ANY WARRANTY; without even the implied warranty of
**MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**GNU General Public License for more details.
**
**You should have received a copy of the GNU Lesser General Public License
**along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include <sstream>
#include <unistd.h>
#include <iostream>
#include <cstring>
//#include "define_rog.hh"
#include "Gui.hh"
#include "Loop.hh"
#include "yirlAPI.hh"
#include "Menu.hh"
#include "ChangeWidgetEvent.hh"
#include "GameLoop.hh"
#include "global.h"
Loop::Loop()
  : _guiEventStack(GuiEventStack::getInstance()),
    _alocator(ElemAlocator::getInstance()),
    _currentElem(NULL)
{
  Gui::init();
}

Loop::~Loop()
{
  Gui::destroy();
}

#include "iostream"
extern	"C"
{
  ILoop	*entry_point(GameLoop::LibParam param)
  {
    setMana(param.mana);
	setGame(param.gameLoop);
	setPrototypeInstance(param.current);
    return new Loop();
  }
}

bool Loop::render()
{
  return (_currentElem->render());
}

bool Loop::setCurrent(StructEntity *gameElem)
{
  Widget *ret = _alocator.alloc(gameElem);
  if (!ret)
    {
      DPRINT_ERR("gameEntity is NULL\n");
      return false;
    }
  if (_currentElem != NULL)
    delete _currentElem;
  _currentElem = ret;
  return (true);
}

/**
 * Look for an Struct Entity named start and set it as current
 * @return true if the start entity is found, false otherwise
 */
bool Loop::init()
{
  StructEntity *startingPoint = (StructEntity *)getEntityByName("start");
  if (startingPoint == NULL)
    {
      DPRINT_INFO("can not find the entity call start\n");
      return (false);
    }
  setCurrent(startingPoint);
  return (true);
}

void		Loop::execGuiStack()
{
  Event *currentEvent;
  //Widget *current;
  while (!_guiEventStack.empty())
  {
    currentEvent = _guiEventStack.pop();
    switch ((GuiEvent::Type)currentEvent->getType())
    {
      case GuiEvent::CHANGE_WIDGET:
        StructEntity *toChange = reinterpret_cast<ChangeWidgetEvent *>(currentEvent)->getCurrent();
        Widget    *ret = _currentElem->tryFindElem(toChange);
        if (ret != NULL) {
          if (ret->getElem() == _currentElem->getElem())
		  {
            setCurrent(reinterpret_cast<ChangeWidgetEvent *>(currentEvent)->getReplacement());
          }
		  else if (_currentElem->getType() == CONTENER)
		  {
            static_cast<Contener *>(_currentElem)->replaceContent(
              toChange
              , reinterpret_cast<ChangeWidgetEvent *>(currentEvent)->getReplacement());
          }
          //delete _currentElem;
          //_currentElem = 
        }
      break;
    }
    delete currentEvent;
  }
}

bool		tryHandleInput(int c)
{
  if (c == 'q') {
    endGame(true);
    return true;
  }
  return false;
}

bool		Loop::doTurn(void)
{
  static const TimeSetting *ts = getTimeSetting();
  execGuiStack();
  render();
  int c = Gui::getTouch(ts);
  Widget::InputStatue ret = _currentElem->tryHandleInput(c);
  if (ret == Widget::ACTION) {
    return true; //tmp
  }
  else if (ret == Widget::NOTHANDLE) {
    return (tryHandleInput(c));
  }
  return false;
}
