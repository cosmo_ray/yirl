/*
**Copyright (C) <2013> <YIRL_Team>
**
**This program is free software: you can redistribute it and/or modify
**it under the terms of the GNU Lesser General Public License as published by
**the Free Software Foundation, either version 3 of the License, or
**(at your option) any later version.
**
**This program is distributed in the hope that it will be useful,
**but WITHOUT ANY WARRANTY; without even the implied warranty of
**MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**GNU General Public License for more details.
**
**You should have received a copy of the GNU Lesser General Public License
**along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include	<iostream>
#include	<algorithm>  
#include	<curses.h>
#include	<unistd.h>
#include	"Gui.hh"
#include	"GameLoop.hh"

void	Gui::init()
{
  initscr();
  noecho();
  cbreak();
  curs_set(0);
  keypad(stdscr, TRUE);
  start_color();
  init_pair(1, COLOR_BLACK, COLOR_WHITE);
}

void	Gui::destroy()
{
  endwin();
}

/**
 * @return return YIRL corresponding event from curses
 */
int	Gui::getTouch(const TimeSetting *ts)
{
  if (ts->type == TimeSetting::Type::REAL_TIME) {
    timeout(0);
    usleep(ts->val);
  } else {
    timeout(-1);
  }
  return (getch());
}
