/*
**Copyright (C) <2013> <YIRL_Team>
**
**This program is free software: you can redistribute it and/or modify
**it under the terms of the GNU Lesser General Public License as published by
**the Free Software Foundation, either version 3 of the License, or
**(at your option) any later version.
**
**This program is distributed in the hope that it will be useful,
**but WITHOUT ANY WARRANTY; without even the implied warranty of
**MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**GNU General Public License for more details.
**
**You should have received a copy of the GNU Lesser General Public License
**along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include	<algorithm>
#include	<string.h>
#include	"Contener.hh"
#include	"Gui.hh"
#include	"debug.h"

namespace
{
  void	rend(const Contener::Content &cnt)
  {
    cnt.cnt->render();
  }
}

Contener::~Contener()
{
  std::list<Content>::iterator it = _contents.begin();
  const std::list<Content>::iterator end = _contents.end();

  for (; it != end; ++it)
    {
      delete it->cnt;
    }
}

void	Contener::init()
{
  Entity *links = findDirectEntity((Entity *)_elem, "elems"); 
  Entity *type = findDirectEntity((Entity *)_elem, "type");
  int	arrayLen;

  if (links == NULL || type == NULL || getStringVal(type) == NULL)
    {
      if (links == NULL)
	DPRINT_ERR("error when creating Contener in %s, can not find elems\n", tryGetStructEntityName(T_E(_elem)));
      if (type == NULL || getStringVal(type) == NULL)
	DPRINT_ERR("error when creating Contener in %s, can not find type\n", tryGetStructEntityName(T_E(_elem)));
      return;
    }
  if (!strcmp("vertical", getStringVal(type)))
    _t = VERTICAL;
  arrayLen = getLen(links);
  for (int i = 0; i < arrayLen; ++i)
    {
      Entity	*tmp = getEntity(links, i);
      if (tmp == NULL || tmp->type != REF)
	{
	  if (tmp == NULL)
	    {
	      DPRINT_ERR("can not find entity number %d in %s \n", i, tryGetStructEntityName(T_E(_elem)));
	    }
	  else
	    {
	      DPRINT_ERR("Tmp is not a ref but a %s \n", entityTypeToString(tmp->type));
	    }
	  continue;
	}
      tmp = getReferencedObj(tmp);
      addContent(((StructEntity *)tmp), BACK);
    }
}

void	Contener::addContent(StructEntity *content, Position p)
{
  if (content == NULL)
    {
      DPRINT_ERR("Could not add a NULL entity\n");
      return ;
    }
  addContent(((StructEntity *)getReferencedObj(findDirectEntity((Entity *)content, "elem")))
	     , getIntVal(findDirectEntity(((Entity *)content), "occupedPourcent"))
	     , p);
}


void	Contener::addContent(StructEntity *gameElem, unsigned int op, Contener::Position p)
{
  Widget *toAdd = ElemAlocator::getInstance().alloc(gameElem);

  DPRINT_INFO("Contener::addContent");
  std::list<Content>::iterator	addedElem;
  
  if (toAdd == NULL)
    {
      DPRINT_ERR("can not add %s widget\n", tryGetStructEntityName(T_E(gameElem)));
      return;
    }
  _current = toAdd;
  if (p == BACK)
    {
      _contents.push_back(Content(toAdd, op));
      addedElem = _contents.end();
      --addedElem;
    }
  else
    {
      _contents.push_front(Content(toAdd, op));
      addedElem = _contents.begin();
    }
  DPRINT_INFO("Added Element: cnt:%s\nOP:%d\n"
	      , tryGetStructEntityName(T_E(addedElem->cnt->getElem()))
	      , addedElem->occupedPourcent);
  resizing(addedElem);
}

void	Contener::replaceContent(StructEntity *oldElem, StructEntity *newElem)
{
  if (oldElem == NULL || newElem == NULL)
    {
      DPRINT_ERR("Content::replaceContent: could not replace a NULL element");
      return;
    }

  std::list<Content>::iterator it = findElem(oldElem);
  if (it == _contents.end())
    {
      DPRINT_ERR("Content::replaceContent: could not find oldElem");
      return;
    }
  Widget *newWidget = ElemAlocator::getInstance().alloc(newElem, it->cnt->getX(), it->cnt->getY(), it->cnt->getW(), it->cnt->getH());

  if ((*_current) == oldElem)
    _current = newWidget;
  delete it->cnt;
  it->cnt = newWidget;
}

void	Contener::removeContent(StructEntity *gameElem)
{
  if (gameElem == NULL)
    {DPRINT_ERR("could not remove a NULL element");}
  std::list<Content>::iterator it = _contents.begin();
  const std::list<Content>::iterator end = _contents.end();

  if ((*_current) == gameElem)
    _current = NULL;
  std::remove(it, end, gameElem);
}

void	Contener::removeContent(Widget *gameElem)
{
  removeContent(gameElem->getElem());
}

bool	Contener::render()
{
  DPRINT_INFO("%d", _contents.size());
  for_each(_contents.begin(), _contents.end(), rend);
  return true;
}

Widget::InputStatue Contener::selectNextElem()
{
  if (_current == NULL)
    return (NOACTION);
  std::list<Content>::iterator it = find (_contents.begin(), _contents.end(), _current);
  if (it == _contents.end())
    return (NOACTION);
  ++it;
  if (it == _contents.end())
    it = _contents.begin();
  _current = it->cnt;
  return (NOACTION);
}

Widget::InputStatue Contener::tryHandleInput(int c)
{
  if (_current != NULL)
    {
      Widget::InputStatue ret = _current->tryHandleInput(c);
      if (ret != NOTHANDLE)
	return (ret);
      else if (c == '\t')
	return (selectNextElem());
    }
  return NOTHANDLE;
}

std::list<Contener::Content>::iterator Contener::findElem(StructEntity *elemEntity)
{
  if (elemEntity == NULL)
    std::cerr << "could not find a NULL element" << std::endl;
  return (std::find(_contents.begin(), _contents.end(), elemEntity));
}

Widget    *Contener::tryFindElem(StructEntity *elemEntity)
{
  if (elemEntity == NULL)
    std::cerr << "could not find a NULL element" << std::endl;
  std::list<Content>::iterator it = _contents.begin();
  const std::list<Content>::iterator end = _contents.end();
  it = std::find(it, end, elemEntity);
  if (it == end)
    return (Widget::tryFindElem(elemEntity));
  return (it->cnt);
}

// Cross-multiplication :)
// this  other
// 100   80 
void	Contener::vResizing(Content &toResize)
{
  toResize.cnt->setW(_w);
  toResize.cnt->setH((_h * toResize.occupedPourcent) / 100);
  toResize.cnt->setX(_x);
  toResize.cnt->setY((_h * (toResize.cumulatedPourcent - toResize.occupedPourcent) / 100) + _y);
  DPRINT_INFO("V resize widget to this size\nH:%d\nW:%d\nX:%d\nY:%d\n%d pourcent ocuped\ncumulated pourcent: %d\n"
	      , toResize.cnt->getH(), toResize.cnt->getW(), toResize.cnt->getX(), toResize.cnt->getY()
	      , toResize.occupedPourcent
	      , toResize.cumulatedPourcent);
}

void	Contener::hResizing(Content &toResize)
{
  toResize.cnt->setH(_h);
  toResize.cnt->setW((_w * toResize.occupedPourcent) / 100);
  toResize.cnt->setY(_y);
  toResize.cnt->setX((_w * (toResize.cumulatedPourcent - toResize.occupedPourcent) / 100) + _x);
  DPRINT_INFO("H resize widget to this size\nH:%d\nW:%d\nX:%d\nY:%d\n%d pourcent ocuped\ncumulated pourcent: %d\n",
	      toResize.cnt->getH(), toResize.cnt->getW(), toResize.cnt->getX(), toResize.cnt->getY()
	      , toResize.occupedPourcent
	      , toResize.cumulatedPourcent);
}

void	Contener::globalResizing(Content &toResize)
{
  if (_t == VERTICAL)
    vResizing(toResize);
  else
    hResizing(toResize);
  toResize.cnt->resize();
}

void	Contener::resizing(std::list<Content>::iterator &toResize)
{
  std::list<Content>::iterator it = toResize;
  const std::list<Content>::iterator end = _contents.end();  

  DPRINT_INFO("resizing in a contener of size\nH:%d\nW:%d\nX:%d\nY:%d\n", _h, _w, _x, _y);
  toResize->cumulatedPourcent = toResize->occupedPourcent;
  // if (it != _contents.begin())
  if (it != _contents.begin())
    {
      --it;
      toResize->cumulatedPourcent += it->cumulatedPourcent;
    }

  for (it = _contents.begin(); it != toResize; ++it) {
    globalResizing(*it);
  }
  
  globalResizing(*toResize);
  ++it;

  for (; it != end; ++it) {
      it->cumulatedPourcent = it->occupedPourcent + toResize->cumulatedPourcent;
      globalResizing(*it);
      ++toResize;
    }
}

void Contener::resize()
{
}
