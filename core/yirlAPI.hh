/*
**Copyright (C) <2013> <YIRL_Team>
**
**This program is free software: you can redistribute it and/or modify
**it under the terms of the GNU Lesser General Public License as published by
**the Free Software Foundation, either version 3 of the License, or
**(at your option) any later version.
**
**This program is distributed in the hope that it will be useful,
**but WITHOUT ANY WARRANTY; without even the implied warranty of
**MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**GNU General Public License for more details.
**
**You should have received a copy of the GNU Lesser General Public License
**along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef	CORECALL_HH
#define CORECALL_HH
#include	"MainEntityManager.hh"

#include	"entity.h"
#include	<stdarg.h>
/*Fonction  intermediere voir MainEntityManager et Game Loop pour avoir les fonctions au dessus*/

struct TimeSetting;
extern "C"
{
  /**
   * Load the odule with the given name
   * @moduleName the name of the module
   */
  bool		loadModule(const char *moduleName);
/*Use to keep an instance of the original game loop*/  
class GameLoop;
  void	setGame(GameLoop *gameL);
  void	initGame(GameLoop *gameL);

   /**
     * Look for the entity ::objName:: with the type ::objType:: in EntityManager 
     * @return The entity if found, NULL otherwise
     */
  Entity	*getObject(const char *objType, const char *objName) WEAK;
  /**
   * Look for the entity ::objName:: in EntityManager 
   * @return The entity if found, NULL otherwise
   */
  Entity	*getEntityByName(const char *objName) WEAK;

  const TimeSetting *getTimeSetting() WEAK;
  void setTimeType(int t) WEAK;

  void setFunctionPrototype(Entity *entity, const EntityStructPrototypes::ExtandEntityType *value);


  /**
   * @param[in] end true to end the game
   */
  void		endGame(bool end) WEAK;
  //void		pushEvent(FunctionType type) __attribute__((weak));

  /**
   * @return the name of the graphical lib to load
   * this function is a core call, because the graphicall library will be load by the widget lib
   */
  const		char *getGraphicalLibName() WEAK;

  /**
   * The function entity in argument
   * @return the return of the function call
   */

  Entity	*addEntity(const char *objType, const char *objName);

  void		pushChangeWidgetEvent(StructEntity *cur, StructEntity *newWidget);

  Entity  *vCallByName(const char *name, int nbArg, va_list *ap) WEAK;
  Entity  *callByName(const char *name, int nbArg, ...) WEAK;
  
  
  Entity	*call(FunctionEntity *function, ...) WEAK;
  /**
   * As call but take a va_list pointer in parameter
   */
  Entity	*callWithVA(FunctionEntity *function, va_list *ap) WEAK;
  /*
  * Use this to get MainEntityManager
  */
  MainEntityManager	*getMainEntityManager();

  /**
   * Set MainEntityManager on windows, do nothing on unix
   */
  void	setMana(MainEntityManager *aMana);

  /* initiallize mana (mainEntityManager*/
  void	initMana();

  std::string	*formatString(const char *str);

  int	getResX() WEAK;
  int	getResY() WEAK;

  // void	dprint_err(char *str, ...)
  // {
  // }
} // close extern "C"

#endif
