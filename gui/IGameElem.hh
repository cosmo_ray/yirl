/*
**Copyright (C) <2013> <YIRL_Team>
**
**This program is free software: you can redistribute it and/or modify
**it under the terms of the GNU Lesser General Public License as published by
**the Free Software Foundation, either version 3 of the License, or
**(at your option) any later version.
**
**This program is distributed in the hope that it will be useful,
**but WITHOUT ANY WARRANTY; without even the implied warranty of
**MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**GNU General Public License for more details.
**
**You should have received a copy of the GNU Lesser General Public License
**along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef	IGAME_ELEM
#define	IGAME_ELEM
/*CETTE CLASSE NE CONTIENT QUE LES PROTOTYPE DE FONCTION UTILE A LA PARTIE GRAPHIQUE
POUR LA PARTIE IL FAUT LA METTRE A JOUR*/

#define	RETRYPE(TYPE)	int	getType()
/**!
**\class Classe representant un element de jeu
**/
class	IGameElem
{
public:
/**!
**\class Interface de base pour la taille d'un element
**/
  struct ISize
  {
    enum Type
      {
	RECT
      };
    ISize(int _type) : type(_type){}
    int type;
  };
/**!
**\class Taille en rectangle d'un element
**/
    struct Rect : ISize
  {
    Rect():ISize(RECT){}
    Rect(unsigned int aX, unsigned int aY, unsigned int aW, unsigned int aH)
      : ISize(RECT), x(aX), y(aY), w(aW), h(aH) {}
    unsigned int x;
    unsigned int y;
    unsigned int w;
    unsigned int h;
  };

  #define	LAST_CONTENER_ELEM TEXT_SCREEN

  /**
   * A Contener is a element contening others Elems
   */
  enum Type
    {
      // Contener types
      MENU,
      TEXT_SCREEN,
      // Other
      BUTTON,
      SPRITE
    };
  virtual ~IGameElem(){}
  
  /**!
  **\brief fonction qui retourne ID de element(pour image)
  **\return en int qui represente identifiant de l'element
  **/
  virtual int	getIdBackground() = 0;
  /**!
  **\brief Fonction qui retourne sont identifiant graphique, c'est à dire ca representation graphique pour la librairie (MENU, TEXT_SCREN ...)
  **\return un int qui est l'identifiant du type
  **/
  virtual int	getType() = 0;
  /**!
   **\brief Retourne la position de l'élement grahique sous forme de ISize, l'element retourné doit etre delete car il est alloué dynamiquement.
   **\return un object ISIZE
   **/
  virtual	ISize	*getPos() = 0;
  /**!
  **\brief Renvoie sa position en tant que Z sur la carte
  **\return un entier representant la valeur en Z (profondeur) sur la carte
  **/
  virtual int	getZ() = 0;
};

#endif
