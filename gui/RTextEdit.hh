/*
**Copyright (C) <2013> <YIRL_Team>
**
**This program is free software: you can redistribute it and/or modify
**it under the terms of the GNU Lesser General Public License as published by
**the Free Software Foundation, either version 3 of the License, or
**(at your option) any later version.
**
**This program is distributed in the hope that it will be useful,
**but WITHOUT ANY WARRANTY; without even the implied warranty of
**MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**GNU General Public License for more details.
**
**You should have received a copy of the GNU Lesser General Public License
**along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef	RTEXTEDIT_HH
#define	RTEXTEDIT_HH

#include	"macro.h"
#include	"TextEdit.hh"
/**!
**\class class pour afficher du text
**/
class	RTextEdit : public TextEdit
{
public:
  RTextEdit(StructEntity *gameElem)
    : TextEdit(gameElem)
  {
    init();
  }

  RTextEdit(StructEntity *gameElem, unsigned int x, unsigned int y, unsigned int w, unsigned int h)
    : TextEdit(gameElem, x, y, w, h)
  {
    init();
  }
/**!
**\brief initilise le texte
**/
  void	init();
  virtual ~RTextEdit();
/**!
**\brief affiche le text sur l'ecran
**/
  virtual bool render();
  /**!
  **\brief Redimensionne le text
  **/
  virtual void resize();
protected:
private:
  // pointer use to permit external library to have they own memory inside a RTestEdit
  void	*_helper;
};

#endif
