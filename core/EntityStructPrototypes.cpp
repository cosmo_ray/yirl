/*
**Copyright (C) <2013> <YIRL_Team>
**
**This program is free software: you can redistribute it and/or modify
**it under the terms of the GNU Lesser General Public License as published by
**the Free Software Foundation, either version 3 of the License, or
**(at your option) any later version.
**
**This program is distributed in the hope that it will be useful,
**but WITHOUT ANY WARRANTY; without even the implied warranty of
**MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**GNU General Public License for more details.
**
**You should have received a copy of the GNU Lesser General Public License
**along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include	<iostream>
#include	"debug.h"
#include	"EntityManager.hh"
#include "global.h"
EntityStructPrototypes::EntityStructPrototypes()
{
}

EntityStructPrototypes::~EntityStructPrototypes()
{
}

EntityStructPrototypes &EntityStructPrototypes::getInstance()
{
  static	EntityStructPrototypes ret;

  return ret;
}

bool	EntityStructPrototypes::checkStructPrototype(const Entity &entity)
{
  std::unordered_map<std::string, StructPrototype *>::iterator it = _prototypes.find(entity.name);

  if (it != _prototypes.end())
    return (false);
  return (true);
}

bool	EntityStructPrototypes::isStructExisting(const std::string &structName) const
{
  return (_prototypes.count(structName));
}

#include <iostream>
EntityStructPrototypes::StructPrototype *EntityStructPrototypes::getPrototype(const char *structName)
{
  if (isStructExisting(structName))
    return (_prototypes.at(structName));
  return (NULL);
}

EntityStructPrototypes::StructPrototype	*EntityStructPrototypes::addStruct(const std::string &structName)
{
  return (addStruct(structName.c_str()));
}

EntityStructPrototypes::StructPrototype	*EntityStructPrototypes::addStruct(const char *structName)
{
  if (isStructExisting(structName))
    return (NULL);
  DPRINT("add struct: %s\n", structName);
  StructPrototype *toInser = new StructPrototype(structName);
  _prototypes.insert(std::make_pair(structName, toInser));
  return (_prototypes.at(structName));
}

void	EntityStructPrototypes::rmStruct(const char *structName)
{
  _prototypes.erase(structName);
}
