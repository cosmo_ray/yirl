#ifndef CHANGE_WIDGET_EVENT_HH
#define CHANGE_WIDGET_EVENT_HH

#include	"GuiEvent.hh"
#include	"entity.h"
/*Classe pour changer d'evenement en cas de besoin*/
class ChangeWidgetEvent : public GuiEvent
{
public:
  ChangeWidgetEvent(StructEntity *current, StructEntity *replacement);
  virtual ~ChangeWidgetEvent(){}

  StructEntity *getCurrent();
  StructEntity *getReplacement();
private:
  StructEntity *_current;
  StructEntity *_replacement;
};


#endif
