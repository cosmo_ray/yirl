/*
**Copyright (C) <2013> <YIRL_Team>
**
**This program is free software: you can redistribute it and/or modify
**it under the terms of the GNU Lesser General Public License as published by
**the Free Software Foundation, either version 3 of the License, or
**(at your option) any later version.
**
**This program is distributed in the hope that it will be useful,
**but WITHOUT ANY WARRANTY; without even the implied warranty of
**MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**GNU General Public License for more details.
**
**You should have received a copy of the GNU Lesser General Public License
**along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include	<stdio.h>
#include	"rule.h"

extern fileBuffers	bufferManager;

static inline int	get_index(int file_id)
{
  return (bufferManager.buffers[file_id]->index);
}

static inline char	*get_buf(int file_id)
{
  return (bufferManager.buffers[file_id]->cur_buf);
}

static inline int	get_buff_size(int file_id)
{
  return (bufferManager.buffers[file_id]->buff_size);
}

static inline fileBuffer *get_buffer(int file_id)
{
  return (bufferManager.buffers[file_id]);
}


static	int	try_reread(int file_id)
{
  if ((get_index(file_id)) >= get_buff_size(file_id))
    {
      if (parser_read_next_block(file_id) < 1)
	return 0;
    }
  return 1;
}

static int	check_jumpable(fileBuffer *buffer)
{
  int	i;
  int	jumpable_char_len = strlen(buffer->jumpable_char);

  for (i = 0; i < jumpable_char_len; ++i)
    {
      if (buffer->cur_buf[buffer->index] == buffer->jumpable_char[i])
	return (1);
    }
  return (0);
}

static void	jmp_jmupable_character(int file_id)
{  
  fileBuffer	*buffer = get_buffer(file_id);
  int	ret;

  if ((get_index(file_id)) >= get_buff_size(file_id))
    {
      ret = parser_read_next_block(file_id);
      if (ret < 0)
	return;
      if (ret == 0)
	{
	  parser_validate_buffer(buffer);
	  return;
	}
    }
  if (!check_jumpable(buffer))
    {
      parser_validate_buffer(buffer);
      return;
    }
  parser_incr_index(buffer);
  return (jmp_jmupable_character(file_id));
}

int	parser_check_null(int file_id)
{
  jmp_jmupable_character(file_id);
  if ((get_index(file_id)) >= get_buff_size(file_id))
    {
      if (parser_read_next_block(file_id) < 1)
	return (0);
    }
  if (get_buf(file_id)[get_index(file_id)] == '\0')
    {
      parser_incr_index(get_buffer(file_id));
      return (parser_validate_buffer(get_buffer(file_id)));
    }
  return(parser_restor_buffer(get_buffer(file_id)));
}

int	parser_check_char_nj(int file_id, char c)
{
  if ((get_index(file_id)) >= get_buff_size(file_id))
    {
      if (parser_read_next_block(file_id) < 1)
	return (0);
    }
  /* putchar(get_buf(file_id)[get_index(file_id)]); */
  /* putchar('\n'); */
  if (get_buf(file_id)[get_index(file_id)] == c)
    {
      parser_incr_index(get_buffer(file_id));
      return (1);
    }
  return(0);
}

char	*parser_get_interval_string(int file_id, char a, char b, char *str)
{
  jmp_jmupable_character(file_id);
  return (parser_get_interval_string_nj(file_id, a, b, str));
}

char	*parser_get_interval_string_nj(int file_id, char a, char b, char *str)
{
  int	i = 0;

  while (parser_check_interval_char_nj(file_id, a, b))
    {
      str[i] = get_buf(file_id)[get_index(file_id)];
      parser_incr_index(get_buffer(file_id));
      ++i;
    }
  if (i)
    {
      parser_validate_buffer(get_buffer(file_id));      
      str[i] = 0;
      return (str);
    }
  return (NULL);
}

char	*parser_get_alfa_str(int file_id, char *str)
{
  int	i = 0;

  jmp_jmupable_character(file_id);
  while (try_reread(file_id) && (parser_check_interval_char_nj(file_id, 'a', 'z') || parser_check_interval_char_nj(file_id, 'A', 'Z')))
    {
      str[i] = get_buf(file_id)[get_index(file_id)];
      parser_incr_index(get_buffer(file_id));
      ++i;      
    }
  if (i)
    {
      parser_validate_buffer(get_buffer(file_id));      
      str[i] = 0;
      return (str);
    }
  return (NULL);
}

char	*parser_get_alfanumeric_str(int file_id, char *str)
{
  int	i = 0;

  jmp_jmupable_character(file_id);
  while (try_reread(file_id) && (parser_check_interval_char_nj(file_id, 'a', 'z') || parser_check_interval_char_nj(file_id, 'A', 'Z') || parser_check_interval_char_nj(file_id, '0', '9')))
    {
      str[i] = get_buf(file_id)[get_index(file_id)];
      parser_incr_index(get_buffer(file_id));
      ++i;
    }
  if (i)
    {
      parser_validate_buffer(get_buffer(file_id));      
      str[i] = 0;
      return (str);
    }
  return (NULL);
}

static	int get_num_str(int i, char *str, int file_id)
{
  if (parser_check_interval_char_nj(file_id, '0', '9') && try_reread(file_id))
    {
      str[i] = get_buf(file_id)[get_index(file_id)];
      parser_incr_index(get_buffer(file_id));
      return (get_num_str(i + 1, str, file_id));
    }
  else
    return (i);
}

char	*parser_get_float_str(int file_id, char *str)
{
  int i;
  int i2;

  jmp_jmupable_character(file_id);
  if ((i = get_num_str(0, str, file_id))
      && parser_check_char_nj(file_id, 'c')
      && (i2 = get_num_str(0, str, file_id)))
    {
      parser_validate_buffer(get_buffer(file_id));
      str[i + i2 + 1] = 0;
      return (str);
    }
  parser_restor_buffer(get_buffer(file_id));
  return (NULL);
}


char	*parser_get_int_str(int file_id, char *str)
{
  int i;

  jmp_jmupable_character(file_id);
  if ((i = get_num_str(0, str, file_id)))
    {
      parser_validate_buffer(get_buffer(file_id));      
      str[i] = 0;
      return (str);
    }
  parser_restor_buffer(get_buffer(file_id));
  return (NULL);
}


int	parser_check_char(int file_id, char c)
{
  jmp_jmupable_character(file_id);
  if (parser_check_char_nj(file_id, c))
    {
      return (parser_validate_buffer(get_buffer(file_id)));
    }
  return (parser_restor_buffer(get_buffer(file_id)));
}

int	parser_check_interval_char_nj(int file_id, char a, char b)
{
  if ((get_index(file_id)) >= get_buff_size(file_id))
    {
      if (parser_read_next_block(file_id) < 1)
	return (0);
    }
  if (get_buf(file_id)[get_index(file_id)] >= a && get_buf(file_id)[get_index(file_id)] <= b)
    {
      return (1);
    }
  return(0);
}

int	parser_check_interval_char(int file_id, char a, char b)
{
  jmp_jmupable_character(file_id);
  return (parser_check_interval_char_nj(file_id, a, b));
}

int	parser_is_numeric(int file_id)
{
  return (parser_check_interval_char(file_id, '0', '9'));
}

char	parser_check_chars(int file_id, const char *str)
{
  size_t	i;
  size_t	len = strlen(str);

  jmp_jmupable_character(file_id);
  if ((get_index(file_id)) >= get_buff_size(file_id))
    {
      if (parser_read_next_block(file_id) < 1)
	return (0);
    }
  for (i = 0; i < len; ++i)
    {
      if (get_buf(file_id)[get_index(file_id)] == str[i])
	{
	  parser_incr_index(get_buffer(file_id));
	  parser_validate_buffer(get_buffer(file_id));
	  return (str[i]);
	}
    }
  return (parser_restor_buffer(get_buffer(file_id)));
}

int	parser_check_str(int file_id, const char *str)
{
  int	i;
  int	len = strlen(str);
  fileBuffer *buffer = get_buffer(file_id);

  jmp_jmupable_character(file_id); 
  for (i = 0; i < len; ++i)
    {
      if (!parser_check_char_nj(file_id, str[i]))
	{
	  return (parser_restor_buffer(buffer));
	}
    }
  return (parser_validate_buffer(buffer));
}

void	parser_add_jumpable_char(int file_id, char c)
{
  fileBuffer *buffer = get_buffer(file_id);
  int	idx = strlen(buffer->jumpable_char);

  buffer->jumpable_char[idx] = c;
  buffer->jumpable_char[idx + 1] = '\0';
}

void	parser_set_jumpable_chars(int file_id, const char *str)
{
  fileBuffer *buffer = get_buffer(file_id);
  int	len_str = strlen(str);
  int i;

  for (i = 0; i < len_str; ++i)
    {
      buffer->jumpable_char[i] = str[i];
    }
  buffer->jumpable_char[i] = '\0';
}


void	parser_reset_jumpable_char(int file_id)
{
  fileBuffer *buffer = get_buffer(file_id);

  buffer->jumpable_char[0] = '\0';
}

char	*parser_get_next_word_nj(int file_id, char *buf)
{
  int	i = 0;
  while (try_reread(file_id) && (!check_jumpable(get_buffer(file_id))))
    {
      buf[i] = get_buf(file_id)[get_index(file_id)];
      parser_incr_index(get_buffer(file_id));
      ++i;
    }
  if (!i)
    return (NULL); // don't need to restore because the index dosn't move since the last jmp.
  buf[i] = '\0';
  parser_validate_buffer(get_buffer(file_id));
  return (buf);
}

char	*parser_get_next_word(int file_id, char *buf)
{
  jmp_jmupable_character(file_id);
  return (parser_get_next_word_nj(file_id, buf));
}

int	parser_is_end_file(int file_id)
{
  jmp_jmupable_character(file_id);
  if (get_buffer(file_id)->buff_size == 0)
    return (1);
  return (0);
}
