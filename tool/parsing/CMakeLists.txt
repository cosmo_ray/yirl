file (
  GLOB
  parser_SRCS
  *.c
)

set(CMAKE_CFLAGS "${CMAKE_CFLAGS} -W -Wall")

add_library(
  parser
  SHARED
  ${parser_SRCS}
)
install ( TARGETS parser DESTINATION ${LIB_INSTALL_DIR} )