/*
**Copyright (C) <2013> <YIRL_Team>
**
**This program is free software: you can redistribute it and/or modify
**it under the terms of the GNU Lesser General Public License as published by
**the Free Software Foundation, either version 3 of the License, or
**(at your option) any later version.
**
**This program is distributed in the hope that it will be useful,
**but WITHOUT ANY WARRANTY; without even the implied warranty of
**MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**GNU General Public License for more details.
**
**You should have received a copy of the GNU Lesser General Public License
**along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef RIMG_SCREEN_HLP
#define RIMG_SCREEN_HLP

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include "CWidget.hh"

#define DEFAULT_SIZE 64

class RImgScreenHlp : public CWidget
{
public:
  SDL_Surface *img;
  
public:
  RImgScreenHlp(const Widget &wid, const char *path)
    :CWidget(wid), img(NULL)
  {
    if (path) {
      img = IMG_Load(path);
    }
  }

  ~RImgScreenHlp()
  {
    SDL_FreeSurface(img);
  }

};

#endif
