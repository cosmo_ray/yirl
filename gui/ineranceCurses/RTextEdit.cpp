/*
**Copyright (C) <2013> <YIRL_Team>
**
**This program is free software: you can redistribute it and/or modify
**it under the terms of the GNU Lesser General Public License as published by
**the Free Software Foundation, either version 3 of the License, or
**(at your option) any later version.
**
**This program is distributed in the hope that it will be useful,
**but WITHOUT ANY WARRANTY; without even the implied warranty of
**MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**GNU General Public License for more details.
**
**You should have received a copy of the GNU Lesser General Public License
**along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include	<cstring>
#include	"RTextEdit.hh"
#include	"CWidget.hh"

void	RTextEdit::init()
{
  CWidget **tmp = ((CWidget **)&_helper);
  *tmp = new CWidget(*this);
}

RTextEdit::~RTextEdit()
{
    delete reinterpret_cast<CWidget *>(_helper);
}

bool RTextEdit::render()
{
  CWidget *wid = reinterpret_cast<CWidget *>(_helper);
  int x,y ,h,w;

  wmove(wid->win,0,0);
  getyx(wid->win, y, x);
  getmaxyx(wid->win, h, w);
  w -= x;
  mvwaddstr(wid->win, 0, w/2, "Text Input:");
  mvwaddstr(wid->win, 1, 1, _str.c_str());
  waddch(wid->win,' ');
  wrefresh(wid->win);
  return (true);
}

void RTextEdit::resize()
{
  CWidget *wid = reinterpret_cast<CWidget *>(_helper);

  wid->resize();
}
