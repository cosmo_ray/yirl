#include <stdio.h>
#include <string>
#include "gtest/gtest.h"
#include "../../yirl/tool/parsing/parser.h"

// TEST(TestName, TestCase)
TEST(ParserTest, InitLib)
{
	extern	fileBuffers	bufferManager;	// Get bufferManager from init.cpp
	parser_init_lib();					// Must init every time (that's why it is the first test)

	// EXPECT_EQ(val1, val2) mean :  IS val1 = val2 ?
	EXPECT_EQ(0, bufferManager.size);
	EXPECT_EQ(NULL, bufferManager.buffers);
}

TEST(ParserTest, OpenFile)
{
	parser_init_lib();
	EXPECT_EQ(-1, parser_open_file("Unknow"));

	// EXPECT_NE(val1, val2) mean :  IS val1 =! val2 ?
	EXPECT_NE(-1, parser_open_file("test.txt"));
	parser_close_file(0);
}

TEST(ParserTest, CloseFile)
{
	extern	fileBuffers	bufferManager;
	parser_init_lib();

	parser_open_file("test.txt");
	//EXPECT_NE(NULL, bufferManager.buffers[0]);  Unsupported
	
	parser_close_file(0);
	EXPECT_EQ(NULL, bufferManager.buffers[0]);
}

TEST(ParserTest, CloseLib)
{
	extern	fileBuffers	bufferManager;
	parser_init_lib();

	parser_open_file("test.txt");
	parser_open_file("test2.txt");
	parser_close_lib();
	EXPECT_EQ(NULL, bufferManager.buffers);
}

// ALL IN STATIC
/*TEST(ParserTest, FindUnalocatePlace)
{
	extern	fileBuffers	bufferManager;
	parser_init_lib();
	EXPECT_EQ(-1, parser_find_unalocate_place());
	parser_open_file("test.txt");
	parser_close_file(0);
	EXPECT_EQ(0, parser_find_unalocate_place());
}

TEST(ParserTest, GetPlace)
{
	extern	fileBuffers	bufferManager;
	parser_init_lib();
	EXPECT_EQ(0, parser_get_place());
	parser_open_file("test.txt");
	parser_open_file("test2.txt");
	parser_close_file(1);
	EXPECT_EQ(1, parser_get_place());
	parser_open_file("test2.txt");
	EXPECT_EQ(2, parser_get_place());
}*/

TEST(ParserTest, ReadNextBlock)
{
	parser_init_lib();

	parser_open_file("test.txt");
	EXPECT_EQ(0, parser_read_next_block(0));
	parser_open_file("test2.txt");
	EXPECT_NE(0, parser_read_next_block(1));
	parser_close_file(1);
	parser_close_file(0);
}

TEST(ParserTest, RestorBuffer)
{
	extern	fileBuffers	bufferManager;
	parser_init_lib();

	parser_open_file("test.txt");
	EXPECT_EQ(0, parser_restor_buffer(bufferManager.buffers[0]));
	parser_read_next_block(0);
	parser_restor_buffer(bufferManager.buffers[0]);
	EXPECT_EQ(-1, bufferManager.buffers[0]->old_buffer);
	parser_close_file(0);
}

TEST(ParserTest, ValidateBuffer)
{
	extern	fileBuffers	bufferManager;
	parser_init_lib();

	parser_open_file("test.txt");
	parser_validate_buffer(bufferManager.buffers[0]);
	EXPECT_EQ(0, bufferManager.buffers[0]->change_buffer);
	EXPECT_EQ(-1, bufferManager.buffers[0]->old_buffer);
	parser_close_file(0);
}

TEST(ParserTest, IncrIndex)
{
	extern	fileBuffers	bufferManager;
	parser_init_lib();

	parser_open_file("test.txt");
	int i = bufferManager.buffers[0]->index;
	parser_incr_index(bufferManager.buffers[0]);
	// EXPECT_LT(val1, val2) mean :  IS val1 < val2 ?
	EXPECT_LT(i, bufferManager.buffers[0]->index);
	parser_close_file(0);
}

//   /!\ Weird function. 
TEST(ParserTest, CheckNull)
{
	parser_init_lib();

	parser_open_file("test.txt");
	int i = parser_check_null(0);
	EXPECT_EQ(0, i);
	parser_close_file(0);
	parser_open_file("test3.txt");
	i = parser_check_null(0);
	EXPECT_EQ(1, i);
	parser_close_file(0);

}

TEST(ParserTest, CheckChar)
{
	parser_init_lib();

	parser_open_file("test.txt");
	int i = parser_check_char(0, 'T');
	EXPECT_EQ(1, i);
	i = parser_check_char(0, 'A');
	EXPECT_EQ(0, i);
	parser_close_file(0);
}

TEST(ParserTest, CheckCharNj)
{
	parser_init_lib();

	parser_open_file("test.txt");
	int i = parser_check_char_nj(0, 'T');
	EXPECT_EQ(1, i);
	i = parser_check_char_nj(0, 'A');
	EXPECT_EQ(0, i);
	parser_close_file(0);
	parser_open_file("test4.txt");
	i = parser_check_char_nj(0, 'C');
	EXPECT_EQ(0, i);
	i = parser_check_char_nj(0, 'A');
	EXPECT_EQ(0, i);
	parser_close_file(0);
}

TEST(ParserTest, CheckChars)
{
	parser_init_lib();

	parser_open_file("test.txt");
	std::string a = "Ty";
	int i = parser_check_chars(0, a.c_str());
	EXPECT_EQ(84, i);
	a = "wy";
	i = parser_check_chars(0, a.c_str());
	EXPECT_EQ(0, i);
	parser_close_file(0);
}

TEST(ParserTest, CheckStr)
{
	parser_init_lib();

	parser_open_file("test.txt");
	std::string a = "This";
	int i = parser_check_str(0, a.c_str());
	EXPECT_EQ(1, i);
	a = "tarbouk";
	i = parser_check_str(0, a.c_str());
	EXPECT_EQ(0, i);
	parser_close_file(0);
}

TEST(ParserTest, CheckIntervalChar)
{
	parser_init_lib();

	parser_open_file("test.txt");
	int i = parser_check_interval_char(0, 'A', 'Z');
	EXPECT_EQ(1, i);
	i = parser_check_interval_char(0, 'w', 'z');
	EXPECT_EQ(0, i);
	parser_close_file(0);
}

TEST(ParserTest, CheckIntervalCharNj)
{
	parser_init_lib();

	parser_open_file("test.txt");
	int i = parser_check_interval_char_nj(0, 'A', 'Z');
	EXPECT_EQ(1, i);
	i = parser_check_interval_char_nj(0, 'w', 'z');
	EXPECT_EQ(0, i);
	parser_close_file(0);
	parser_open_file("test4.txt");
	i = parser_check_interval_char_nj(0, ' ', ' ');
	EXPECT_EQ(1, i);
	i = parser_check_interval_char_nj(0, 'w', 'z');
	EXPECT_EQ(0, i);
	parser_close_file(0);
}

TEST(ParserTest, CheckGetIntervalString)
{
	parser_init_lib();

	parser_open_file("test.txt");
	char *a = new char[2048];
	a[0] = '\0';
	parser_get_interval_string(0, 'A', 'z', a);
	EXPECT_NE(a[0], '\0');
	a[0] = '\0';
	parser_get_interval_string(0, 'A', 'B', a);
	EXPECT_EQ(a[0], '\0');
	parser_close_file(0);
}

TEST(ParserTest, CheckGetIntervalStringNj)
{
	parser_init_lib();

	parser_open_file("test.txt");
	char *a = new char[2048];
	a[0] = '\0';
	parser_get_interval_string_nj(0, 'A', 'z', a);
	EXPECT_NE(a[0], '\0');
	a[0] = '\0';
	parser_get_interval_string_nj(0, 'A', 'B', a);
	EXPECT_EQ(a[0], '\0');
	parser_close_file(0);
	parser_open_file("test4.txt");
	a[0] = '\0';
	parser_get_interval_string_nj(0, ' ', ' ', a);
	EXPECT_NE(a[0], '\0');
	parser_close_file(0);
}

// WARNING : GetNextWord does not do what is name tell
TEST(ParserTest, GetNextWord)
{
	parser_init_lib();

	parser_open_file("test.txt");
	char *a = new char[2048];
	a[0] = '\0';
	parser_get_next_word(0, a);
	std::string tmp = "";
	tmp = tmp + a;
	std::string tmp2 = "This";
	EXPECT_EQ(tmp2, tmp);
	a[0] = '\0';
	parser_get_next_word(0, a);
	tmp = "";
	tmp = tmp + a;
	tmp2 = "tarbouk";
	EXPECT_NE(tmp2, tmp);
	parser_close_file(0);
}

TEST(ParserTest, IsNumeric)
{
	parser_init_lib();

	parser_open_file("test5.txt");
	int i = parser_is_numeric(0);
	EXPECT_EQ(1, i);
	parser_open_file("test.txt");
	i = parser_is_numeric(1);
	EXPECT_EQ(0, i);
	parser_close_file(1);
	parser_close_file(0);
}

TEST(ParserTest, GetAlphaStr)
{
	parser_init_lib();

	parser_open_file("test6.txt");
	char *a = parser_get_alfa_str(0, new char[2048]);
	std::string tmp = "";
	tmp = tmp + a;
	std::string tmp2 = "alphaNum";
	EXPECT_EQ(tmp2, tmp);
	parser_open_file("test5.txt");
	a[0] = '\0';
	EXPECT_EQ(NULL, parser_get_alfa_str(1, new char[2048]));
	parser_close_file(1);
	parser_close_file(0);
}

TEST(ParserTest, GetAlphanumericStr)
{
	parser_init_lib();

	parser_open_file("test6.txt");
	char *a = parser_get_alfanumeric_str(0, new char[2048]);
	std::string tmp = "";
	tmp = tmp + a;
	std::string tmp2 = "alphaNum01234567989";
	EXPECT_EQ(tmp2, tmp);
	parser_close_file(0);
}

// MISS --- parser_get_int_str(int file_id, char *buf) AND parser_get_float_str(int file_id, char *buf);


TEST(ParserTest, AddJumpableChar)
{
	extern	fileBuffers	bufferManager;
	parser_init_lib();

	parser_open_file("test6.txt");
	char tmp2 = '$';
	parser_add_jumpable_char(0, tmp2);
	char tmp = bufferManager.buffers[0]->jumpable_char[strlen(bufferManager.buffers[0]->jumpable_char) - 1];
	EXPECT_EQ(tmp, tmp2);
	parser_close_file(0);
}

TEST(ParserTest, SetJumpableChar)
{
	extern	fileBuffers	bufferManager;
	parser_init_lib();

	parser_open_file("test6.txt");
	std::string tmp2 = "test";
	parser_set_jumpable_chars(0, tmp2.c_str());
	std::string tmp = bufferManager.buffers[0]->jumpable_char;
	EXPECT_EQ(tmp, tmp2);
	parser_close_file(0);
}

TEST(ParserTest, ResetJumpableChar)
{
	extern	fileBuffers	bufferManager;
	parser_init_lib();

	parser_open_file("test6.txt");
	std::string tmp2 = "test";
	parser_set_jumpable_chars(0, tmp2.c_str());
	parser_reset_jumpable_char(0);
	EXPECT_EQ('\0', bufferManager.buffers[0]->jumpable_char[0]);
	parser_close_file(0);
}