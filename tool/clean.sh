#!/bin/bash


for file in $@; do
    echo -e "\tParam : $file"
    /bin/sed '1,27d' $file > tmp_file
    mv tmp_file $file
done
