/*
**Copyright (C) <2013> <YIRL_Team>
**
**This program is free software: you can redistribute it and/or modify
**it under the terms of the GNU Lesser General Public License as published by
**the Free Software Foundation, either version 3 of the License, or
**(at your option) any later version.
**
**This program is distributed in the hope that it will be useful,
**but WITHOUT ANY WARRANTY; without even the implied warranty of
**MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**GNU General Public License for more details.
**
**You should have received a copy of the GNU Lesser General Public License
**along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef TEXT_EDIT_HH_
#define TEXT_EDIT_HH_

#include	<string>
#include	"Widget.hh"

/**
 * Widget use to get text
 */
class TextEdit : public Widget
{
public:
  TextEdit(StructEntity *gameElem);
  TextEdit(StructEntity *gameElem, unsigned int x, unsigned int y, unsigned int w, unsigned int h);
  ~TextEdit();
  InputStatue tryHandleInput(int ch);
  virtual bool render() = 0;
  virtual void resize() = 0;
protected:
  void init();
  // The link call when the text has been input, should be an "actionLink" taking a string in parameter
  Entity *_next;
  std::string _str; // the text inputed(or the default one)
};

#endif
