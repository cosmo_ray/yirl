/*
**Copyright (C) <2013> <YIRL_Team>
**
**This program is free software: you can redistribute it and/or modify
**it under the terms of the GNU Lesser General Public License as published by
**the Free Software Foundation, either version 3 of the License, or
**(at your option) any later version.
**
**This program is distributed in the hope that it will be useful,
**but WITHOUT ANY WARRANTY; without even the implied warranty of
**MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**GNU General Public License for more details.
**
**You should have received a copy of the GNU Lesser General Public License
**along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include	<string>
#include	<fstream>
#include	<streambuf>
#include	<stack>
#include	"CheckJson.hh"
#include	"EntityStructPrototypes.hh"
#include	"debug.h"
#include	"global.h"

namespace LoadJson
{
  int loadFDec(Json::Value &root, EntityStructPrototypes &prototypes)
  {
    const Json::Value declarations = root["fdec"];
    int i;
    int end = declarations.size();

    for (i = 0; i != end; ++i )  // Iterates over the sequence elements.
      {
	DPRINT_INFO("fdec: %s\n", declarations[i].asCString());
	prototypes.addStruct(declarations[i].asString());
      }
    return (i);
  }

  static EntityStructPrototypes::StructPrototype *getStructPrototype(const std::string &str, EntityStructPrototypes &prototypes)
  {
    EntityStructPrototypes::StructPrototype *ret;
    if ((ret = prototypes.getPrototype(str.c_str())) != NULL)
      return (ret);
    return (prototypes.addStruct(str));
  }

  static EntityStructPrototypes::ExtandEntityType *failCreateArrayRefInfo(const Json::Value &menber, int i)
  {
    (void)menber;
    if (i < 0)
      {
	DPRINT_ERR("Fail to create Array\n");
      }
    else
      {
	DPRINT_ERR("Fail to create Array of type %s\n", menber[i].asString().c_str());
      }
    return (NULL);
  }

  EntityStructPrototypes::ExtandEntityType *createArrayRefInfo(const Json::Value &menber, unsigned int i)
  {
    EntityType	contentType;
    EntityType  retType;

    if (i >= menber.size())
      return (failCreateArrayRefInfo(menber, -1));
    contentType = stringToEntityType(menber[i].asString().c_str());
    if ((int)contentType != -1)
      {
	retType = contentType;
	if (contentType == ARRAY || contentType == REF)
	  {
	    EntityStructPrototypes::ExtandEntityType *retArrayInfo;

	    retArrayInfo = createArrayRefInfo(menber, i + 1);
	    if (retArrayInfo == NULL)
	      return (failCreateArrayRefInfo(menber, i));
	    return (new EntityStructPrototypes::ExtandEntityType(retType, "", false, retArrayInfo));
	  }
	else
	  {
	    return (new EntityStructPrototypes::ExtandEntityType(retType, "", false));
	  }
      }
    else if (getPrototypeInstance()->isStructExisting(menber[i].asString()))
      {
	retType = STRUCT;

	std::string retStructName = menber[i].asString();
	return (new EntityStructPrototypes::ExtandEntityType(retType, "", false, retStructName));
      }
    else
      return (failCreateArrayRefInfo(menber, i));
    return (NULL);
  }

  void handleStatic(Json::Value::const_iterator staticElems, EntityStructPrototypes::StructPrototype *structPrototype, EntityStructPrototypes &prototypes, structPrototypePairStack &fatherToAdd)
  {
    addStruct<STATIC_MENBER>(staticElems, structPrototype, prototypes, fatherToAdd);
  }

  void handleIneritance(Json::Value extand, EntityStructPrototypes::StructPrototype *structPrototype, structPrototypePairStack &fatherToAdd)
  {
    int i;
    int end = extand.size();
    DPRINT(" __ extand1 -- %d\n", end);
    for (i = 0; i != end; ++i )  // Iterates over the sequence elements.
      {
	fatherToAdd.push(std::make_pair(structPrototype, extand[i].asString()));
      }
  }

  static inline bool getAddStruct(Json::Value::const_iterator &structure, EntityStructPrototypes &prototypes, structPrototypePairStack &fatherToAdd)
  {
    EntityStructPrototypes::StructPrototype *structPrototype = getStructPrototype(structure.key().asString(), prototypes);
    return (addStruct(structure, structPrototype, prototypes, fatherToAdd));
  }

  static bool addFathers(EntityStructPrototypes &prototypes, structPrototypePairStack &fatherToAdd)
  {
    EntityStructPrototypes::StructPrototype *tmp;

    DPRINT_INFO("-- extand2 -- fatherToAdd is empty: %s\n", (fatherToAdd.empty())? "TRUE": "FALSE");

    while (!fatherToAdd.empty())
      {
	tmp = prototypes.getPrototype(fatherToAdd.top().second.c_str());
	if (tmp != NULL)
	  {
	    DPRINT_INFO("extand: %s\n", fatherToAdd.top().second.c_str());
	    fatherToAdd.top().first->addFather(tmp);
	  }
	else
	  {
	    DPRINT_INFO("can not find: %s\n", fatherToAdd.top().second.c_str());
	  }
	fatherToAdd.pop();
      }
    return (true);
  }

  int loadDec(Json::Value &root, EntityStructPrototypes &prototypes)
  {
    unsigned int index = 0;
    const Json::Value declarations = root["dec"];
    const Json::Value::const_iterator end = declarations.end();
    // typedef in CheckJson.hpp;
    structPrototypePairStack fatherToAdd;

    for (Json::Value::const_iterator it = declarations.begin(); it != end; ++it )  // Iterates over the sequence elements.
      {
	if (!declarations.isObject())
	  return (-1);
	DPRINT_INFO("dec: %s\n", it.key().asCString());
	getAddStruct(it, prototypes, fatherToAdd);
	++index;
      }
    addFathers(prototypes, fatherToAdd);
    return (index);
  }
}
