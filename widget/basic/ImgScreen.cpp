/*
**Copyright (C) <2013> <YIRL_Team>
**
**This program is free software: you can redistribute it and/or modify
**it under the terms of the GNU Lesser General Public License as published by
**the Free Software Foundation, either version 3 of the License, or
**(at your option) any later version.
**
**This program is distributed in the hope that it will be useful,
**but WITHOUT ANY WARRANTY; without even the implied warranty of
**MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**GNU General Public License for more details.
**
**You should have received a copy of the GNU Lesser General Public License
**along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include	"ImgScreen.hh"
#include	"CallEvent.hh"
#include	"debug.h"

ImgScreen::ImgScreen(StructEntity *gameElem)
  : Widget (IMG_SCREEN, gameElem)
{
  init();
}

ImgScreen::ImgScreen(StructEntity *gameElem, unsigned int x, unsigned int y, unsigned int w, unsigned int h)
  : Widget (IMG_SCREEN, gameElem, x, y, w, h)
{
  init();
}

ImgScreen::ImgScreen(int t, StructEntity *gameElem)
  : Widget (t, gameElem)
{
  ImgScreen::init();
}

ImgScreen::ImgScreen(int t, StructEntity *gameElem, unsigned int x, unsigned int y, unsigned int w, unsigned int h)
  : Widget (t, gameElem, x, y, w, h)
{
  ImgScreen::init();
}


ImgScreen::~ImgScreen()
{
}

void ImgScreen::init()
{
  const char *tmpBg;
  
  _next = findEntity((Entity *)_elem, "next");
  _background = findEntity((Entity *)_elem, "background");
  //printf("\nBG: %p\n", findEntity((Entity *)_elem, "background"));
  //TODO: function wich check if the string insiide the background is NULL or design a path or a color and set the "type" of BG
}

ImgScreen::InputStatue ImgScreen::tryHandleInput(int ch)
{
  InputStatue ret = NOTHANDLE;

  if (ch == '\n')
    {
      handleEnter(getReferencedObj(_next));
      ret = ACTION;
    }
  return (ret);
}
