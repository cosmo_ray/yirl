#!/bin/sh
rm -rf CMakeCache.txt CMakeFiles cmake_install.cmake
git checkout -- `git status -uno | grep Makefile | cut -d":" -f2`
