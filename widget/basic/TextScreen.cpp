/*
**Copyright (C) <2013> <YIRL_Team>
**
**This program is free software: you can redistribute it and/or modify
**it under the terms of the GNU Lesser General Public License as published by
**the Free Software Foundation, either version 3 of the License, or
**(at your option) any later version.
**
**This program is distributed in the hope that it will be useful,
**but WITHOUT ANY WARRANTY; without even the implied warranty of
**MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**GNU General Public License for more details.
**
**You should have received a copy of the GNU Lesser General Public License
**along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include	"yirlAPI.hh"
#include	"TextScreen.hh"
#include	"CallEvent.hh"

TextScreen::TextScreen(StructEntity *gameElem)
  : ImgScreen(TEXT_SCREEN, gameElem)
{
  init();
}

TextScreen::TextScreen(StructEntity *gameElem, unsigned int x, unsigned int y, unsigned int w, unsigned int h)
  : ImgScreen(TEXT_SCREEN, gameElem, x, y, w, h)
{
  init();
}

TextScreen::~TextScreen()
{
}

void TextScreen::resetAff()
{
  std::string	*tmp = NULL;

  if (entityToAff_ == NULL)
    goto error;
  tmp = formatString(getStringVal(entityToAff_));
  toAff_ = *tmp;
  delete tmp;
  return;
 error:
  toAff_ = "";
}

void TextScreen::init()
{
  entityToAff_ = findEntity((Entity *)_elem, "text");
  resetAff();
  
  Widget::finishInit();
  //_background = getStringVal(findEntity((Entity *)_elem, "background"));
}

TextScreen::InputStatue TextScreen::tryHandleInput(int ch)
{
  InputStatue ret = NOTHANDLE;

  if (ch == '\n')
    {
      handleEnter(getReferencedObj(_next));
      ret = ACTION;
    }
  return (ret);
}
