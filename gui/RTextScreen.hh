/*
**Copyright (C) <2013> <YIRL_Team>
**
**This program is free software: you can redistribute it and/or modify
**it under the terms of the GNU Lesser General Public License as published by
**the Free Software Foundation, either version 3 of the License, or
**(at your option) any later version.
**
**This program is distributed in the hope that it will be useful,
**but WITHOUT ANY WARRANTY; without even the implied warranty of
**MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**GNU General Public License for more details.
**
**You should have received a copy of the GNU Lesser General Public License
**along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef	CTEXTSCREEN_HH
#define	CTEXTSCREEN_HH

#include	"macro.h"
#include	"TextScreen.hh"

class	RTextScreen : public TextScreen
{
public:
  RTextScreen(StructEntity *gameElem)
    : TextScreen(gameElem)
  {
    init();
  }

  RTextScreen(StructEntity *gameElem, unsigned int x, unsigned int y, unsigned int w, unsigned int h)
    : TextScreen(gameElem, x, y, w, h)
  {
    init();
  }
/**!
**\brief Initilise le text
**/
  void	init();
  virtual ~RTextScreen();
/**
**\brief affiche le text sur l'ecran
**/
  virtual bool render();
  /**
  **\brief re-dimensionne le texte
  **/
  virtual void resize();
protected:
private:
  // pointer use to permit external library to have they own memory inside a RTestScreen
  void	*_helper;
};

#endif
