/*
**Copyright (C) <2013> <YIRL_Team>
**
**This program is free software: you can redistribute it and/or modify
**it under the terms of the GNU Lesser General Public License as published by
**the Free Software Foundation, either version 3 of the License, or
**(at your option) any later version.
**
**This program is distributed in the hope that it will be useful,
**but WITHOUT ANY WARRANTY; without even the implied warranty of
**MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**GNU General Public License for more details.
**
**You should have received a copy of the GNU Lesser General Public License
**along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef	EVENT_STACK_HH
#define	EVENT_STACK_HH

#include	<list>
#include	"Event.hh"
/*Simple pile d'evenement (d'ailleur pourquoi on utilise une list et pas une vrais stack pour les manipuler?)*/
class EventStack
{
public:
  EventStack();
  virtual ~EventStack();

  void push(Event *toAdd);
  /**
   * get the elem at the top of the stack
   */
  Event *top();
  /**
   * remove the element at the top of the stack, and remove it
   */
  Event *pop();

  bool	empty();

private:
  std::list<Event *>	_events;
};

#endif

