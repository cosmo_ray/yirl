#include "stdafx.h"
#include "MappingWin.h"

#include <iostream>

MappingWin::MappingWin(void)
{
}


MappingWin::~MappingWin(void)
{
	UnmapViewOfFile(this->view);
	CloseHandle(this->file);
}

void	MappingWin::map(void)
{
	SYSTEM_INFO SysInfo;
	unsigned long file_size;
	DWORD dwSysGran;
	HANDLE hMap;

	this->file = CreateFile(TEXT("test.txt"), GENERIC_READ | GENERIC_WRITE, 0, NULL, OPEN_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL);
	if (this->file == INVALID_HANDLE_VALUE)
	{
		std::cout << "Error Open file" << std::endl;
		return;
	}
	GetSystemInfo(&SysInfo);
	dwSysGran = SysInfo.dwAllocationGranularity;
	file_size = GetFileSize(this->file, NULL);
	hMap = CreateFileMapping(this->file, NULL, PAGE_READWRITE, 0, file_size, NULL);
	if (hMap == NULL)
	{
		std::cout << "Error Creating Mapping" << std::endl;
		return;
	}
	this->view = MapViewOfFile(hMap, FILE_MAP_ALL_ACCESS, 0, 0 , file_size);
	if (this->view == NULL)
	{
		std::cout << "Error Creating Mapping 2 " << GetLastError() << std::endl;
		return;
	}
	this->recup = (char *)this->view;
}