/*
**Copyright (C) <2013> <YIRL_Team>
**
**This program is free software: you can redistribute it and/or modify
**it under the terms of the GNU Lesser General Public License as published by
**the Free Software Foundation, either version 3 of the License, or
**(at your option) any later version.
**
**This program is distributed in the hope that it will be useful,
**but WITHOUT ANY WARRANTY; without even the implied warranty of
**MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**GNU General Public License for more details.
**
**You should have received a copy of the GNU Lesser General Public License
**along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include	"CWidget.hh"
#include	"RMap.hh"

char getElemSpriteFromId(int id)
{
  switch (id)
    {
    case 0:
      return ('.');
    case 1:
      return ('@');
    case 2:
      return ('M');
    case 3:
      return ('T');
    case 4:
      return ('#');
    case 5:
      return ('0');
    case 6:
      return ('.');
    case 7:
      return ('#');
    case 8:
      return ('@');
    case 9:
      return ('#');
    default:
      return ('-');
    }
  return (-1);
}

void	RMap::init()
{
  CWidget **tmp = ((CWidget **)&_helper);
  *tmp = new CWidget(*this);
}

RMap::~RMap()
{
  delete reinterpret_cast<CWidget *>(_helper);
}

bool RMap::render()
{
  CWidget *wid = reinterpret_cast<CWidget *>(_helper);
  unsigned int x,y,h,w;
  unsigned int	lenMap = getLen(_elems);
  unsigned int	curx = 0, cury = 0;
  // int	begX; // the begin of printing each line
  // int	begy; // the begin of printing each colone

  wrefresh(wid->win);
  wmove(wid->win,0,0);
  getyx(wid->win, y, x);
  getmaxyx(wid->win, h, w);
  w -= x;
  h -= y;
  // begX = (w / 2) - (_wMap / 2);

  LOG("curses aff map");
  for (unsigned int i = 0; i < lenMap; ++i)
    {
      if (curx >= _wMap)
	{
	  curx = 0;
	  ++cury;
	}

      LOG("aff case\n");
      mvwaddch(wid->win, (cury+1) + (h/2 - _hMap / 2) , (curx + 1) + (w/2 - _wMap / 2), 
	       getElemSpriteFromId(getMapId(i, -1)));
      ++curx;
    }
  LOG("finish aff map\n");  
  wrefresh(wid->win);
  return false;
}

void RMap::resize()
{
  CWidget *wid = reinterpret_cast<CWidget *>(_helper);

  wid->resize();
}
