/*
**Copyright (C) <2013> <YIRL_Team>
**
**This program is free software: you can redistribute it and/or modify
**it under the terms of the GNU Lesser General Public License as published by
**the Free Software Foundation, either version 3 of the License, or
**(at your option) any later version.
**
**This program is distributed in the hope that it will be useful,
**but WITHOUT ANY WARRANTY; without even the implied warranty of
**MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**GNU General Public License for more details.
**
**You should have received a copy of the GNU Lesser General Public License
**along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef	CHECK_JSON_HPP
#define	CHECK_JSON_HPP

namespace LoadJson
{
  typedef std::stack<std::pair<EntityStructPrototypes::StructPrototype *, std::string> > structPrototypePairStack;

  EntityStructPrototypes::ExtandEntityType *createArrayRefInfo(const Json::Value &menber, unsigned int i);

  void handleStatic(Json::Value::const_iterator staticElems, EntityStructPrototypes::StructPrototype *structPrototype, EntityStructPrototypes &prototypes, structPrototypePairStack &fatherToAdd);
  void handleIneritance(Json::Value staticElems, EntityStructPrototypes::StructPrototype *structPrototype, structPrototypePairStack &fatherToAdd);

  template<bool TYPE = false>
  bool addArrayRefMenber(const Json::Value::const_iterator &menber, EntityStructPrototypes::StructPrototype *structPrototype, EntityType type)
  {
    EntityStructPrototypes::ExtandEntityType *contantInfo = createArrayRefInfo(*menber, 1);
    structPrototype->addMenber<TYPE>(type, menber.key().asString().c_str(), contantInfo);
    return (true);
  }

  template<bool TYPE = false>
  bool addFunction(const Json::Value::const_iterator &menber, EntityStructPrototypes::StructPrototype *structPrototype)
  {
    EntityStructPrototypes::FunctionInfo	*functionInfo;
    unsigned int i = 1;
    EntityType	contentType;
    functionInfo = new EntityStructPrototypes::FunctionInfo;

    if (i >= (*menber).size())
      return (false);
    
    // check if the current argument is the return type of this function
    contentType = stringToEntityType( (*menber)[i].asString().c_str());
    while (i + 1 <  (*menber).size())
      {
	/**
	 * TODO
	 * chang menber.key().asString() to the structure name
	 */
	if (contentType == STRUCT)
	  functionInfo->arguments.push_back(std::pair<EntityType, std::string *>(static_cast<EntityType>(contentType), new std::string((*menber)[i].asString().c_str())));
	else
	  functionInfo->arguments.push_back(std::pair<EntityType, std::string *>(static_cast<EntityType>(contentType), NULL));
	++i;
	contentType = stringToEntityType( (*menber)[i].asString().c_str());
      }
 
    // if i is superior or equal to menber.size() so thers no return type
    if (i >=  (*menber).size())
	return (false);
    /**
     * TODO
     * change menber.key().asString() to the structure name
     */
    if (contentType == STRUCT)
      functionInfo->returnType =  std::pair<EntityType, std::string *>(static_cast<EntityType>(contentType), new std::string((*menber)[i].asString().c_str()));
    else
      functionInfo->returnType = std::pair<EntityType, std::string *>(static_cast<EntityType>(contentType), NULL);
    structPrototype->addMenber<TYPE>(FUNCTION, menber.key().asString().c_str(), functionInfo);
    return (true);
  }

  template<bool TYPE = false>
  bool addStruct(Json::Value::const_iterator &structure, EntityStructPrototypes::StructPrototype *structPrototype, EntityStructPrototypes &prototypes, structPrototypePairStack &fatherToAdd)
  {
    const Json::Value::const_iterator end = (*structure).end();
    Json::Value::const_iterator it;
    EntityType	type;

    for (it = (*structure).begin(); it != end; ++it )  // Iterates over the sequence elements.
      {
	if (it.key().asString() == "static" && (*it).isObject())
	  {
	    handleStatic(it, structPrototype, prototypes, fatherToAdd);
	  }
	else if (it.key().asString() == "inerit" && (*it).isArray())
	  {
	    handleIneritance(*it, structPrototype, fatherToAdd);
	  }
	else if ((*it).isArray())
	  {
	    int i = 0;
	    type = stringToEntityType((*it)[i].asString().c_str());
	    if (type == FUNCTION)
	      addFunction<TYPE>(it, structPrototype);
	    else
	      addArrayRefMenber<TYPE>(it, structPrototype, type);
	  }
	else if ((type = stringToEntityType((*it).asString().c_str())) != -1)
	  {
	    structPrototype->addMenber<TYPE>(type, it.key().asString().c_str());
	  }
	else if (prototypes.isStructExisting((*it).asString().c_str()))
	  {
	    structPrototype->addMenber<TYPE>(STRUCT, it.key().asString().c_str(), (*it).asString().c_str());
	  }
      }
    return (true);
  }
}

#endif
