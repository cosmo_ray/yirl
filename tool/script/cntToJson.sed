#!/bin/sed -rnf
/DEF/,/END/ {
	/DEF/ { s/DEF/{/1 ;N}
 	/END/ { s/END/}/1}
 	/^$/d
	s/^([a-zA-Z]+)$/"\1": /1
	/\{/,/\}/{
    	s/^[ \t]*([^ \t]+)[ \t]+([^;]+);.*$/\t"\2": { "type": "\1" }/1
	}
}
p
