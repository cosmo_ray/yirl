/*
**Copyright (C) <2013> <YIRL_Team>
**
**This program is free software: you can redistribute it and/or modify
**it under the terms of the GNU Lesser General Public License as published by
**the Free Software Foundation, either version 3 of the License, or
**(at your option) any later version.
**
**This program is distributed in the hope that it will be useful,
**but WITHOUT ANY WARRANTY; without even the implied warranty of
**MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**GNU General Public License for more details.
**
**You should have received a copy of the GNU Lesser General Public License
**along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef	CWIDGET_HH
#define	CWIDGET_HH

/**
 * I use this file for all the SDL2 specify data i have to store/initialise...
 */

#include	"Widget.hh"
#include	"debug.h"
#include	"yirlAPI.hh"
#include	<SDL2/SDL.h>
#include	<SDL2/SDL_ttf.h>

#define	WIN_W_SIZE 640
#define	WIN_H_SIZE 480

#define	CARACT_PER_LINE 70

/**
 * Struct contenng all the SDL useful "global"
 * And if you can explain to me in what a singleton instead of a global is usefull here, i'll be happy to hear it
 * But if you come say "herk a global it's crappy" without any argumentation
 * Please stop programing, you don't have a competances for this. 
 */
struct	SDL_Global
{
  SDL_Global(): fullscreen(0){} 

  int		fullscreen;
  SDL_Window*	pWindow;
  TTF_Font*	font;
  SDL_Surface*	wSurface() {return (SDL_GetWindowSurface(pWindow));}
  SDL_Rect	getRect() {return (wSurface()->clip_rect);}
  SDL_Renderer* renderer;
};

SDL_Global &getSg();

class	CWidget
{
public:
  CWidget(const Widget &wid);
  ~CWidget()
  {
    DPRINT_INFO("Delete win\n");
  }

  void resize()
  {
    _rect.h = _wid.getH() * getResY() / 1000;
    _rect.w = _wid.getW() * getResX() / 1000;
    _rect.x = _wid.getX() * getResX() / 1000;
    _rect.y = _wid.getY() * getResY() / 1000;
  }

  void	fillBg(SDL_Surface *img);
  void	fillBg(SDL_Renderer*ren, short r, short g, short b, short a);

  SDL_Rect &rect(){return (_rect);}
protected:
  const Widget &_wid;
  SDL_Rect	_rect;
private:
  CWidget(const CWidget &);
  CWidget &operator=(const CWidget &);
};

#endif
