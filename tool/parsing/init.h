/*
**Copyright (C) <2013> <YIRL_Team>
**
**This program is free software: you can redistribute it and/or modify
**it under the terms of the GNU Lesser General Public License as published by
**the Free Software Foundation, either version 3 of the License, or
**(at your option) any later version.
**
**This program is distributed in the hope that it will be useful,
**but WITHOUT ANY WARRANTY; without even the implied warranty of
**MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**GNU General Public License for more details.
**
**You should have received a copy of the GNU Lesser General Public License
**along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef	INIT_H
#define	INIT_H

#define	BUFSIZE	2048
#define	JUMPABLE_CHAR_BUFSIZE 124

typedef	struct
{
  int	fd;
  int	nb_buffer;
  int	buff_size; // the size of the current buffer
  int	old_buffer;
  int	index; // the index of the stream of the current buffer
  int	change_buffer; // the number of buffer change since the begin of an operation
  char	jumpable_char[JUMPABLE_CHAR_BUFSIZE]; // the caracter that arem't evaluated(generaly space)
  char	*cur_buf;
  char	(*buffers)[BUFSIZE];
}	fileBuffer;

typedef	struct
{
  int	size;
  fileBuffer	**buffers;
}	fileBuffers;



int	parser_init_lib(void);
int	parser_open_file(const char *);
void	parser_close_file(int i);
void	parser_close_lib(void);
/**
 *
 * @return the value of read
 */
int    parser_read_next_block(int file_id);

int    parser_restor_buffer(fileBuffer *buffer);
int    parser_validate_buffer(fileBuffer *buffer);
void   parser_incr_index(fileBuffer *buffer);

/**
 * Free the buffer at the file_id index, close fd, and free allocated memory.
 *
 * @return -1
 */
int	parser_free_ragequit(int file_id);

#endif
