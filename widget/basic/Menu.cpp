/*
**Copyright (C) <2013> <YIRL_Team>
**
**This program is free software: you can redistribute it and/or modify
**it under the terms of the GNU Lesser General Public License as published by
**the Free Software Foundation, either version 3 of the License, or
**(at your option) any later version.
**
**This program is distributed in the hope that it will be useful,
**but WITHOUT ANY WARRANTY; without even the implied warranty of
**MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**GNU General Public License for more details.
**
**You should have received a copy of the GNU Lesser General Public License
**along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include	<iostream>
#include	"Menu.hh"
#include	"debug.h"

Menu::Menu(StructEntity *gameElem)
  : Widget (MENU, gameElem)
  , _currentPos(0)
{
  init();
}

Menu::Menu(StructEntity *gameElem, unsigned int x, unsigned int y, unsigned int w, unsigned int h)
  : Widget (MENU, gameElem, x, y, w, h)
  , _currentPos(0)
{
  init();
}

void Menu::init()
{
  _links = findEntity((Entity *)_elem, "links");
  _totalLen = getLen(_links);
  if (_links == NULL) {
    //DPRINT_ERR("error when creating Menu, can not asign links\n");
    return;
  }
  Widget::finishInit();
}

Menu::InputStatue Menu::tryHandleInput(int ch)
{
  InputStatue ret = NOTHANDLE;

  if (ch == UP_ARROW)
    {
      _currentPos -= 1;
      ret = NOACTION;
    }
  else if (ch == DOWN_ARROW)
    {
      _currentPos += 1;
      ret = NOACTION;
    }
  else if (ch == '\n')
    {
      Entity *textLink = getReferencedObj(getEntity(_links, _currentPos));
      handleEnter(getReferencedObj(findEntity(textLink, "link")));
      ret = ACTION;
    }
  _currentPos %= _totalLen;
  // if (_currentPos > 5000) // hum hum
  //   _currentPos = 0;
  // else if (_currentPos >= _totalLen)
  //   _currentPos = _totalLen -1;
  // blockant part
  // waddstr(_win, (char *)&ch);
  // wprintw(_win, "\n%d\n", ch);
  // non blockant part
  return (ret);
}
