/*
**Copyright (C) <2013> <YIRL_Team>
**
**This program is free software: you can redistribute it and/or modify
**it under the terms of the GNU Lesser General Public License as published by
**the Free Software Foundation, either version 3 of the License, or
**(at your option) any later version.
**
**This program is distributed in the hope that it will be useful,
**but WITHOUT ANY WARRANTY; without even the implied warranty of
**MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**GNU General Public License for more details.
**
**You should have received a copy of the GNU Lesser General Public License
**along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include	"../tool/parsing/parser.h"
#include	"GameLoop.hh"
#include	"debug.h"
#include	"Entity.hh"
#include	"conf/changeFirstModule.hh"
#include	"yirlAPI.hh"
#include	"macro.h"
//Include de test

#ifndef __unix__
/*Probleme de complation sous windows, le .cpp de change first module
ne se compile pas, fonction vide de remplacement avec un retour 0*/
UNIMPLEMENTED_FUNCTIONALITY_SYSTEM(0, fm, "windows", int  changeFirstModule(const char *))
V_UNIMPLEMENTED_FUNCTIONALITY_SYSTEM(lm, "windows", void  listModules())

#endif

/**
 * Init and run the game.
 * @see GameLoop
 * @return 0
 */
int	startGameLoop()
{
  GameLoop	&game = GameLoop::getInstance();
  initGame(&game);
  game.start();
  return (0);
}

namespace
{
  

  void	printUsage()
  {
    printf("Usage: yirl [-wid[=widget_library]] [-fm[=module_name]] [-lm] [-lwl]\n");
  }
  
  /**
   * Print yirl usage
   * @return -1
   */
  int	errUsage(const char *cmd)
  {
    printf("%s: is not a valide cmd\n", cmd);
    printUsage();
    return (-1);
  }

  /**
   * Count the number of '-' at the begining of the command
   * @param cmd	the command
   * @return	-1 if no '-' in the string, the number of '-' at the begining of the command
   */
  int checkDash(char *cmd)
  {
    unsigned int i;

    for (i = 0; i < strlen(cmd) && cmd[i] == '-'; ++i);
    if (strlen(cmd) == i)
      return (-1);
    return (i);
  }

  /**
   * Recursive function wich process a CLI argument.
   * If the current argument is not the last one, call itself to process the next command.
   * @param pos	index of the command line argument to process
   * @param ac	number of command line arguments
   * @param av	array containing all command line arguments
   * @return -1 if syntaxe error in the argument, 0 otherwise
   */
  int handleArgument(int pos, int ac, char **av)
  {
    int	nbDash;
    char *cmd;
#ifdef __unix__

    if (pos >= ac)
      return (0);
    nbDash = checkDash(av[pos]);

    if (nbDash == 1)
      {
	cmd = av[pos] + nbDash;
	if (!strcmp(strtok(cmd, "="), "fm"))
	  {
	    changeFirstModule(strtok(NULL, "="));
	  }
	else if (!strcmp(cmd, "lm"))
	  listModules();
	else if (!strcmp(cmd, "lwl"))
	  listWidLib();
	else if (!strcmp(cmd, "h"))
	  printUsage();
	else if (!strcmp(cmd, "wid"))
	  changeWidLib(strtok(NULL, "="));
	else if (!strcmp(cmd, "start"))
	  startGameLoop();
	else
	  return (errUsage(cmd));
      }
    else
      return (-1);
    return (handleArgument(pos + 1, ac, av));
	#endif
  }

  /**
   * Check if yirl was called with command line arguments and call handleArgument.
   * If no arguments, start the game loop.
   * @see startGameLoop
   * @param ac	the number of command line arguments
   * @param av	the array of command line arguments
   * @return	return -1 if there is error in command line arguments, 0 otherwise.
   */
  int handleArguments(int ac, char **av)
  {
    if (ac == 1)
      return (startGameLoop());
    if (handleArgument(1, ac, av))
      return -1;
    return 0;
  }
}

int	main(int ac, char **av)
{
  int	ret;

  debug_init();
  ret = handleArguments(ac, av);
  debug_exit();
  return (ret);
}
