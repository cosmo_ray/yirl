/*
**Copyright (C) <2013> <YIRL_Team>
**
**This program is free software: you can redistribute it and/or modify
**it under the terms of the GNU Lesser General Public License as published by
**the Free Software Foundation, either version 3 of the License, or
**(at your option) any later version.
**
**This program is distributed in the hope that it will be useful,
**but WITHOUT ANY WARRANTY; without even the implied warranty of
**MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**GNU General Public License for more details.
**
**You should have received a copy of the GNU Lesser General Public License
**along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include	<cstring>
#include	"RTextScreen.hh"
#include	"CWidget.hh"

void	RTextScreen::init()
{
  CWidget **tmp = ((CWidget **)&_helper);
  *tmp = new CWidget(*this);
}

RTextScreen::~RTextScreen()
{
    delete reinterpret_cast<CWidget *>(_helper);
}

bool RTextScreen::render()
{
  CWidget *wid = reinterpret_cast<CWidget *>(_helper);
  int x,y,h,w;
  resetAff();
  const char *toPrint = toAff_.c_str();
  int	len = strlen(toPrint);

  wrefresh(wid->win);
  wmove(wid->win,0,0);
  getyx(wid->win, y, x);
  getmaxyx(wid->win, h, w);
  w -= x;
  h -= y;
  (void) h; // for debug print
  DPRINT_INFO("enter render, widget at h: %d w: %d x: %d y: %d\n", h, w, x, y);

  //iterator on the string pos
  int it = 0;
  for (int itY = 1; it < len; ++itY)
    {
      for (int itX = 1; itX < (w - 1) && it < len; ++itX)
  	{
  	  if (toPrint[it] == '\n')
  	    {
  	      ++it;
  	      break;
  	    }
  	  mvwaddch(wid->win, itY, itX, toPrint[it]);
  	  ++it;
  	}
    }
  // wprintw(wid->win, "%s\n", _toAff.c_str());
  //redrawwin(_win);
  wrefresh(wid->win);
  DPRINT_INFO("quit render, widget at h: %d w: %d x: %d y: %d\n", h, w, x, y);
  return (true);
}

void RTextScreen::resize()
{
  CWidget *wid = reinterpret_cast<CWidget *>(_helper);

  wid->resize();
}
