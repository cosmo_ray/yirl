/*
**Copyright (C) <2013> <YIRL_Team>
**
**This program is free software: you can redistribute it and/or modify
**it under the terms of the GNU Lesser General Public License as published by
**the Free Software Foundation, either version 3 of the License, or
**(at your option) any later version.
**
**This program is distributed in the hope that it will be useful,
**but WITHOUT ANY WARRANTY; without even the implied warranty of
**MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**GNU General Public License for more details.
**
**You should have received a copy of the GNU Lesser General Public License
**along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include	"CWidget.hh"
#include	"RMap.hh"
#include	"SDLContent.hpp"

#define	SIZE_SPRITE_W  50
#define	SIZE_SPRITE_H  50
extern SDL_Global sg;

namespace {
  inline void displaySprites(SDLContent *, int x, int y, SDL_Surface *image,
			     int spriteSizeW, int spriteSizeH, SDLContent *wid,
			     int thresholdW)
  {
    SDL_Rect DestR;

    DestR.x = x * spriteSizeW + wid->rect().x + thresholdW;
    DestR.y = y * spriteSizeH + wid->rect().y; 
    DestR.w = spriteSizeW; 
    DestR.h = spriteSizeH; 
    if (!image)
      {
	DPRINT_WARN("%s\n", "image is NULL");
	return;
      }
    // SDL_Rect spritePosition = {x, y, image->w, image->h};
    SDL_Texture *texture = SDL_CreateTextureFromSurface(getSg().renderer, image);
    SDL_RenderCopy(getSg().renderer, texture, NULL, &DestR);
    SDL_DestroyTexture(texture);
    return;
  }

  inline void addSprite(SDLContent *wid, int id)
  {
    Entity *tmp = getEntityByName("spritesManager");
    if (tmp) {
      Entity *sprites = findEntity(tmp, "sprites");
      for (unsigned int i = 0, size = getLen(sprites);
	   i < size; ++i) {
	if (getIntVal(findEntity(getReferencedObj(getEntity(sprites, i)), "id")) == id){
	  wid->addSprite(getStringVal(findEntity(getReferencedObj(getEntity(sprites, i)), "path")), id);
 	  return;
	}
      }
    }
  }
}

SDL_Surface	*getElemSpriteFromId(SDLContent *wid, int id)
{
  SDL_Surface	*ret = wid->getSprite(id);
  
  if (ret == NULL)
    return wid->getSprite(-1);
  return ret;
}

void	RMap::init()
{
  SDLContent **tmp = ((SDLContent**)&_helper);
  *tmp = new SDLContent(*this);
  SDLContent *wid = reinterpret_cast<SDLContent *>(_helper);

  wid->addSprite("./Content/Sprites/forest/map-object/grass1.png", 0);
  wid->addSprite("./Content/Sprites/hero/hero.png", 1);
  wid->addSprite("./Content/Sprites/forest/object/grasstree.png", 2);
  wid->addSprite("./Content/Sprites/hero/monster.png", 3);  addSprite(wid, 0);
  addSprite(wid, 1);
  addSprite(wid, 2);
  addSprite(wid, 3);
  addSprite(wid, 4);
  addSprite(wid, 5);
  addSprite(wid, 6);
  addSprite(wid, 7);
  addSprite(wid, 8);
  addSprite(wid, 9);
}

RMap::~RMap()
{
  delete reinterpret_cast<SDLContent*>(_helper);
}

static int nbSpriteW(SDLContent *wid)
{
  return wid->rect().w / SIZE_SPRITE_W;
}

static int nbSpriteH(SDLContent *wid)
{
  return  wid->rect().h / SIZE_SPRITE_H;
}

static int getXFromCurrent(Entity *current, unsigned int w)
{
  return (getIntVal(findEntity(current, "pos")) % w);
}

static int getYFromCurrent(Entity *current, unsigned int w)
{
  return (getIntVal(findEntity(current, "pos")) / w);
}

bool RMap::render()
{
  SDLContent *wid = reinterpret_cast<SDLContent *>(_helper);
  // int x,y,h,w;
  int lenMap = getLen(_elems);
  unsigned int curx = 0, cury = 0;
  int id;
  int posCurrentElem = getIntVal(findEntity(_current, "pos"));
  int posCurrentElemY; int posCurrentElemX;
  int sizeScreenW = wid->rect().w, sizeScreenH = wid->rect().h;
  int nbCaseInW = sizeScreenW / SIZE_SPRITE_W;
  int nbCaseInH = sizeScreenH / SIZE_SPRITE_H;
  
  SDL_UpdateWindowSurface(getSg().pWindow);
  posCurrentElemY = posCurrentElem / _wMap;
  posCurrentElemX = posCurrentElem % _wMap;
  posCurrentElemY = (posCurrentElemY - (nbCaseInH) / 2);
  posCurrentElemX = (posCurrentElemX - (nbCaseInW) / 2);

  if (posCurrentElemY < 0)
    posCurrentElemY = 0;
  if (posCurrentElemX < 0)
    posCurrentElemX = 0;

  if ((posCurrentElemX + nbCaseInW) > _wMap) {
    posCurrentElemX = _wMap - nbCaseInW;
    if (posCurrentElemX < 0)
      posCurrentElemX = 0;
  }
  
  if ((posCurrentElemY + nbCaseInH) > _hMap) {
    posCurrentElemY = _hMap - nbCaseInH;
    if (posCurrentElemY < 0)
      posCurrentElemY = 0;
  }
  
  if (_cType == FOLLOW_CARACTER) {
    int i = (posCurrentElemY * _wMap + posCurrentElemX);
    int end = i + nbCaseInH * nbCaseInW;
    int thresholdW = 0;
    
    if (SIZE_SPRITE_W * nbCaseInW < getResX())
      thresholdW =  getResX() / 2 - (SIZE_SPRITE_W * nbCaseInW) / 2;
    for (; i < end || i < lenMap; ++i) {
      if (curx >= _wMap) {
	curx = 0;
	++cury;
      }
      for (int i2 = 0, tmpLen = getCaseLen(i);
	   i2 < tmpLen; ++i2) {
	id = getMapId(i, i2);
	SDL_Surface *image = getElemSpriteFromId(wid, id);
	displaySprites(wid, curx, cury, image,
		       SIZE_SPRITE_W, SIZE_SPRITE_H, wid, thresholdW);
      }
      ++curx;
    }
  } else {
    int sizeSpriteW = SIZE_SPRITE_W * sizeScreenW / (_wMap * SIZE_SPRITE_W);
    int sizeSpriteH = SIZE_SPRITE_H * sizeScreenH / (_hMap * SIZE_SPRITE_H);
    int thresholdW = 0;
    if (sizeSpriteW > sizeSpriteH)
      sizeSpriteW = sizeSpriteH;
    else if (sizeSpriteW < sizeSpriteH)
      sizeSpriteH = sizeSpriteW;

    if (sizeSpriteW * _wMap < getResX())
      thresholdW =  getResX() / 2 - (sizeSpriteW * _wMap) / 2;
    for (int i = 0; i < lenMap; ++i) {
      if (curx >= _wMap) {
	curx = 0;
	++cury;
      }
      for (int i2 = 0, tmpLen = getCaseLen(i);
	   i2 < tmpLen; ++i2) {
	if (hasToRend(i, i2)) {
	  id = getMapId(i, i2);
	  SDL_Surface *image = getElemSpriteFromId(wid, id);
	  displaySprites(wid, curx, cury, image,
			 sizeSpriteW,
			 sizeSpriteH, wid, thresholdW);
	  setRefresh(i, i2, 0);
	}
      }
      ++curx;
    }
  }
  SDL_RenderPresent(getSg().renderer);
  SDL_UpdateWindowSurface(getSg().pWindow);
  return true;
}


void RMap::resize()
{
  CWidget *wid = reinterpret_cast<CWidget *>(_helper);

  wid->resize();
}

#undef	SIZE_SPRITE_X
#undef	SIZE_SPRITE_Y
