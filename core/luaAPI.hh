/*
**Copyright (C) <2013> <YIRL_Team>
**
**This program is free software: you can redistribute it and/or modify
**it under the terms of the GNU Lesser General Public License as published by
**the Free Software Foundation, either version 3 of the License, or
**(at your option) any later version.
**
**This program is distributed in the hope that it will be useful,
**but WITHOUT ANY WARRANTY; without even the implied warranty of
**MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**GNU General Public License for more details.
**
**You should have received a copy of the GNU Lesser General Public License
**along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef	LUACORECALL_HH
#define LUACORECALL_HH

#include	"ScriptLua.hh"
#include	"yirlAPI.hh"
#include	"entity.h"

int	luaYAnd(lua_State *L);

int	luaCreateRef(lua_State *L);

int	luaGetObject(lua_State *L);

int	luaGetEntityByName(lua_State *L);

int	luaGetMenberEntity(lua_State *L);

int	luaGetStringVal(lua_State *L);

int	luaGetIntVal(lua_State *L);

int	luaGetFloatVal(lua_State *L);

int	luaSetIntVal(lua_State *L);

int	luaSetFloatVal(lua_State *L);

int	luaSetRefVal(lua_State *L);

int	luaGetReferencedObj(lua_State *L);

int	luaEndGame(lua_State *L);

int	luaAddEntity(lua_State *L);

int	luaGetArrayMenber(lua_State *L);

int	luaArrayRemove(lua_State *L);

int	luaArrayPushBack(lua_State *L);

int	luaSetRefAt(lua_State *L);

int	luaArrayPopBack(lua_State *L);

int	luaGetArrayLen(lua_State *L);

int	luaCreateArray(lua_State *L);

int	luaGetName(lua_State *L);

int     luaCopyEntity(lua_State *L);

int	luaTryGetStructEntityName(lua_State *L);

int	luaCall(lua_State *L);

int	luaCallByName(lua_State *L);

int    	luaPushChangeWidgetEvent(lua_State *L);

int	luaGetFunctionNumberArgs(lua_State *L);

int	luaUnsetFunction(lua_State *L);

int	luaSetFunction(lua_State *L);

int	luaGetType(lua_State *L);

int	luaSetTimeType(lua_State *L);

int	luaPlaySound(lua_State *L);

#endif
