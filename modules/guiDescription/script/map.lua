NULL = 0
STRUCT = 0
YINT = 1
YFLOAT = 2
YSTRING = 3
REF = 4
ARRAY = 5
FUNCTION = 6
GEN = 7
STATIC = 8

BLOCK = 1
ANSWER = 2


function rmCurrent(elem)
   local w = getEntityIntValue(getEntityMenber(elem, "w"))
   local map = getEntityMenber(elem, "elems")
   local posElem = getEntityMenber(elem, "pos")
   local oldPos = getEntityIntValue(posElem)
   local oldX = oldPos % w
   local oldY = oldPos / w
   arrayRemove(getArrayMenber(map, oldPos), elem, 1)
end

function rmInit(elem)
   setFunction(getEntityMenber(elem, "init"), NULL)
end

function positionCaraAtStartingPoint(elem, pj)
   local startP = getEntityMenber(elem, "startingPoints")
   local elems =  getEntityMenber(elem, "elems")
   local x = getEntityMenber(startP, "x")
   local y = getEntityMenber(startP, "y")
   local w = getEntityMenber(elem, "w")
   local pos = getEntityIntValue(x) + (getEntityIntValue(y) * getEntityIntValue(w))
   local toAdd = nil
   if pj == nil then
      toAdd = getEntity("MapElemActionable", "pjTest");
   else
      toAdd = pj;
   end
   setEntityIntValue(getEntityMenber(toAdd, "pos"), pos)
   setEntityRefValue(getEntityMenber(toAdd, "map"), elem)
   setEntityRefValue(getEntityMenber(elem, "current"), toAdd)
   setRefresh(toAdd)
   arrayPushBack(getArrayMenber(elems, pos), createRef(toAdd, NULL))
end

function createMapElemAtStartingPoint(elem)
   local startP = getEntityMenber(elem, "startingPoints")
   local elems =  getEntityMenber(elem, "elems")
   local x = getEntityMenber(startP, "x")  
   local y = getEntityMenber(startP, "y")
   local w = getEntityMenber(elem, "w")
   local pos = getEntityIntValue(x) + (getEntityIntValue(y) * getEntityIntValue(w))
   local toAdd = addEntity("MapElemActionable", "toAdd")
   setEntityIntValue(getEntityMenber(toAdd, "pos"), pos)
   setEntityRefValue(getEntityMenber(toAdd, "map"), elem)
   setEntityRefValue(getEntityMenber(elem, "current"), toAdd)
   setRefresh(toAdd)
   arrayPushBack(getArrayMenber(elems, pos), createRef(toAdd, NULL))
end

function moveMapElem(elem, x, y)
   map = getReferencedObj(getEntityMenber(elem, "map"));
   w = getEntityIntValue(getEntityMenber(map, "w"))
   map = getEntityMenber(map, "elems")
   posElem = getEntityMenber(elem, "pos")
   oldPos = getEntityIntValue(posElem)
   oldX = oldPos % w
   oldY = oldPos / w
   newPos = x + (y * w)
   setEntityIntValue(getEntityMenber(elem, "pos"), newPos)
   elem = arrayPopBack(getArrayMenber(map, oldPos))
   arrayPushBack(getArrayMenber(map, newPos), elem)
end

function moveMapElemPos(elem, newPos)
   local map = getReferencedObj(getEntityMenber(elem, "map"));
   local w = getEntityIntValue(getEntityMenber(map, "w"))
   local map = getEntityMenber(map, "elems")
   local posElem = getEntityMenber(elem, "pos")
   local oldPos = getEntityIntValue(posElem)
   local oldX = oldPos % w
   local oldY = oldPos / w
   setEntityIntValue(getEntityMenber(elem, "pos"), newPos)
   local elem = arrayRemove(getArrayMenber(map, oldPos), elem, 1)
   arrayPushBack(getArrayMenber(map, newPos), elem)
end

function solid()
end

function changeMap(elem, arg1)
   nextE = getReferencedObj(getEntityMenber(elem, "next"));
   pushChangeWidgetEvent(getEntityByName("mapCnt"), nextE);
end

function answer(elem, arg1)
   call(getEntityMenber(elem, "answer"), elem, arg1)
end

function actionOn(elem, arg1)
   local flags = getEntityMenber(arg1, "flags")
   local flagsVal = getEntityIntValue(flags)
   if yAnd(flagsVal, ANSWER) ~= 0 then
      answer(arg1, elem)
   end
   if yAnd(flagsVal, BLOCK) ~= 0 then
      solid()
   else
      moveOn(elem, arg1)
   end
end

function setRefresh(elem, val)
   local refresh = getEntityMenber(elem, "refresh")
   setEntityIntValue(refresh, 1)
end

function moveOn(elem, arg1)
   local map = getReferencedObj(getEntityMenber(elem, "map"));
   local elems =  getEntityMenber(map, "elems")
   len = getLen(elems)
   if len < 1 then
      print("arguments are incorect")
      --TODO: handle the error here
      return
   end
   local map = getReferencedObj(getEntityMenber(elem, "map"));
   local w = getEntityIntValue(getEntityMenber(map, "w"))
   local pos = getEntityIntValue(getEntityMenber(arg1, "pos"));
   local oldPos = getEntityIntValue(getEntityMenber(elem, "pos"));
   moveMapElemPos(elem, pos)
   local subElem = getArrayMenber(elems, oldPos)
   subElem = getReferencedObj(getArrayMenber(subElem, 0))
   setRefresh(arg1)
   setRefresh(elem)
   setRefresh(subElem)
   setEntityIntValue(refresh, 1)
end
