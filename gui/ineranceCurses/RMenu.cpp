/*
**Copyright (C) <2013> <YIRL_Team>
**
**This program is free software: you can redistribute it and/or modify
**it under the terms of the GNU Lesser General Public License as published by
**the Free Software Foundation, either version 3 of the License, or
**(at your option) any later version.
**
**This program is distributed in the hope that it will be useful,
**but WITHOUT ANY WARRANTY; without even the implied warranty of
**MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**GNU General Public License for more details.
**
**You should have received a copy of the GNU Lesser General Public License
**along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include  "debug_core.h"
#include	"RMenu.hh"
#include	"CWidget.hh"

void	RMenu::init()
{
  CWidget **tmp = ((CWidget **)&_helper);
  *tmp = new CWidget(*this);
}

RMenu::~RMenu()
{
  delete reinterpret_cast<CWidget *>(_helper);
}

bool RMenu::render()
{
  CWidget *wid = reinterpret_cast<CWidget *>(_helper);
  unsigned int i = 0;
  Entity *tmp;

  wrefresh(wid->win);
  wmove(wid->win, 1, 1);
  while (i < _totalLen)
  {
    tmp = getEntity(_links, i);
    DPRINT_INFO("\tentity at index %d is : %s\t[%s:%d]\n", i, tryGetEntityName(tmp), __FILE__, __LINE__);
    tmp = getReferencedObj(tmp);
    DPRINT_INFO("tmp : %s\t[%s:%d]\n", tryGetEntityName(tmp), __FILE__, __LINE__);
    // addstr(tmp->name);
    // addch('\n');
    // addstr(((StructEntity *)tmp)->structName);
    // addch('\n');
    if (i == _currentPos)
      wattron(wid->win, COLOR_PAIR(1));
    waddstr(wid->win, getStringVal(findEntity(tmp, "text")));
    wmove(wid->win, 2 + i, 1);
    if (i == _currentPos)
    	wattroff(wid->win, COLOR_PAIR(1));
    ++i;
  }
  // addstr(gameElem->name);
  // addch('\n');
  // addstr(gameElem->structName);
  //redrawwin(wid->win);
  wrefresh(wid->win);
  return (true);
}

void RMenu::resize()
{
  CWidget *wid = reinterpret_cast<CWidget *>(_helper);

  wid->resize();
}
