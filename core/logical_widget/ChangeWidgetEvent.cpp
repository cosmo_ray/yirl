#include	"ChangeWidgetEvent.hh"

extern "C" {
ChangeWidgetEvent::ChangeWidgetEvent(StructEntity *current, StructEntity *replacement)
  : GuiEvent(CHANGE_WIDGET)
  ,_current(current)
  , _replacement(replacement)
{
}

StructEntity *ChangeWidgetEvent::getCurrent()
{
  return (_current);
}

StructEntity *ChangeWidgetEvent::getReplacement()
{
  return (_replacement);
}
}
