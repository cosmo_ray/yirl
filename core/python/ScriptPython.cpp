/*
**Copyright (C) <2013> <YIRL_Team>
**
**This program is free software: you can redistribute it and/or modify
**it under the terms of the GNU Lesser General Public License as published by
**the Free Software Foundation, either version 3 of the License, or
**(at your option) any later version.
**
**This program is distributed in the hope that it will be useful,
**but WITHOUT ANY WARRANTY; without even the implied warranty of
**MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**GNU General Public License for more details.
**
**You should have received a copy of the GNU Lesser General Public License
**along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "ScriptPython.hh"

ScriptPython::ScriptPython(const std::string &mod, const std::string &name)
  : Script(mod, name)
{
  Py_Initialize();
  file_ = PyFile_FromString(const_cast<char *>(getFileName(mod, name).c_str()), "r");
}

ScriptPython::~ScriptPython()
{
  Py_Finalize();
}

Entity *ScriptPython::call(FunctionEntity *, va_list *)
{
  return (NULL);
}
