/*
**Copyright (C) <2013> <YIRL_Team>
**
**This program is free software: you can redistribute it and/or modify
**it under the terms of the GNU Lesser General Public License as published by
**the Free Software Foundation, either version 3 of the License, or
**(at your option) any later version.
**
**This program is distributed in the hope that it will be useful,
**but WITHOUT ANY WARRANTY; without even the implied warranty of
**MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**GNU General Public License for more details.
**
**You should have received a copy of the GNU Lesser General Public License
**along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include	<typeinfo>
#include	<typeinfo>
#include	<iostream>
#include	"luaAPI.hh"
#include	"yirlAPI.hh"
#include	"ScriptLua.hh"
#include	"EntityStructPrototypes.hh"
#include	"debug.h"

ScriptLua::ScriptLua(const std::string &mod, const std::string &name)
  : Script(mod, name)
{
  init(mod, name);
}

ScriptLua::~ScriptLua()
{
  lua_close(_l);
}

void ScriptLua::init(const std::string &mod, const std::string &name)
{
  static lua_State *l = NULL;

  if (l == NULL) {
    //creat lua vm
    l = luaL_newstate();
    //init lua vm
    luaL_openlibs(l);
  }
  _l = l;
  //load file
  if (luaL_dofile(_l, getFileName(mod, name).c_str()))
    {
      DPRINT_ERR("error in opening script %s\nerror:%s\n", name.c_str(), lua_tostring(_l,-1));
    }
  else
    registreCall();
}

void ScriptLua::registreCall(void)
{
  /*love lua*/
  lua_register(_l, "yAnd", luaYAnd);

  /*Entity Manager*/
  lua_register(_l, "addEntity", luaAddEntity);
  lua_register(_l, "getEntity", luaGetObject);
  lua_register(_l, "getEntityByName", luaGetEntityByName);

  /*Struct*/
  lua_register(_l, "getEntityMenber", luaGetMenberEntity);
  lua_register(_l, "tryGetStructName", luaTryGetStructEntityName);

  /*array*/
  lua_register(_l, "getArrayMenber", luaGetArrayMenber);
  lua_register(_l, "arrayPushBack", luaArrayPushBack);
  lua_register(_l, "arrayPopBack", luaArrayPopBack);
  lua_register(_l, "getLen", luaGetArrayLen);
  lua_register(_l, "createArray", luaCreateArray);
  lua_register(_l, "setRefAt", luaSetRefVal);
  lua_register(_l, "arrayRemove", luaArrayRemove);
  
  /*string*/
  lua_register(_l, "getEntityStringValue", luaGetStringVal);  

  /*int*/
  lua_register(_l, "getEntityIntValue", luaGetIntVal);
  lua_register(_l, "setEntityIntValue", luaSetIntVal);

  /*float*/
  lua_register(_l, "getEntityFloatValue", luaGetFloatVal);
  lua_register(_l, "setEntityFloatValue", luaSetFloatVal);

  /*ref*/
  lua_register(_l, "getReferencedObj", luaGetReferencedObj);
  lua_register(_l, "setEntityRefValue", luaSetRefVal);
  lua_register(_l, "createRef", luaCreateRef);


  /*function*/
  lua_register(_l, "getFuncNumberArgs", luaGetFunctionNumberArgs);
  lua_register(_l, "unsetFunction", luaUnsetFunction);
  lua_register(_l, "setFunction", luaSetFunction);

  /*other*/
  lua_register(_l, "getName", luaGetName);
  lua_register(_l, "setTimeType", luaSetTimeType);
  lua_register(_l, "endGame", luaEndGame);
  lua_register(_l, "copyEntity", luaCopyEntity);
  lua_register(_l, "call", luaCall);
  lua_register(_l, "callByName", luaCallByName);
  lua_register(_l, "pushChangeWidgetEvent", luaPushChangeWidgetEvent);

  /*Lib Sound*/
  lua_register(_l, "playSound", luaPlaySound);
}

Entity *ScriptLua::callByName(const char *name, int nbArg, va_list *ap)
{
  lua_getglobal(_l, name);
  if (lua_isnil(_l,-1))
    return NULL;

  Entity *tmp;

  for (unsigned int i = 0; i < nbArg; ++i)
    {
      tmp = va_arg(*ap, Entity *);
      DPRINT_INFO("pushing %p\n", tmp);
      lua_pushlightuserdata(_l, tmp);
    }
  lua_call(_l, nbArg, 1);
  return (Entity *)lua_touserdata(_l, 0);  
}

Entity *ScriptLua::call(FunctionEntity *func, va_list *ap)
{
  if (func == NULL)
    {
      DPRINT_ERR("func entoty is NULL\n");
      return NULL;
    }
  DPRINT_INFO("try to find %s in %p \n", getFunctionVal((Entity *)func), _l);
  if (!getFunctionVal((Entity *)func))
    return NULL;
  return callByName(getFunctionVal((Entity *)func), func->nArgs, ap);
}
