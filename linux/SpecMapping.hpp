/*
**Copyright (C) <2013> <YIRL_Team>
**
**This program is free software: you can redistribute it and/or modify
**it under the terms of the GNU Lesser General Public License as published by
**the Free Software Foundation, either version 3 of the License, or
**(at your option) any later version.
**
**This program is distributed in the hope that it will be useful,
**but WITHOUT ANY WARRANTY; without even the implied warranty of
**MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**GNU General Public License for more details.
**
**You should have received a copy of the GNU Lesser General Public License
**along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef SPECMAPPING_HPP
#define SPECMAPPING_HPP

/*Mapping for linux*/

#include <sys/mman.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>

class SpecMapping : public Mapping<SpecMapping>
{
  void* addr;
  unsigned int size;
public:
  SpecMapping(){}
  ~SpecMapping(){}

  /*Map the file in path
   Return NULL if fail or a void* for sucess*/
  void* mapping(const char * path)
  {
    int fd = open(path, O_RDWR);
    struct stat tmp;

    if (fd == -1)
      return NULL;
    fstat(fd, &tmp);
    this->size = tmp.st_size;
    this->addr = mmap(NULL, size, PROT_READ | PROT_WRITE, MAP_PRIVATE, fd, 0);
    return addr;
  }
  /*Unmap the file*/
  bool unmapping()
  {
    munmap(addr, size);
    return true;
  }
};

#endif
