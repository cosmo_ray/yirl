#!/bin/python3

# Copyright (C) <2013> <YIRL_Team>
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import sys
import os

class Define:
    def __init__( self, defines):
        self.lines = defines.split()
        for i, l in enumerate(self.lines):
            self.lines[i] = l.split('=')
            self.lines[i][1] = self.lines[i][1].split('|')
    def getLine( self, idx):
        for i in self.lines:
            if idx in i:
                return i[1][0]
    def getType( self, idx):
        for i in self.lines:
            if idx in i:
                return i[1][1]

def quotedName(name):
    return ("\""+name+"\"")

def createKey(nameKey):
    return (quotedName(nameKey)+":")

def createCase(defObj, entityKey, pos):
    entityName = defObj.getLine(entityKey)
    entityType = defObj.getType(entityKey)
    ret = "{" 
    ret += createKey("_name") + quotedName(mapName + entityName + str(pos)) + ","
    ret += createKey("_extend") + quotedName(entityName) + ","
    ret += createKey("_type") + quotedName(entityType) + "}"
    return (ret)

mapName = sys.argv[1]
defines = sys.argv[2]

f = open( mapName,'r')
f2 = open( defines,'r')
defObj = Define(f2.read())
mapName = os.path.splitext(mapName)[0]
m = f.read()
l = len(m)
width = 0
height = 0
toCreate = ""
jsonToGenere = "{\n"
jsonToGenere += "\"impl\":{\n"
jsonToGenere += "\"" + mapName + "\":{\n"
elems = "\"elems\" : [\n"

for i, c in enumerate(m):
    if c == '\n':
        if width == 0:
            width = i
        height += 1
        elems += "\n"
        continue
    elems += "[" + createCase(defObj, c, i - height) + "],"
f.close
if height:
    if elems[-1:] == '\n':
        elems = elems[:-1]
    elems = elems[:-1]
elems += "\n]\n"
jsonToGenere += ("\"_type\":" + "\"Map\"" + ",\n")
jsonToGenere += ("\"w\":" + str(width) + ",\n")
jsonToGenere += ("\"h\":" + str(height) + ",\n")
jsonToGenere += elems
jsonToGenere += "}\n"
jsonToGenere += "}\n"
jsonToGenere += "}\n"
print(jsonToGenere)
