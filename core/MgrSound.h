#ifndef __MgrSound__
#define __MgrSound__

#include <string>
#include <list>
#include <fmodex/fmod.h>

class MgrSound{
    public:
        MgrSound();
        ~MgrSound();
        int play(const std::string &);

        std::string getArtist();
        std::string getTitle();
        unsigned int getLength();
        unsigned int getCurrentPos();

        void pause();
        void resume();
        void avancer(unsigned int);
        void reculer(unsigned int);
        void setVolume(float);

        void update();
    private:
        FMOD_SYSTEM  *sys;
        FMOD_SOUND   *sound;
        FMOD_CHANNEL *channel;
};


#endif
