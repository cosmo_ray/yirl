/*
**Copyright (C) <2013> <YIRL_Team>
**
**This program is free software: you can redistribute it and/or modify
**it under the terms of the GNU Lesser General Public License as published by
**the Free Software Foundation, either version 3 of the License, or
**(at your option) any later version.
**
**This program is distributed in the hope that it will be useful,
**but WITHOUT ANY WARRANTY; without even the implied warranty of
**MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**GNU General Public License for more details.
**
**You should have received a copy of the GNU Lesser General Public License
**along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef CALL_EVENT_HH
#define CALL_EVENT_HH

#include	<string>
#include	<cstdarg>
#include	"entity.h"
#include	"CoreEvent.hh"

class  CallEvent : public CoreEvent
{
public:
  CallEvent(FunctionEntity *, va_list *);

  FunctionEntity 	 *getToCall();
  va_list		*getAP();
private:
  FunctionEntity 	*toCall_;
  va_list		*ap_;
};

#endif
