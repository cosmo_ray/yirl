How to switch between textual and graphical screen
--------------------------------------------------
There is two lines in the MakeFile to link the curses or the SDL2 libs.
Just comment the one you don't want and uncomment the one you want to use.
curses : textual
SDL2 : graphical
