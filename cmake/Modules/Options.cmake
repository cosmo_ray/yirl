####################################
# Here are the command lines options
####################################
option(DEBUG "DEBUG mode" ON)

set(gui "curses" CACHE STRING "Helpstring")
set(MODULE_DIRECTORY "${RESSOURCE_INSTALL_DIR}/modules/" CACHE PATH "the path of the directory containing the yirl's modules")
set(STARTING_POINT "StartingPoint" CACHE STRING "Helpstring")