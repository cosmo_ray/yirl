/*
**Copyright (C) <2013> <YIRL_Team>
**
**This program is free software: you can redistribute it and/or modify
**it under the terms of the GNU Lesser General Public License as published by
**the Free Software Foundation, either version 3 of the License, or
**(at your option) any later version.
**
**This program is distributed in the hope that it will be useful,
**but WITHOUT ANY WARRANTY; without even the implied warranty of
**MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**GNU General Public License for more details.
**
**You should have received a copy of the GNU Lesser General Public License
**along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef		MENU_HH_
# define	 MENU_HH_

# include	"Widget.hh"
# include	"entity.h"

/** Widget contening Menu
 *
 * This class is made to permite to create content of type Menu
 * A Menu is a set of links.
 * A link is some text bind to a Script call or Contening a replacement for one widget.
 * There is 3 type of link:
 * The action link permeting t call a script
 * DirectLink permeting to replace this widget with another
 * InderectLink permeting to replace a widget by another one
 */
class Menu : public Widget
{
public:
  Menu(StructEntity *gameElem);
  Menu(StructEntity *gameElem, unsigned int x, unsigned int y, unsigned int w, unsigned int h);
  virtual ~Menu(){}

  InputStatue tryHandleInput(int ch);
  virtual bool render() = 0;
  virtual void resize() = 0;
protected:
  /**
   * Methode call in constructors contening all the code of the constructors
   * This methode is use to uniformise constructors code
   */
  void init();

  unsigned int _currentPos;
  unsigned int _totalLen;
  Entity *_links;
};

#endif
