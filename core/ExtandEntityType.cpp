/*
**Copyright (C) <2013> <YIRL_Team>
**
**This program is free software: you can redistribute it and/or modify
**it under the terms of the GNU Lesser General Public License as published by
**the Free Software Foundation, either version 3 of the License, or
**(at your option) any later version.
**
**This program is distributed in the hope that it will be useful,
**but WITHOUT ANY WARRANTY; without even the implied warranty of
**MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**GNU General Public License for more details.
**
**You should have received a copy of the GNU Lesser General Public License
**along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include	"EntityStructPrototypes.hh"


EntityStructPrototypes::FunctionInfo::~FunctionInfo()
{
  std::list<StructType>::iterator	it = arguments.begin();
  const std::list<StructType>::iterator	end = arguments.end();
  if (returnType.first == STRUCT)
    delete returnType.second;
  while (it != end)
    {
      if (it->first == STRUCT)
	delete it->second;
      ++it;
    }

}

EntityStructPrototypes::ExtandEntityType::ExtandEntityType(EntityType type, const std::string &name, bool isStatic)
  : _basicType(type), _name(name), _arrayInfo(NULL), _functionInfo(NULL), _isStatic(isStatic)
{
}

EntityStructPrototypes::ExtandEntityType::ExtandEntityType(EntityType type, const std::string &name, bool isStatic, const std::string &structName)
  : _basicType(type), _name(name), _structName(structName), _arrayInfo(NULL), _functionInfo(NULL), _isStatic(isStatic)
{
}

EntityStructPrototypes::ExtandEntityType::ExtandEntityType(EntityType type, const std::string &name, bool isStatic, EntityStructPrototypes::ExtandEntityType *arrayInfo)
  : _basicType(type), _name(name), _arrayInfo(arrayInfo), _functionInfo(NULL), _isStatic(isStatic)
{
}

EntityStructPrototypes::ExtandEntityType::ExtandEntityType(EntityType type, const std::string &name, bool isStatic, FunctionInfo *functionInfo)
  : _basicType(type), _name(name), _arrayInfo(NULL), _functionInfo(functionInfo), _isStatic(isStatic)
{
}

EntityStructPrototypes::ExtandEntityType::ExtandEntityType(const ExtandEntityType &other)
  : _basicType(other._basicType), _name(other._name), _structName(other._structName), _functionInfo(NULL), _isStatic(other._isStatic)
{
}

EntityStructPrototypes::ExtandEntityType::~ExtandEntityType()
{
  if (_arrayInfo != NULL)
    delete _arrayInfo;
  if (_functionInfo != NULL)
    delete _functionInfo;
}


bool EntityStructPrototypes::ExtandEntityType::operator==(const ExtandEntityType &other)
{
  return (other._basicType == _basicType && other._structName == _structName);
}

bool		EntityStructPrototypes::ExtandEntityType::isStatic() const
{
  return (_isStatic);
}

EntityType	EntityStructPrototypes::ExtandEntityType::getType() const
{
  return (_basicType);
}

const std::string &EntityStructPrototypes::ExtandEntityType::getContentName() const
{
  return (_name);
}

const std::string &EntityStructPrototypes::ExtandEntityType::getStructName() const
{
  return (_structName);
}

const EntityStructPrototypes::ExtandEntityType *EntityStructPrototypes::ExtandEntityType::getArrayInfo() const
{
  return (_arrayInfo);
}

const EntityStructPrototypes::FunctionInfo *EntityStructPrototypes::ExtandEntityType::getFunctionInfo() const
{
  return (_functionInfo);
}
