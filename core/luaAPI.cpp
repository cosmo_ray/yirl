/*
**Copyright (C) <2013> <YIRL_Team>
**
**This program is free software: you can redistribute it and/or modify
**it under the terms of the GNU Lesser General Public License as published by
**the Free Software Foundation, either version 3 of the License, or
**(at your option) any later version.
**
**This program is distributed in the hope that it will be useful,
**but WITHOUT ANY WARRANTY; without even the implied warranty of
**MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**GNU General Public License for more details.
**
**You should have received a copy of the GNU Lesser General Public License
**along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include	"debug.h"
#include	"luaAPI.hh"
#include	"MgrSound.h"
#include	<iostream>
#include	<vector>

int	luaYAnd(lua_State *L)
{
  lua_pushnumber(L, (int)lua_tonumber(L, 1) & (int)lua_tonumber(L, 2));
  return (1);
}

int	luaGetName(lua_State *L)
{
  if (lua_gettop(L) != 1 || !lua_islightuserdata (L, 1))
    {
      DPRINT_ERR("function arguments are incorect\n"
	     "real prototyre is: getName(lightuserdata entity)");
      return (-1);
    }
  lua_pushstring(L, tryGetEntityName((Entity *)lua_topointer (L, 1)));
  return (1);
}

int	luaGetObject(lua_State *L)
{
  if (lua_gettop(L) != 2 || !lua_isstring (L, 1) || !lua_isstring (L, 2))
    {
      DPRINT_ERR("function arguments are incorect\n"
	     "real prototyre is: getEntity(string objType, string objName)");
      return (-1);
    }
  lua_pushlightuserdata(L, getObject(lua_tostring (L, 1), lua_tostring (L, 2)));
  return (1);
}

int	luaGetEntityByName(lua_State *L)
{
  if (lua_gettop(L) != 1 || !lua_isstring (L, 1))
    {
      DPRINT_ERR("function arguments are incorect\n"
	     "real prototyre is: getEntityByName(string objName)");
      return -1;
    }
  lua_pushlightuserdata(L, getEntityByName(lua_tostring (L, 1)));
  return 1;
}


int	luaYeGet(lua_State *L)
{
  if (lua_gettop(L) != 2 || !lua_islightuserdata(L, 1)) {
    goto error;
  }
  if (lua_isstring(L, 2)) {
    lua_pushlightuserdata(L, findEntity(((Entity *)lua_topointer(L, 1)),
					lua_tostring (L, 2)));
    return 1;
  } else if (lua_isnumber(L, 2)) {
    lua_pushlightuserdata(L, getEntity(((Entity *)lua_topointer(L, 1)),
				       lua_tonumber (L, 2)));
    return 1;
  }
 error:
  DPRINT_ERR("function arguments for yeGet are incorect\n");
  return -1;
}

int	luaGetMenberEntity(lua_State *L)
{
  if (lua_gettop(L) != 2 || !lua_islightuserdata(L, 1) || !lua_isstring(L, 2))
    {
      DPRINT_ERR("function arguments are incorect\n"
	     "real prototyre is: getEntityMenber(lightuserdata entity, string objName)\n");
      return (-1);
    }
  lua_pushlightuserdata(L, findEntity(((Entity *)lua_topointer(L, 1)),
				      lua_tostring (L, 2)));
  return 1;
}

int	luaGetArrayLen(lua_State *L)
{
  DPRINT_INFO("luaGetArrayMenber\n");
  if (lua_gettop(L) != 1 || !lua_islightuserdata(L, 1))
    {
      DPRINT_ERR("function arguments are incorect\n"
	     "real prototyre is: getLen(lightuserdata entity)\n");
      return -1;
    }
  lua_pushnumber(L, getLen((Entity *)lua_topointer(L, 1)));
  return 1;
}

int	luaCopyEntity(lua_State *L)
{
  DPRINT_INFO("enter luaCopyEntity\n");
  if (lua_gettop(L) != 2 || !lua_islightuserdata(L, 1) ||
      !lua_islightuserdata(L, 2))
    {
      DPRINT_ERR("function arguments are incorect\n"
		 "real prototyre is: copyEntity(lightuserdata src, lightuserdata dest)\n");
      return -1;
    }
  lua_pushlightuserdata(L, copyEntity((Entity *)lua_topointer(L, 1),
				      (Entity *)lua_topointer(L, 2)));
  return 1;
}


int	luaCreateArray(lua_State *L)
{
  DPRINT_INFO("enter luaCreateArray\n");
  if (lua_gettop(L) != 2)
    {
      DPRINT_ERR("function arguments are incorect\n"
	     "real prototyre is: createArray(int type, lightuserdata father)\n");
      return -1;
    }
  lua_pushlightuserdata(L, creatArrayEntity((EntityType)lua_tonumber(L, 1),
					    (Entity *)lua_topointer(L, 2)));
  return 1;
}

int	luaGetArrayMenber(lua_State *L)
{
  DPRINT_INFO("enter luaGetArrayMenber\n");
  if (lua_gettop(L) != 2 || !lua_islightuserdata(L, 1) || !lua_isnumber(L, 2))
    {
      DPRINT_ERR("function arguments are incorect\n"
	     "real prototyre is: getArrayMenber(lightuserdata entity, int pos)\n");
      return -1;
    }
  lua_pushlightuserdata(L, getEntity(((Entity *)lua_topointer(L, 1)),
				     lua_tonumber (L, 2)));
  return 1;
}

int	luaArrayPopBack(lua_State *L)
{
  DPRINT_INFO("enter luaArrayPopBack\n");
  if (lua_gettop(L) != 1 || !lua_islightuserdata(L, 1))
    {
      DPRINT_ERR("function arguments are incorect\n"
	     "real prototyre is: arrayPopBack(lightuserdata entity)\n");
      return -1;
    }

  DPRINT_INFO("before popBack\n");
  lua_pushlightuserdata(L, popBack(((Entity *)lua_topointer(L, 1))));
  return 1;
}

int	luaArrayPushBack(lua_State *L)
{
  DPRINT_INFO("enter luaArrayPushBack\n");
  if (lua_gettop(L) != 2 || !lua_islightuserdata(L, 1) ||
      !lua_islightuserdata(L, 2))
    {
      DPRINT_ERR("function arguments are incorect\n"
	     "real prototyre is: arrayPushBack(lightuserdata entity, lightuserdata toPush)\n");
      return -1;
    }

  DPRINT_INFO("before PushBack\n");
  pushBack(((Entity *)lua_topointer(L, 1)), (Entity *)lua_topointer(L, 2));
  return 0;
}


int	luaGetStringVal(lua_State *L)
{
  if (lua_gettop(L) != 1 || !lua_islightuserdata(L, 1))
    {
      DPRINT_ERR("function arguments are incorect\n"
	     "real prototyre is: getEntityStringValue(lightuserdata entity)\n");
      return -1;
    }
  DPRINT_INFO("try to get:%s from %p\n",
	      getStringVal((Entity *)lua_topointer(L, 1)),
	      lua_topointer(L, 1));
  lua_pushstring(L, ((StringEntity *)lua_topointer(L, 1))->value);
  return 1;
}

int	luaGetIntVal(lua_State *L)
{
  DPRINT_INFO("luaGetIntVal\n");  
  if (lua_gettop(L) != 1 || !lua_islightuserdata(L, 1))
    {
      DPRINT_ERR("function arguments are incorect\n"
	     "real prototyre is: getEntityIntValue(lightuserdata entity)\n");
      return -1;
    }
  lua_pushnumber(L, (getIntVal(((Entity *)lua_topointer(L, 1)))));
  return 1;
}

int	luaGetFloatVal(lua_State *L)
{
  if (lua_gettop(L) != 1 || !lua_islightuserdata(L, 1))
    {
      DPRINT_ERR("function arguments are incorect\n"
	     "real prototyre is: getEntityIntValue(lightuserdata entity)\n");
      return -1;
    }
  lua_pushnumber(L, (getFloatVal(((Entity *)lua_topointer(L, 1)))));
  return 1;
}

int	luaSetFunction(lua_State *L)
{
  DPRINT_INFO("enter luaSetFunction\n");
  if (lua_gettop(L) != 2)
    {
      DPRINT_ERR("function arguments are incorect\n"
	     "real prototyre is: unsetFunction(lightuserdata entity, string functionName)\n");
      return -1;
    }
  setFunction(T_E(lua_topointer(L, 1)), lua_tostring(L, 2));
  return 0;
}

int	luaSetIntVal(lua_State *L)
{
  DPRINT_INFO("luaSetIntVal\n");  
  if (lua_gettop(L) != 2 || !lua_islightuserdata (L, 1) || !lua_isnumber (L, 2))
    {
     // DPRINT_ERR("function arguments are incorect\n""real prototyre is: setEntityIntValue(...)\n");

      return -1;
    }
  setInt(((Entity *)lua_topointer(L, 1)), lua_tonumber(L, 2));
  return 0;
}

int	luaSetFloatVal(lua_State *L)
{
  if (lua_gettop(L) != 2 || !lua_islightuserdata (L, 1) || !lua_isnumber (L, 2))
    {
      DPRINT_ERR("function arguments are incorect\n"
	     "real prototyre is: setEntityIntValue(...)\n");
      return (-1);
    }
  setFloat(((Entity *)lua_topointer(L, 1)), lua_tonumber(L, 2));
  return (0);
}

int	luaSetRefAt(lua_State *L)
{
    if (lua_gettop(L) != 3 || !lua_islightuserdata (L, 1) || !lua_islightuserdata (L, 2))
    {
      DPRINT_ERR("function arguments are incorect\n"
	     "real prototyre is: setRefAt(lightuserdata entity, lightuserdata toSet, number pos)\n");
      return (-1);
    }
    setRefAt(((Entity *)lua_topointer(L, 1)), lua_tonumber(L, 2), T_E(lua_topointer(L, 3)));
  return (0);
}

int	luaSetRefVal(lua_State *L)
{
  if (lua_gettop(L) != 2 || !lua_islightuserdata (L, 1) || !lua_islightuserdata (L, 2))
    {
      DPRINT_ERR("function arguments are incorect\n"
	     "real prototyre is: setEntityRefValue(lightuserdata entity, lightuserdata toSet)\n");
      return (-1);
    }
  setRef(((Entity *)lua_topointer(L, 1)), (Entity *)lua_topointer(L, 2));
  return (0);
}


int	luaGetReferencedObj(lua_State *L)
{
  if (lua_gettop(L) != 1 || !lua_islightuserdata(L, 1))
    {
      DPRINT_ERR("function arguments are incorect\n"
	     "real prototyre is: getReferencedObj(lightuserdata entity)\n");
      return (-1);
    }
  lua_pushlightuserdata(L, getReferencedObj(((Entity *)lua_topointer(L, 1))));
  return (1);
}

int	luaArrayRemove(lua_State *L)
{
  if (lua_gettop(L) < 2)
    {
      DPRINT_ERR("function arguments are incorect\n"
	     "real prototyre is: arrayRemove(lightuserdata array, lightuserdata toRemove, int deepSearch)\n");
      return (-1);
    }
  if (lua_gettop(L) == 2)
    lua_pushlightuserdata(L, arrayRemove(T_E(lua_topointer(L, 1)), T_E(lua_topointer(L,2)), 0));
  else
    lua_pushlightuserdata(L, arrayRemove(T_E(lua_topointer(L, 1)), T_E(lua_topointer(L,2)), lua_tonumber(L, 3)));
  return (1);
}


int	luaAddEntity(lua_State *L)
{
  if (lua_gettop(L) != 2)
    {
      DPRINT_ERR("function arguments are incorect\n"
	     "real prototyre is: addEntity(string objType, string objName)\n");
      return (-1);
    }
  lua_pushlightuserdata(L, addEntity(lua_tostring(L, 1), lua_tostring(L, 2)));
  return (1);
}

int	luaUnsetFunction(lua_State *L)
{
  DPRINT_INFO("enter luaUnsetFunction\n");
  if (lua_gettop(L) != 1)
    {
      DPRINT_ERR("function arguments are incorect\n"
	     "real prototyre is: unsetFunction(lightuserdata entity)\n");
      return (-1);
    }
  unsetFunction(T_E(lua_topointer(L, 1)));
  return (0);
}


int	luaCreateRef(lua_State *L)
{
  DPRINT_INFO("enter luaCreateRef\n");
  if (lua_gettop(L) != 2)
    {
      DPRINT_ERR("function arguments are incorect\n"
	     "real prototyre is: createRef(lightuserdata value, lightuserdata father)\n");
      return (-1);
    }
  lua_pushlightuserdata(L, creatRefEntity((Entity *)lua_topointer(L, 1), (Entity *)lua_topointer(L, 2)));
  return (1);
}

int	luaEndGame(lua_State *)
{
  endGame(true);
  return (0);  
}

int	luaTryGetStructEntityName(lua_State *L)
{
  DPRINT_INFO("enter luaTryGetStructEntityName\n");
  if (lua_gettop(L) != 1)
    {
      DPRINT_ERR("function arguments are incorect\n"
	     "real prototyre is: tryGetStructEntityName(lightuserdata value, lightuserdata father)\n");
      return (-1);
    }
  lua_pushstring(L, tryGetStructEntityName((Entity *)lua_topointer(L, 1)));
  return (1);
}

int	luaCallByName(lua_State *L)
{
  int	nArg = lua_gettop(L);
  const char *name;
  if (nArg < 1)
    {
      DPRINT_ERR("error in callByName\n"
		 "Prototype is callByName(name, ...)");
      return (-1);
    }

  name = lua_tostring(L, 1);
  switch (nArg)
    {
    case 1:
      DPRINT_INFO("callByName 0 args");
      lua_pushlightuserdata(L, callByName(name, 0));
      return (1);
    case 2:
      DPRINT_INFO("callByName 1 args");
      lua_pushlightuserdata(L, callByName(name, 1,
				    T_E(lua_topointer(L, 2))));
      return (1);
    case 3:
      DPRINT_INFO("callByName 2 args");
      lua_pushlightuserdata(L, callByName(name, 2,
				    T_E(lua_topointer(L, 2)),
				    T_E(lua_topointer(L, 3)))) ;
      return (1);
    default:
      DPRINT_ERR("screw you");
    }
  return (-1);
}

int	luaCall(lua_State *L)
{
  int	nArg = lua_gettop(L);
  if (!nArg)
    {
      DPRINT_ERR("error in luaCall: missing function entity\n");
      return (-1);
    } 

  if (!lua_topointer(L, 1))
    return (-1);
  int	entityNArg = getFunctionNumberArgs(T_CE(lua_topointer(L, 1)));
  (void)entityNArg;
  DPRINT_INFO("call with nArg %d %d\n", nArg, entityNArg);

  // DPRINT_ERR("something went really wrong $d $d", nArg, );

  switch (nArg)
    {
    case 1:
      DPRINT_INFO("call 0 args");
      lua_pushlightuserdata(L, call(T_F(lua_topointer(L, 1))));
      return (1);
    case 2:
      DPRINT_INFO("call 1 args");
      lua_pushlightuserdata(L, call(T_F(lua_topointer(L, 1)),
				    T_E(lua_topointer(L, 2))));
      return (1);
    case 3:
      DPRINT_INFO("call 2 args");
      lua_pushlightuserdata(L, call(T_F(lua_topointer(L, 1)),
				    T_E(lua_topointer(L, 2)),
				    T_E(lua_topointer(L, 3)))) ;
      return (1);
    default:
      DPRINT_ERR("screw you");
    }
  return (-1);
}

int    	luaPushChangeWidgetEvent(lua_State *L)
{
  if (lua_gettop(L) != 2 || !lua_islightuserdata (L, 1) || !lua_islightuserdata (L, 2))
    {
      DPRINT_ERR("function arguments are incorect\n"
		 "real prototyre is: pushChangeWidgetEvent(lightuserdata entity, lightuserdata toSet)\n");
      return -1;
    }

  pushChangeWidgetEvent((StructEntity *)lua_topointer(L, 1), (StructEntity *)lua_topointer(L, 2));
  return 0;
}

int	luaGetFunctionNumberArgs(lua_State *L)
{
  int	nArg = lua_gettop(L);
  if (!nArg)
    {
      DPRINT_ERR("error in luaGetFunctionNumberArgs: missing function entity\n");
      return -1;
    }
  lua_pushnumber(L, getFunctionNumberArgs(T_CE(lua_topointer(L, 1))));
  return 1;
}

int	luaGetType(lua_State *L)
{
  if (lua_gettop(L) != 1 || !lua_islightuserdata (L, 1)) {
    DPRINT_ERR("error in getType: missing entity\n");
    return -1;
  }
  lua_pushnumber(L, tryGetType(T_E(lua_topointer(L, 1))));
  return 1;
}

int	luaSetTimeType(lua_State *L)
{
  DPRINT_INFO("luaSetIntVal\n");  
  if (lua_gettop(L) != 1 || !lua_isnumber (L, 1))
    {
      DPRINT_ERR("function arguments are incorect\n"
	     "real prototyre is: luaSetTimeType(int type)\n");
      return -1;
    }
  setTimeType(lua_tonumber(L, 1));
  return 0;
}

int	luaPlaySound(lua_State *L)
{
  if (lua_gettop(L) != 1 || !lua_isstring (L, 1))
    {
      DPRINT_ERR("function arguments are incorect\n"
		 "real prototyre is: playSound(string PathFile)");
      return (-1);
    }
  static std::vector<MgrSound *> test;
  test.push_back(new MgrSound());
  test[test.size()-1]->play(lua_tostring(L, 1));
  return (0);  
}
