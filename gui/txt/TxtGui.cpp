/*
<<<<<<< HEAD
**Copyright (C) <2013> <GOTTO Matthias>
**
**This program is free software: you can redistribute it and/or modify
**it under the terms of the GNU General Public License as published by
=======
**Copyright (C) <2013> <YIRL_Team>
**
**This program is free software: you can redistribute it and/or modify
**it under the terms of the GNU Lesser General Public License as published by
>>>>>>> 1d468711b7c1ecb4181d3fb16d9fb8756bc9c43d
**the Free Software Foundation, either version 3 of the License, or
**(at your option) any later version.
**
**This program is distributed in the hope that it will be useful,
**but WITHOUT ANY WARRANTY; without even the implied warranty of
**MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**GNU General Public License for more details.
**
<<<<<<< HEAD
**You should have received a copy of the GNU General Public License
=======
**You should have received a copy of the GNU Lesser General Public License
>>>>>>> 1d468711b7c1ecb4181d3fb16d9fb8756bc9c43d
**along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include	"yirlAPI.hh"
#include	"cstdlib"
#include	"TxtGui.hh"
#include	<unistd.h>
#include	<cstring>
#include	<iostream>
#include	<stdio.h>

const char	*cmdTab[] = {
  "attaque",
  "heal",
  "quit",
  NULL
};

extern	"C"
{
  IGui	*entry_point(int)
  {
    return new TxtGui();
  }
}

TxtGui::TxtGui()
{
  system("clear");
}

Entity *getMonster(int currentMonster)
{
  switch (currentMonster)
    {
    case 0:
      return (getObject("Mechant", "mage"));
    case 2:
      return (getObject("Mechant", "guerrier"));
    case 3:
      return (getObject("Mechant", "ogre"));
    case 4:
      return (getObject("Mechant", "rouge"));
    case 5:
      return (getObject("Mechant", "boss"));
    default:
      break;
    }
  return (NULL);
}

void	printMonsterStat(Entity *monster)
{
  std::cout << getStringVal(findEntity(monster, "name")) << " have: " << getIntVal(findEntity(monster, "pv")) << " pv" << std::endl;
}

void	printPjStat(Entity *pj)
{
  //std::cout << "addr:" << pj << std::endl;
  std::cout << "hello " << getStringVal(findEntity(pj, "name")) << " you have: " << getIntVal(findEntity(pj, "pv")) << " pv" << std::endl;
}

bool init()
{
  return (true);
}

bool TxtGui::tryGetInput()
{
  char	buf[254];
  int	i = 0;
  Entity *gameInfo = getObject("MonsterOrder", "mo");
  int hasPresent = getIntVal(findEntity(gameInfo, "hasPresent"));
  int currentMonster = getFloatVal(findEntity(gameInfo, "monster"));
  std::cout << currentMonster << std::endl;
  Entity *monster = getReferencedObj(getEntity(findEntity(gameInfo, "monsters"), currentMonster));
  Entity *pj =  getObject("Hero", "you");

  if (monster == NULL)
    {
      std::cout << "ta ta ta ta ta ta ta ta-ta\n" << std::endl;
      endGame(true);
      return (true);
    }
  if (!hasPresent)
    {
      setInt(findEntity(gameInfo, "hasPresent"), 1);
      std::cout << getStringVal(findEntity(monster, "description")) << std::endl;
      std::cout << getStringVal(findEntity(monster, "name")) << " comme to kill you" << std::endl;
    }

  printMonsterStat(monster);
  printPjStat(pj);
  std::cout << "type an action:" << std::endl;
  while (cmdTab[i] != NULL)
    {
      std::cout << i << "> " << cmdTab[i] << std::endl;
      i++;
    }
  i = read(0, buf, 254);
  buf[i - 1] = '\0';

  Entity *action;
  if (!strcmp(cmdTab[2], buf))
    {
      std::cout << "bye bye boo will not miss you\n" << std::endl;
      endGame(true);
      return (true);
    }
  else
    {
      if ((action = findEntity(pj, buf)) != NULL)
	{
	  std::cout << "name:" << pj->name << std::endl;
	  std::cout << "addr:" << pj << std::endl;
	  if (call((FunctionEntity *)action, pj, monster))
	    return (true);
	}
    }
  std::cout << buf << " is not a valide action\n"  << std::endl;
  return (tryGetInput());
}
