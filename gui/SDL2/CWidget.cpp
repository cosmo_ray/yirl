/*
**Copyright (C) <2013> <YIRL_Team>
**
**This program is free software: you can redistribute it and/or modify
**it under the terms of the GNU Lesser General Public License as published by
**the Free Software Foundation, either version 3 of the License, or
**(at your option) any later version.
**
**This program is distributed in the hope that it will be useful,
**but WITHOUT ANY WARRANTY; without even the implied warranty of
**MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**GNU General Public License for more details.
**
**You should have received a copy of the GNU Lesser General Public License
**along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include	"CWidget.hh"

CWidget::CWidget(const Widget &wid)
  :_wid(wid)
{
    _rect.h = _wid.getH() * getResY() / 1000;
    _rect.w = _wid.getW() * getResX() / 1000;
    _rect.x = _wid.getX() * getResX() / 1000;
    _rect.y = _wid.getY() * getResY() / 1000;
}

void	CWidget::fillBg(SDL_Surface *img)
{
  if (img) {
    SDL_Texture *texture = SDL_CreateTextureFromSurface(getSg().renderer, img);
    SDL_RenderCopy(getSg().renderer, texture, NULL, &_rect);
    SDL_DestroyTexture(texture);
  }
}

void	CWidget::fillBg(SDL_Renderer* renderer, short r, short g, short b, short a)
{
  SDL_Surface *textSurface =  SDL_CreateRGBSurface(0, _rect.w, _rect.h, 32, 0, 0, 0, 0);
  SDL_FillRect(textSurface, NULL, SDL_MapRGBA(textSurface->format, r, g, b, a));
  SDL_Texture* text = SDL_CreateTextureFromSurface(renderer, textSurface);
  SDL_RenderCopy(renderer, text, NULL, &_rect);
  SDL_RenderPresent(renderer);
  SDL_DestroyTexture(text);
  SDL_FreeSurface(textSurface);
}
