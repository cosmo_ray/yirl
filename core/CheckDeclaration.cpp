/*
**Copyright (C) <2013> <YIRL_Team>
**
**This program is free software: you can redistribute it and/or modify
**it under the terms of the GNU Lesser General Public License as published by
**the Free Software Foundation, either version 3 of the License, or
**(at your option) any later version.
**
**This program is distributed in the hope that it will be useful,
**but WITHOUT ANY WARRANTY; without even the implied warranty of
**MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**GNU General Public License for more details.
**
**You should have received a copy of the GNU Lesser General Public License
**along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include	"CheckDeclaration.hh"
#include	"EntityStructPrototypes.hh"
#include	"debug.h"
#include	"global.h"

CheckDec::CheckDec(char *parserBuffer, int fileId)
  : _structuresPrototypes(*(getPrototypeInstance())) 
  , _parserBuffer(parserBuffer)
  , _fileId(fileId)
  , _isStatic(false)
{
}

void		CheckDec::jmpComment()
{
  parser_set_jumpable_chars(_fileId, "\n");
  parser_get_next_word_nj(_fileId, _parserBuffer);
  parser_set_jumpable_chars(_fileId, " \t\n;");
}


void		CheckDec::jmpNextComments()
{
  while (parser_check_str(_fileId, "//"))
    {
      jmpComment();
    }
}

int CheckDec::handleStatic()
{
  _isStatic = true;
  return (findInStructDefDictionary());
}

int		CheckDec::findInStructDefDictionary()
{
  for (int i = 0; i < NBR_ENTITYTYPE; ++i)
    {
      if (parser_check_str(_fileId, _structDefDictionary[i].c_str()))
	return (i);
    }
  if (parser_check_str(_fileId, "static"))
    return (handleStatic());
  parser_get_alfanumeric_str(_fileId, _parserBuffer);
  if (strlen(_parserBuffer) && getPrototypeInstance()->isStructExisting(_parserBuffer))
    {
      _buff = _parserBuffer;
      return (STRUCT);
    }
  return -1;
}

EntityStructPrototypes::ExtandEntityType *CheckDec::checkArrayInfo()
{
  EntityStructPrototypes::ExtandEntityType *ret;
  int	type;

  if ((type = findInStructDefDictionary()) == -1)
    return (NULL);
  if (static_cast<EntityType>(type) == ARRAY || static_cast<EntityType>(type) == REF)
    {
      EntityStructPrototypes::ExtandEntityType *contantInfo;

      if (!parser_check_char(_fileId, '<'))
	return (NULL);
      if ((contantInfo = checkArrayInfo()) == NULL)
	return (NULL);
      ret = new EntityStructPrototypes::ExtandEntityType(static_cast<EntityType>(type), "", false, contantInfo);
      if (!parser_check_char(_fileId,'>'))
	{
	  DPRINT_ERR("forget to close the array or reference\n");
	  delete ret;	  
	  return (NULL);
	}
    }
  else if (static_cast<EntityType>(type) == STRUCT)
    ret = new EntityStructPrototypes::ExtandEntityType(static_cast<EntityType>(type), "", false, _buff);
  else
    ret = new EntityStructPrototypes::ExtandEntityType(static_cast<EntityType>(type), "", false);
  return (ret);
}

bool	CheckDec::arrayRefDec(EntityType type)
{
  EntityStructPrototypes::ExtandEntityType *contantInfo;

  if (!parser_check_char(_fileId, '<'))
    return (false);
  if ((contantInfo = checkArrayInfo()) == NULL)
    return (false);
  if (!parser_check_char(_fileId, '>'))
    {
      DPRINT_ERR("forget to close the array or reference\n");
      delete contantInfo;
      return (false);
    }
  parser_set_jumpable_chars(_fileId, ";\n \t");
  parser_get_next_word(_fileId, _parserBuffer);
  DPRINT("add array or a referance: %s\n", _parserBuffer);
  if (_isStatic)
    _prototype->addMenber<true>(type, _parserBuffer, contantInfo);
  else
    _prototype->addMenber<false>(type, _parserBuffer, contantInfo);
  return (true);
}

bool	CheckDec::functionDec()
{
  EntityStructPrototypes::FunctionInfo	*functionInfo;
  char name[256];
  strcpy(name, parser_get_next_word(_fileId, _parserBuffer));
  functionInfo = new EntityStructPrototypes::FunctionInfo;
  if (!parser_check_str(_fileId, "->"))
    return (false);
  int type = findInStructDefDictionary();
  while (parser_check_str(_fileId, "->"))
    {
      DPRINT("args: %s\n", _buff.c_str());
      if (type == STRUCT)
	functionInfo->arguments.push_back(std::pair<EntityType, std::string *>(static_cast<EntityType>(type), new std::string(_buff)));
      else
   	functionInfo->arguments.push_back(std::pair<EntityType, std::string *>(static_cast<EntityType>(type), NULL));
      type = findInStructDefDictionary();
    }
  DPRINT("ret: %s\n", _buff.c_str());
  if (_isStatic)
    _prototype->addMenber<true>(FUNCTION, name, functionInfo);
  else
    _prototype->addMenber<false>(FUNCTION, name, functionInfo);
  return (true);
}

bool	CheckDec::varDec(EntityType type)
{
  if (type == STRUCT)
    {
      parser_get_next_word(_fileId, _parserBuffer);
      DPRINT("add structure: %s : %s\n", _buff.c_str(), _parserBuffer);
      if (_isStatic)
	_prototype->addMenber<true>(type, _parserBuffer, _buff.c_str());
      else
	_prototype->addMenber<false>(type, _parserBuffer, _buff.c_str());
    }
  else if (type == ARRAY || type == REF)
    return (arrayRefDec(type));
  else if (type == FUNCTION)
    return (functionDec());
  else
    {
      parser_get_next_word(_fileId, _parserBuffer);
      DPRINT("add elem: %s\n", _parserBuffer);
      if (_isStatic)
	_prototype->addMenber<true>(type, _parserBuffer);
      else
	_prototype->addMenber<false>(type, _parserBuffer);
    }
  return (true);
}

bool	CheckDec::check()
{
  int	i;
  
  parser_get_next_word(_fileId, _parserBuffer);
  if ((_prototype = _structuresPrototypes.addStruct(_parserBuffer)) == NULL
      && (_prototype = getPrototypeInstance()->getPrototype(_parserBuffer)) == NULL)
    {
      DPRINT_ERR("invalide struct name: %s\n", _parserBuffer);
      return (false);
    }
  parser_set_jumpable_chars(_fileId, " \t");
  if (parser_check_char(_fileId, ';')) // it's a forward declaration
    {
      parser_set_jumpable_chars(_fileId, " \t\n;");
      return (true);
    }

  parser_set_jumpable_chars(_fileId, " \t\n;");
  if (!parser_check_char(_fileId, '{'))
    {
      _structuresPrototypes.rmStruct(_prototype->getName().c_str());
      DPRINT_ERR("invalide command in declaration: \"%s\"\n", _parserBuffer);
      DPRINT_ERR("have you forget \"{\" to open struct declaration ?\n");
      return (false);
    }
  jmpNextComments();
  while ((i = findInStructDefDictionary()) != -1)
    {
      varDec(static_cast<EntityType>(i));
      jmpNextComments();
      _isStatic = false;
    }
  if (parser_check_char(_fileId, '}')) //outch, it very creap
    {
      return (true);
    }
  DPRINT_ERR("invalide command in declaration: \"%s\"\n", _parserBuffer);
  DPRINT_ERR("you forget the \"}\" to close the structure\n");
  parser_get_next_word(_fileId, _parserBuffer);
  return (false);
}

CheckDec::~CheckDec()
{
}
