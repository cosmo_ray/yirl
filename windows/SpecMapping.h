/*
**Copyright (C) <2013> <YIRL_Team>
**
**This program is free software: you can redistribute it and/or modify
**it under the terms of the GNU Lesser General Public License as published by
**the Free Software Foundation, either version 3 of the License, or
**(at your option) any later version.
**
**This program is distributed in the hope that it will be useful,
**but WITHOUT ANY WARRANTY; without even the implied warranty of
**MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**GNU General Public License for more details.
**
**You should have received a copy of the GNU Lesser General Public License
**along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "../core/Mapping.hh"
#include <windows.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <iostream>
class SpecMapping : public Mapping<SpecMapping>
{
LPVOID view;
HANDLE file;
void*	addr;
public:
	SpecMapping(void){};
	~SpecMapping(void){};
	
	bool	unmapping()
	{
	UnmapViewOfFile(this->view);
	CloseHandle(this->file);
	return true;
	}
	
	void	*map(const char *path)
	{
	SYSTEM_INFO SysInfo;
	unsigned long file_size;
	DWORD dwSysGran;
	HANDLE hMap;

	this->file = CreateFile(TEXT(path), GENERIC_READ | GENERIC_WRITE, 0, NULL, OPEN_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL);
	if (this->file == INVALID_HANDLE_VALUE)
	{
		std::cout << "Error Open file" << std::endl;
		return NULL;
	}
	GetSystemInfo(&SysInfo);
	dwSysGran = SysInfo.dwAllocationGranularity;
	(void)dwSysGran;
	file_size = GetFileSize(this->file, NULL);
	hMap = CreateFileMapping(this->file, NULL, PAGE_READWRITE, 0, file_size, NULL);
	if (hMap == NULL)
	{
		std::cout << "Error Creating Mapping" << std::endl;
		return NULL;
	}
	this->view = MapViewOfFile(hMap, FILE_MAP_ALL_ACCESS, 0, 0 , file_size);
	if (this->view == NULL)
	{
		std::cout << "Error Creating Mapping 2 " << GetLastError() << std::endl;
		return NULL;
	}
	this->addr = (void *)this->view;
	return this->addr;
	}
	
};

