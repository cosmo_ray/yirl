/*
**Copyright (C) <2013> <Matthias Gatto>
**
**This program is free software: you can redistribute it and/or modify
**it under the terms of the GNU Lesser General Public License as published by
**the Free Software Foundation, either version 3 of the License, or
**(at your option) any later version.
**
**This program is distributed in the hope that it will be useful,
**but WITHOUT ANY WARRANTY; without even the implied warranty of
**MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**GNU General Public License for more details.
**
**You should have received a copy of the GNU Lesser General Public License
**along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include	<string>
#include	"MapContener.hh"
#include	"yirlAPI.hh"
MapContener::MapContener(StructEntity *gameElem)
  : Contener(gameElem, false)

{
  init();
}

MapContener::MapContener(StructEntity *gameElem, unsigned int x, unsigned int y, unsigned int w, unsigned int h)
  : Contener(gameElem, x, y, w, h, false)
{
  init();
}

void MapContener::init()
{
  Entity *type = findDirectEntity((Entity *)_elem, "type");
  Entity *links = findDirectEntity((Entity *)_elem, "elems");

  setString(type, "vertical");
  Contener::init();
  map_ = findDirectEntity((Entity *)_elem, "map");
  bottomLayer_ = findDirectEntity((Entity *)_elem, "bottom");
  
  Entity *mapCnt = getObject("Content", std::string(std::string(getName(T_E(_elem))) + "Map").c_str());
  if (mapCnt == NULL)
    mapCnt = addEntity("Content", std::string(std::string(getName(T_E(_elem))) + "Map").c_str());

  setRef(findEntity(mapCnt, "elem"), getReferencedObj(map_));
  setInt(findEntity(mapCnt, "occupedPourcent"), 80);
  pushBack(links, mapCnt);
  addContent((T_ST(mapCnt)), BACK);

  if (bottomLayer_)
    {
      Entity *bottomCnt = getObject("Content", std::string(std::string(getName(T_E(_elem))) + "Bottom").c_str());
      if (bottomCnt == NULL)
	bottomCnt = addEntity("Content", std::string(std::string(getName(T_E(_elem))) + "Bottom").c_str());
      setRef(findEntity(bottomCnt, "elem"), getReferencedObj(bottomLayer_));
      setInt(findEntity(bottomCnt, "occupedPourcent"), 20);
      pushBack(links, bottomCnt);
      addContent((T_ST(bottomCnt)), BACK);
    }
  _current = tryFindElem(T_ST(getReferencedObj(map_)));
}
