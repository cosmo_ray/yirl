/*
**Copyright (C) <2013> <YIRL_Team>
**
**This program is free software: you can redistribute it and/or modify
**it under the terms of the GNU Lesser General Public License as published by
**the Free Software Foundation, either version 3 of the License, or
**(at your option) any later version.
**
**This program is distributed in the hope that it will be useful,
**but WITHOUT ANY WARRANTY; without even the implied warranty of
**MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**GNU General Public License for more details.
**
**You should have received a copy of the GNU Lesser General Public License
**along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef	CHECKIMPLEMENTATION_HH
#define	CHECKIMPLEMENTATION_HH

#include	"entity.h"

namespace	checkCnt
{
  struct	CheckCntInfo;
}

/**!
** \class CheckImpl
** \brief Verifie l'implementation des CNT
**/
class	CheckImpl
{
public:
/**!
** \brief Constructeur de la classe
** \param Prends le manager d'entite et les informations pour les CNT
**/
  CheckImpl(checkCnt::CheckCntInfo &, EntityManager &entityManager);
 /**!
 ** \brief Destructeur de la classe
 **/
  ~CheckImpl();

 /**! 
 ** \biref check principal qui appelle les autres 
 **/
  bool check();
  /**!
  ** \brief Check les declarations des entites
  **/
  bool		checkImplDeclareEntity();
  /**!
  ** \brief Renvoie une structure parent se trouvant de la liste _parentEntitys
  ** \return renvoi la structure parent ou NULL si cela echoue
  **/
  StructEntity	*getParentStruct();
  /**!
  ** \brief set l'entite en parametre comme etant static
  ** \return renvoi true si cela est bon et false si ca echoue
  **/
  bool		setStatic(Entity *entity);
  /**!
  ** \brief set la fonction dans l'entite
  ** \param l'entite question
  ** \return renvoi true si cela est bon et false si ca echoue
  **/
  bool		setFunction(Entity *entity);
  /**!
  ** \brief check l'implementation d'un array
  ** \param entite qui recupere array
  ** \return renvoi true si cela est bon et false si ca echoue
  **/
  bool		checkImplSetArray(Entity *entity);
  /**!
  ** \brief verifie l'implementation d'une reference et la set
  ** \param entite qui recupere la reference
  ** \return true si cela est bon false si ca echoue
  **/
  bool		checkImplSetRef(Entity *entity);
  /**!
  ** \brief verifie la valeur pour une enttie et la set
  ** \param l'entite qui la recupere
  ** \return renvoie true si cela est bon et false si ca echoue
  **/
  bool		checkSetValueEntity(Entity *entity);
  /**!
  ** \brief verifie l'implementation d'un tableau de valeur et le set
  ** \param l'entite qui recupere el tableau de valeure
  ** \return true si cela marche et false si ca rate
  **/
  bool		checkImplSetArrayValue(Entity *entity);
  /**!
  ** \brief verifie implementation d'une entite et set les valeurs de base
  ** \return renvoi true si cale marche et false si ca rate
  **/
  bool		checkImplSetEntityElem();

private:
  checkCnt::CheckCntInfo &_info; /*!<*/
  EntityManager	&_entityManager;
  std::list<Entity *>	_parentsEntitys;
};

#endif
