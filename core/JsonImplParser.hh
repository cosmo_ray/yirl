/*
**Copyright (C) <2013> <YIRL_Team>
**
**This program is free software: you can redistribute it and/or modify
**it under the terms of the GNU Lesser General Public License as published by
**the Free Software Foundation, either version 3 of the License, or
**(at your option) any later version.
**
**This program is distributed in the hope that it will be useful,
**but WITHOUT ANY WARRANTY; without even the implied warranty of
**MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**GNU General Public License for more details.
**
**You should have received a copy of the GNU Lesser General Public License
**along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef	JSON_IMPL_PARSER_HH
#define	JSON_IMPL_PARSER_HH

#include	"EntityManager.hh"
#include	"CheckJson.hh"
#include	"EntityStructPrototypes.hh"

namespace LoadJson
{
  class JsonImplParser
  {
  private:
    std::list<std::pair<RefEntity*, std::string>>	_references;
    EntityManager		&_em;
    EntityStructPrototypes	&_esp;
    const Json::Value	       	&_root;
    std::list<Entity *>		_parentList;
    // std::list<>

  public:
    JsonImplParser(EntityManager &em, const Json::Value &root);
    JsonImplParser(EntityManager &em, EntityStructPrototypes &esp,
		   const Json::Value &root);

    /**
     * Look for the entity ::name:: in EntityManager or if not found in the root Json value.
     * @return The entity if found, NULL otherwise
     */
    Entity*		findEntityByName(const std::string& name);

    /**
     * @return The closest parent which is a structure
     */
    StructEntity*	getParentStruct();
    
    /**
     * Copy the string from value into the entity value
     */
    bool	setValueToEntity(StringEntity* entity, const Json::Value& value,
				 __attribute__((unused)) const std::string& entityName = "");

    /**
     * Copy the function from value into the entity value
     */
    bool	setValueToEntity(FunctionEntity* entity, const Json::Value& value,
				 __attribute__((unused)) const std::string& entityName);
    
    /**
     * Look for the reference and set it to the Entity
     * @return True if reference entity is found and entity has the REF type or false
     */
    bool	setValueToEntity(RefEntity* entity, const Json::Value& value,
				 const std::string& entityName = "");

    /**
     * Set the array from the json to the entity
     */
    bool	setValueToEntity(ArrayEntity* entity, const Json::Value& value,
				 bool shouldExtand);

    bool	setValueToEntity(StructEntity* entity,
				 const Json::Value& value,
				 std::list<std::string> *tryOpenLst);

    /**
     * Set the array from the json to the entity
     */
    bool	setValueToEntity(StructEntity* entity, const Json::Value& value,
				 const std::string& entityName = "");

    /**
     * Set the value from the json to the entity
     */
    bool	setValueToEntity(StaticEntity* entity, const Json::Value& value,
				 const std::string& entityName = "");

    /**
     * Get and set the value to the entity depending on its type
     */
    bool	setValueEntity(Entity* entity, const Json::Value& value,
			       const std::string& entityName = "");

    bool	setValueEntity(Entity* entity,
			       const Json::Value &value,
			       const std::string& entityName,
			       bool souldExtand);

    /**
     * Set the Entity from the Json Value with its attributes
     */
    Entity*	setEntity(const std::string& name,
			  const Json::Value &value);

    const Json::Value	entityToJson(const Entity* entity);
    void		resolveLinks();
  };

  /**
   * This method load the implementations of a json configuration file.
   * To add implementation in a json file insert a impl object with all implementations inside it.
   */
  bool loadImpl(Json::Value &root, EntityStructPrototypes &esp, EntityManager &em, bool resolveReferences = true);
}

#endif
