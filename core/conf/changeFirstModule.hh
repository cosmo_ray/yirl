/*
**Copyright (C) <2013> <YIRL_Team>
**
**This program is free software: you can redistribute it and/or modify
**it under the terms of the GNU Lesser General Public License as published by
**the Free Software Foundation, either version 3 of the License, or
**(at your option) any later version.
**
**This program is distributed in the hope that it will be useful,
**but WITHOUT ANY WARRANTY; without even the implied warranty of
**MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**GNU General Public License for more details.
**
**You should have received a copy of the GNU Lesser General Public License
**along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/*!
** \file changeFirstModule
** \brief permet de changer le premier module
** \author yirlV2Team
** 
*/

/*!
** \fn int changeFirstModule(const char *str)
** \brief fonction qui change les donner du premier module par le parametre STR
** \return true si ca reussie false si cela echoue
*/
int	changeWidLib(const char *str);
void	listWidLib();
int     changeFirstModule(const char *str);
void     listModules();
