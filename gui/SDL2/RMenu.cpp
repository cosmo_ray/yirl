/*
**Copyright (C) <2013> <YIRL_Team>
**
**This program is free software: you can redistribute it and/or modify
**it under the terms of the GNU Lesser General Public License as published by
**the Free Software Foundation, either version 3 of the License, or
**(at your option) any later version.
**
**This program is distributed in the hope that it will be useful,
**but WITHOUT ANY WARRANTY; without even the implied warranty of
**MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**GNU General Public License for more details.
**
**You should have received a copy of the GNU Lesser General Public License
**along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include	"RMenu.hh"
#include	"CWidget.hh"
#include	<SDL2/SDL.h>

extern SDL_Global sg;

void	RMenu::init()
{
  CWidget **tmp = ((CWidget **)&_helper);
  *tmp = new CWidget(*this);
}

RMenu::~RMenu()
{
  delete reinterpret_cast<CWidget *>(_helper);
}

bool RMenu::render()
{
  CWidget *wid = reinterpret_cast<CWidget *>(_helper);
  unsigned int i = 0;
  int	y = wid->rect().y;
  int	x = wid->rect().x;
  int text_width;
  int text_height;
  Entity *tmp = getEntity(_links, 0);

  SDL_Color color;
  color.r = 0;
  color.g = 0;
  color.b = 0;
  color.a = 250;

  wid->fillBg(sg.renderer, 255,255,255,255);
  while (tmp != NULL)
    {
      tmp = getReferencedObj(tmp);
      // SDL_Rect toDraw = sg.getRect();
      // toDraw.y = 16 * i;
      const char *txt = getStringVal(findEntity(tmp, "text"));
      if (i == _currentPos)
	color.b = 120;
      SDL_Surface *textSurface = TTF_RenderUTF8_Solid(sg.font, txt, color);
      if (i == _currentPos)
	color.b = 0;
      SDL_Texture* text = SDL_CreateTextureFromSurface(sg.renderer, textSurface);
      text_width = textSurface->w;
      text_height = textSurface->h;
      SDL_FreeSurface(textSurface);
      SDL_Rect renderQuad = {x , y, text_width, text_height };
      y +=text_height;
      SDL_RenderCopy(sg.renderer, text, NULL, &renderQuad);
      SDL_DestroyTexture(text);
      ++i;
      tmp = getEntity(_links, i);
    }
  SDL_RenderPresent(sg.renderer);
  return (true);
}

void RMenu::resize()
{
  CWidget *wid = reinterpret_cast<CWidget *>(_helper);

  wid->resize();
}
