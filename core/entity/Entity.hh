/*
**Copyright (C) <2013> <YIRL_Team>
**
**This program is free software: you can redistribute it and/or modify
**it under the terms of the GNU Lesser General Public License as published by
**the Free Software Foundation, either version 3 of the License, or
**(at your option) any later version.
**
**This program is distributed in the hope that it will be useful,
**but WITHOUT ANY WARRANTY; without even the implied warranty of
**MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**GNU General Public License for more details.
**
**You should have received a copy of the GNU Lesser General Public License
**along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef ENTITY_HH
#define ENTITY_HH

#include "entity.h"

/**
 * Mother abstract class of entity.
 * This class is made for people's thinking that C++ and Object is obviouselly easyerst to use and cleaner than C's one
 * But if you like programing just use entity.h
 */
class YEntity
{
protected:
  Entity	*entity;
  YEntity();
  virtual ~YEntity() 
  { 
    // else de DEBUG, à retirer plus tard.
    if (this->entity != NULL) 
      delete this->entity;
    else
      DPRINT_WARN("%s", "WARNING : Entity is NULL \n");
  }

public:
  // stoet = stringToEntityType mais en CPP
  static EntityType stoet(const char *str) { return stringToEntityType(str); }
  // ettos = entityTypeToString mais en CPP
  static const char *ettos(int type) { return entityTypeToString(type); }

  Entity *getYEntity() const {return entity;}
};

class YStructEntity: public YEntity
{
public:
  YStructEntity() { this->entity = creatStructEntity(NULL); }
  YStructEntity(Entity *father) { this->entity = creatStructEntity(father); }

  ~YStructEntity();

  Entity *get(int index) { return getEntity(this->entity, index); }
  Entity *find(const char *name) { return findEntity(this->entity, name); }
  Entity *findDirectly(const char *name) { return findDirectEntity(this->entity, name); }
};

class YIntEntity: public YEntity
{
  YIntEntity(int i) { this->entity = creatIntEntity(i, NULL); }
  YIntEntity(int i, Entity *father) { this->entity = creatIntEntity(i, father); }

  ~YIntEntity();

  void set(int value) { setInt(this->entity, value); }
};

class YFloatEntity: public YEntity
{
  YFloatEntity(double f) { this->entity = creatFloatEntity(f, NULL); }
  YFloatEntity(double f, Entity *father) { this->entity = creatFloatEntity(f, father); }

  ~YFloatEntity();

  void set(double value) { setFloat(this->entity, value); }
};

class YStringEntity : public YEntity
{
  YStringEntity(const char *str) { this->entity = creatStringEntity(str, NULL); }
  YStringEntity(const char *str, Entity *father) { this->entity = creatStringEntity(str, father); }

  ~YStringEntity();

  void set(const char *value) { setString(this->entity, value); }
};

class YRefEntity : public YEntity
{
  YRefEntity(Entity *e) { this->entity = creatRefEntity(e, NULL); }
  YRefEntity(Entity *e, Entity *father) { this->entity = creatRefEntity(e, father); }

  ~YRefEntity();

  void set(Entity *value) { setRef(this->entity, value); }
};

class YArrayEntity : public YEntity
{
public:
  YArrayEntity(EntityType t) { this->entity = creatArrayEntity(t, NULL); }
  YArrayEntity(EntityType t,Entity *father) { this->entity = creatArrayEntity(t, father); }

  ~YArrayEntity();

  Entity *get(int index);
  Entity *find(const char *name);
  Entity *findDirectly(const char *name);
  void resize(int size);
};

class YFunctionEntity : public YEntity
{
  YFunctionEntity(const char *str) { this->entity = creatFunctionEntity(str, NULL); }
  YFunctionEntity(const char *str, Entity *father) { this->entity = creatFunctionEntity(str, father); }

  ~YFunctionEntity();

  void set(const char *value) { setFunction(this->entity, value); }
};

/*class YGenEntity : public YEntity
{

};*/

class YStaticEntity : public YEntity
{
  YStaticEntity(Entity *e) { this->entity = creatStaticEntity(e, NULL); }
  YStaticEntity(Entity *e, Entity *father) { this->entity = creatStaticEntity(e, father); }

  ~YStaticEntity();

  //  void setValue(Entity *value);
};

#endif
