/*
**Copyright (C) <2013> <YIRL_Team>
**
**This program is free software: you can redistribute it and/or modify
**it under the terms of the GNU Lesser General Public License as published by
**the Free Software Foundation, either version 3 of the License, or
**(at your option) any later version.
**
**This program is distributed in the hope that it will be useful,
**but WITHOUT ANY WARRANTY; without even the implied warranty of
**MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**GNU General Public License for more details.
**
**You should have received a copy of the GNU Lesser General Public License
**along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include	<string.h>
#include	<iostream>
#include	"ElemAlocator.hh"
#include	"RContener.hh"
#include	"RMenu.hh"
#include	"RTextScreen.hh"
#include	"RTextEdit.hh"
#include        "RImgScreen.hh"
#include	"RMap.hh"
#include	"RMapContener.hh"

namespace WidgetFactory
{
  INPLEM_GET_ELEM(Menu)
  INPLEM_GET_ELEM(ImgScreen)
  INPLEM_GET_ELEM(TextScreen)
  INPLEM_GET_ELEM(Contener)
  INPLEM_GET_ELEM(TextEdit)
  INPLEM_GET_ELEM(Map)
  INPLEM_GET_ELEM(MapContener)
}

ElemAlocator::ElemAlocator()
{
  _renderTab.push_back(WidgetFactory::getMenu);
  _renderTab.push_back(WidgetFactory::getImgScreen);
  _renderTab.push_back(WidgetFactory::getTextScreen);
  _renderTab.push_back(WidgetFactory::getContener);
  _renderTab.push_back(WidgetFactory::getTextEdit);
  _renderTab.push_back(WidgetFactory::getMap);
  _renderTab.push_back(WidgetFactory::getMapContener); //this widget embed a contener, so we push a contener
  _renderTabWithParam.push_back(WidgetFactory::getMenu);
  _renderTabWithParam.push_back(WidgetFactory::getImgScreen);
  _renderTabWithParam.push_back(WidgetFactory::getTextScreen);
  _renderTabWithParam.push_back(WidgetFactory::getContener);
  _renderTabWithParam.push_back(WidgetFactory::getTextEdit);
  _renderTabWithParam.push_back(WidgetFactory::getMap);
  _renderTabWithParam.push_back(WidgetFactory::getMapContener); //this widget embed a contener, so we push a contener
}

ElemAlocator::~ElemAlocator()
{
}

Widget *ElemAlocator::alloc(StructEntity *gameElem, unsigned int x, unsigned int y, unsigned int w, unsigned int h)
{
  if (gameElem == NULL)
    return (NULL);
  for (unsigned int i = 0; i < _renderTab.size(); ++i)
    {
      if (!strcmp(_renderAssociationTable[i], gameElem->structName))
	{
	  return (_renderTabWithParam[i](gameElem, x, y, w, h));
	}
    }
  std::cerr << "The start entity is not renderable a Widget" << std::endl;
  return (NULL);
}

Widget *ElemAlocator::alloc(StructEntity *gameElem)
{
  if (gameElem == NULL)
    return (NULL);
  for (unsigned int i = 0; i < _renderTab.size(); ++i)
    {
      if (!strcmp(_renderAssociationTable[i], gameElem->structName))
	  {
		return (_renderTab[i](gameElem));
	  }
    }
  std::cerr << "The start entity is not renderable a Widget" << std::endl;
  return (NULL);
}


ElemAlocator	&ElemAlocator::getInstance()
{
  static	ElemAlocator ret;

  return (ret);
}
