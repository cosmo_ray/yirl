/*
**Copyright (C) <2013> <YIRL_Team>
**
**This program is free software: you can redistribute it and/or modify
**it under the terms of the GNU Lesser General Public License as published by
**the Free Software Foundation, either version 3 of the License, or
**(at your option) any later version.
**
**This program is distributed in the hope that it will be useful,
**but WITHOUT ANY WARRANTY; without even the implied warranty of
**MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**GNU General Public License for more details.
**
**You should have received a copy of the GNU Lesser General Public License
**along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef IMG_SCREEN_HH_
#define IMG_SCREEN_HH_

#include	<string>
#include	"Widget.hh"
/**
**\class Conteneur d'une image a l'ecran
**/
class ImgScreen : public Widget
{
public:
  ImgScreen(StructEntity *gameElem);
  ImgScreen(StructEntity *gameElem, unsigned int x, unsigned int y, unsigned int w, unsigned int h);
  ImgScreen(int, StructEntity *gameElem);
  ImgScreen(int, StructEntity *gameElem, unsigned int x, unsigned int y, unsigned int w, unsigned int h);
  virtual ~ImgScreen();

 /**!
**\brief Tente de voir sur l'input en parametre fait une acteur sur un element
**\param Un entier de la valeur de l'input
**\return Le statut de l'input apres passage dans le conteneur
**/
  virtual InputStatue tryHandleInput(int ch);
   /**!
  **\brief Affiche les elements contenues
  **/
  bool render() = 0;
   /**!
  ** \brief Redimensionne la fenetre
  **/
  virtual void resize() = 0;

protected:
  /**
   * Methode call in constructors contening all the code of the constructors
   * This methode is use to uniformise constructors code
   */
  void init();

  Entity *_next;
  Entity *_background;
private:
  ImgScreen();
};

#endif
