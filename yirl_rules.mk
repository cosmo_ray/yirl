$(OBJDIR):
	mkdir $(OBJDIR)

$(OBJDIR)/%.o   : $(ESRCDIR)/%.c
	$(CC) -c -o $@ $< $(CFLAGS) $(CONLYFLAGS)

$(OBJDIR)/%.o   : $(SRCDIR)/%.c
	$(CC) -c -o $@ $< $(CFLAGS) $(CONLYFLAGS)

$(OBJDIR)/%.o   : $(UTILSRCDIR)/%.c
	$(CC) -c -o $@ $< $(CFLAGS) $(CONLYFLAGS)

$(OBJDIR)/%.o   : $(SRCDIR)/%.cpp
	$(CXX) -c -o $@ $< $(CXXFLAGS)

$(OBJDIR)/%.o   : $(LWSRCDIR)/%.cpp
	$(CXX) -c -o $@ $< $(CXXFLAGS)

$(OBJDIR)/%.o   : $(ESRCDIR)/%.cpp
	$(CXX) -c -o $@ $< $(CXXFLAGS)

$(OBJDIR)/%.o   : $(UTILSRCDIR)/%.cpp
	$(CXX) -c -o $@ $< $(CXXFLAGS)

$(OBJDIR)/%.o   : $(CONFDIR)/%.cpp
	$(CXX) -c -o $@ $< $(CXXFLAGS)

$(OBJDIR)/%.o   : $(PYTHONDIR)/%.cpp
	$(CXX) -c -o $@ $< $(CXXFLAGS)
