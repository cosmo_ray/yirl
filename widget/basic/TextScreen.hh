/*
**Copyright (C) <2013> <YIRL_Team>
**
**This program is free software: you can redistribute it and/or modify
**it under the terms of the GNU Lesser General Public License as published by
**the Free Software Foundation, either version 3 of the License, or
**(at your option) any later version.
**
**This program is distributed in the hope that it will be useful,
**but WITHOUT ANY WARRANTY; without even the implied warranty of
**MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**GNU General Public License for more details.
**
**You should have received a copy of the GNU Lesser General Public License
**along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef TEXT_SCREEN_HH_
#define TEXT_SCREEN_HH_

#include	<string>
#include	"ImgScreen.hh"

/** Widget contening text screen
 *
 * This class is made to permite to create content of type TextScreen
 * A TextScreen is a simple screen contening text and sometime a background as we can see in VM.
 */
class TextScreen : public ImgScreen
{
public:
  TextScreen(StructEntity *gameElem);
  TextScreen(StructEntity *gameElem, unsigned int x, unsigned int y, unsigned int w, unsigned int h);
  virtual ~TextScreen();

  InputStatue tryHandleInput(int ch);
  bool render() = 0;
  virtual void resize() = 0;

protected:
  /**
   * Methode call in constructors contening all the code of the constructors
   * This methode is use to uniformise constructors code
   */
  void init();
  void resetAff();
  
  Entity *entityToAff_;
  std::string toAff_; // Text to print
};

#endif
