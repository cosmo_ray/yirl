
OBJDIR = $(YIRL_DEV_ROOT)obj/
SRCDIR  = $(YIRL_DEV_ROOT)core/
# logical_widget sources
LWSRCDIR = $(SRCDIR)logical_widget/
# entity sources
ESRCDIR = $(SRCDIR)entity/
# util dir
UTILSRCDIR = $(SRCDIR)util/
# python dir
PYTHONDIR = $(SRCDIR)python/

CONFDIR = $(SRCDIR)conf/
PARSERDIR = $(YIRL_DEV_ROOT)tool/parsing/
CURSESDIR = $(YIRL_DEV_ROOT)gui/ineranceCurses/
SDL2DIR = $(YIRL_DEV_ROOT)gui/SDL2/
WIDGETDIR = $(YIRL_DEV_ROOT)widget/basic/

LINUXDIR = $(YIRL_DEV_ROOT)/linux/
WINDOWSDIR = $(YIRL_DEV_ROOT)/windows/

SRC	= $(wildcard $(SRCDIR)*.cpp)
SRC	+= $(wildcard $(LWSRCDIR)*.cpp)
SRC	+= $(wildcard $(ESRCDIR)*.cpp)
SRC	+= $(wildcard $(UTILSRCDIR)*.cpp)
SRC	+= $(wildcard $(CONFDIR)*.cpp)
ifeq ($(WITH_PYTHON), 1)
SRC	+= $(wildcard $(PYTHONDIR)*.cpp)
endif

CSRC	= $(wildcard $(SRCDIR)*.c)
CSRC	+= $(wildcard $(ESRCDIR)*.c)
CSRC	+= $(wildcard $(UTILSRCDIR)*.c)

#OBJ     = $(addprefix $(OBJDIR)/, $(SRC:.cpp=.o))
#OBJ     += $(addprefix $(OBJDIR)/, $(CSRC:.c=.o))
OBJ     = $(addprefix $(OBJDIR)/,$(notdir $(SRC:.cpp=.o)))
OBJ     += $(addprefix $(OBJDIR)/,$(notdir $(CSRC:.c=.o)))

#hem, i don't have locate, but i have lua ...
#LUA_TEST=$(shell diff $(locate -r "liblua.so$" | sed -n 1~2p) >& /dev/null && echo OK || echo KO)
ifeq ($(OS), Windows_NT)
	LUA_TEST=KO
else
	LUA_TEST=OK
endif

IFLAGS	+= -I$(YIRL_DEV_ROOT) -I$(SRCDIR) -I$(LWSRCDIR) -I$(ESRCDIR) -I$(UTILSRCDIR) -I$(CONFDIR) -I$(PARSERDIR)

ifeq ($(LUA_TEST), KO)
	LDFLAGS_LUA = -llua -L$(YIRL_DEV_ROOT)lua
else
	LDFLAGS_LUA = $(shell pkg-config --libs lua)
endif

ifeq ($(OS), Windows_NT)
IFLAGS  += -I$(WINDOWSDIR)
CFLAGS  += -I$(YIRL_DEV_ROOT)include/ -D DEBUG #-Werror -
CXXFLAGS += $(CFLAGS) -std=gnu++0x
PARSERNAME = libparser.a
else
IFLAGS  += -I$(LINUXDIR)
LDFLAGS += -rdynamic -ldl
CFLAGS  += $(shell pkg-config --cflags lua) -O2   #-D DEBUG #-Werror 
CXXFLAGS += $(CFLAGS) -std=c++11
PARSERNAME = libparser.so
endif

ifeq ($(WITH_PYTHON), 1)
IFLAGS	+= -I$(PYTHONDIR)
CFLAGS	+= $(shell python2-config --cflags)
LDFLAGS += $(shell python2-config --ldflags)
endif

LDFLAGS += $(LDFLAGS_LUA) -L$(YIRL_DEV_ROOT)lib -lparser   -ljsoncpp # -lentity -lwidget_logical -lutil
CONLYFLAGS = -std=gnu99
CFLAGS += -W -Wall $(LINE_FLAGS) $(IFLAGS) 

