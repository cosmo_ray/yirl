/*
**Copyright (C) <2013> <YIRL_Team>
**
**This program is free software: you can redistribute it and/or modify
**it under the terms of the GNU Lesser General Public License as published by
**the Free Software Foundation, either version 3 of the License, or
**(at your option) any later version.
**
**This program is distributed in the hope that it will be useful,
**but WITHOUT ANY WARRANTY; without even the implied warranty of
**MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**GNU General Public License for more details.
**
**You should have received a copy of the GNU Lesser General Public License
**along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include	"CheckConfFile.hh"
#include	"debug.h"

class EntityManager;

namespace	checkCnt
{

  CheckCntInfo::CheckCntInfo(const std::string &fileName, EntityManager &entityManager)
    : fileId(parser_open_file(fileName.c_str()))
    , checkDec(buffer, fileId)
    , checkImpl(*this, entityManager)
    , mode(INPLEMENTATION)
  {
  }

  void checkImpl()
  {
  }

  void		verifyWord(CheckCntInfo &info)
  {
    if (info.mode == DEFINITION)
      {
	if (parser_check_str(info.fileId, "END"))
	  {
	    info.mode = INPLEMENTATION;
	    return;
	  }      
	info.checkDec.check();
      }
    else
      {
	if (parser_check_str(info.fileId, "DEF"))
	  {
	    info.mode = DEFINITION;
	    return;
	  }
	info.checkImpl.check();
      }
  }

  bool		fileReader(CheckCntInfo &info)
  {
    parser_set_jumpable_chars(info.fileId, " \t\n;");
    while (!parser_is_end_file(info.fileId))
      {
	if (parser_check_str(info.fileId, "//"))
	  jmpComment(info);
	else
	  verifyWord(info);
      }
    return (true);
  }

  bool		loadFile(const std::string &fileName, EntityManager &entityManager, int)
  {
    CheckCntInfo	info(fileName, entityManager);

    if (info.fileId == -1)
      {
	DPRINT_ERR("Could not opening the file %s\n", fileName.c_str());
	return false;
      }
    fileReader(info);
    return true;
  }

  void		jmpComment(CheckCntInfo &info)
  {
    // Skip line
    parser_set_jumpable_chars(info.fileId, "\n");
    parser_get_next_word_nj(info.fileId, info.buffer);
    parser_set_jumpable_chars(info.fileId, " \t\n;");
  }

  void		jmpNextComments(CheckCntInfo &info)
  {
    while (parser_check_str(info.fileId, "//"))
      {
	jmpComment(info);
      }
  }
}
