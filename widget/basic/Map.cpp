/*
**Copyright (C) <2013> <YIRL_Team>
**
**This program is free software: you can redistribute it and/or modify
**it under the terms of the GNU Lesser General Public License as published by
**the Free Software Foundation, either version 3 of the License, or
**(at your option) any later version.
**
**This program is distributed in the hope that it will be useful,
**but WITHOUT ANY WARRANTY; without even the implied warranty of
**MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**GNU General Public License for more details.
**
**You should have received a copy of the GNU Lesser General Public License
**along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include	<sstream>
#include	"Map.hh"
#include	"CallEvent.hh"
#include	"debug.h"
#include	"yirlAPI.hh"
#include	"GameLoop.hh"

Map::Map(StructEntity *gameElem)
  : Widget (TEXT_SCREEN, gameElem)
{
  init();
}

Map::Map(StructEntity *gameElem, unsigned int x, unsigned int y, unsigned int w, unsigned int h)
  : Widget (TEXT_SCREEN, gameElem, x, y, w, h)
{
  init();
}

Map::~Map()
{
  DPRINT_INFO("destroy widget");
  uninit();
}

void Map::init()
{
  unsigned int	basicLen;
  Entity *mapElems;
  Entity *mapElem;

  if (static_cast<CameraType>(getIntVal(yeGet(T_E(_elem), "cameraType"))) < CameraType::LAST_ELEM)
    _cType = static_cast<CameraType>(getIntVal(yeGet(T_E(_elem), "cameraType")));
  else
    _cType = FOLLOW_CARACTER;
  _wMap = getIntVal(yeGet(T_E(_elem), "w"));
  _hMap = getIntVal(yeGet(T_E(_elem), "h"));
  _elems = yeGet(T_E(_elem), "elems");
  _current = yeGet(T_E(_elem), "current");
  basicLen = getLen(_elems);
  _basicElem = yeGet(T_E(_elem), "basicElem");
  LOG_INFO("INIT MAP len 1:%d  len 2:%d\n", getLen(_elems), (_wMap * _hMap));

  if (basicLen != (_wMap * _hMap))
    manageArray(_elems, _wMap * _hMap, REF);
  
  for (unsigned int i = 0; i < getLen(_elems); ++i) {
      LOG_INFO("init map case");
      mapElems = yeGet(_elems, i);

      if (i >= basicLen) {
	  std::ostringstream oss;
	  oss << i;
	  manageArray(mapElems, 1, YINT);
	  Entity *cpBasicElem  = getObject(tryGetStructEntityName(getReferencedObj(_basicElem)),
					   std::string(std::string(getName(getReferencedObj(_basicElem))) + oss.str()).c_str());
	  
	  if (cpBasicElem == NULL) {
	    cpBasicElem = addEntity(tryGetStructEntityName(getReferencedObj(_basicElem)),
				    std::string(std::string(getName(getReferencedObj(_basicElem))) + oss.str()).c_str());
	  }

	  Entity *tmp = copyEntity(getReferencedObj(_basicElem), cpBasicElem);
	  setRefAt(mapElems, 0, tmp);
	}

      for (unsigned int i2 = 0; i2 < getLen(mapElems); i2++) {
	  LOG_INFO("setting %s at %d\n", tryGetEntityName(mapElems), i);
	  mapElem = getEntity(mapElems, i2);
	  mapElem = getReferencedObj(mapElem);
	  setInt(findEntity(mapElem, "pos"), i);
	  setInt(findEntity(mapElem, "refresh"), 1);
	  setRef(findEntity(mapElem, "map"), T_E(_elem));
	  DPRINT_INFO("get it: %d\n", getIntVal(findEntity(mapElem, "pos")));
	}
      
    }
  Widget::finishInit();
  _current = getReferencedObj(findEntity(T_E(_elem), "current"));
}

FunctionEntity *Map::tryGetActionId(Entity *currentElem, int id)
{
  Entity *actions = findEntity((Entity *)currentElem, "actions");
  Entity *action;
  unsigned int len;
  Entity *eId;

  DPRINT_INFO("%p\n", actions);
  if (!actions)
    return (NULL);
  len = getLen(actions);
  DPRINT_INFO("%d\n", len);
  for (unsigned int i = 0; i < len; ++i) {
      action = getReferencedObj(getEntity(actions, i));
      DPRINT_INFO("%p\n", action);
      eId = findEntity(action, "id");
      if (id == getIntVal(eId))
	return ((FunctionEntity *)findEntity(action, "action"));
    }
  return (NULL);
}

Map::InputStatue Map::tryHandleInput(int key)
{
  static const TimeSetting *ts = getTimeSetting();
  Entity *currentElem = findEntity((Entity *)_elem, "current");
  if (!currentElem) {
    LOG_ERR("current elem is NULL");
    return (NOTHANDLE);
  }
  currentElem = getReferencedObj(currentElem);
  if (!currentElem) {
    LOG_ERR("current elem is NULL");
    return (NOTHANDLE);
  }
  InputStatue ret = NOTHANDLE;
  FunctionEntity *moveFunc = tryGetActionId(currentElem, 0);
  int	pos;
  static int oldCmd;

  LOG_INFO("here: %s, %p\n", tryGetEntityName(currentElem), moveFunc);

  if (!moveFunc)
    goto exit;

  pos = getIntVal(findEntity((Entity *)currentElem, "pos"));

  switch (key)
    {
    case RIGHT_ARROW:
      if ((pos % _wMap + 1) < _wMap)
	pos += 1;
      else
	pos = -1;
      break;
    case LEFT_ARROW:
      if ((int)(pos % _wMap - 1) >= 0)
	pos -= 1;
      else
	pos = -1;
      break;
    case UP_ARROW:
      if ((int)(pos - _wMap) >= 0)
	pos -= _wMap;
      else
	pos = -1;
      break;
    case DOWN_ARROW:
      if ((pos + _wMap) < getLen(_elems))
	pos += _wMap;
      else
	pos = -1;
      break;
    default:
      if (oldCmd && ts->redoLastAction)
	tryHandleInput(oldCmd);	
      pos = -1;
      break;
    }
  if (pos >= 0)
    {
      oldCmd = key;
      Entity *arg = getEntity(_elems, pos);

      arg = getReferencedObj(getEntity(arg, getLen(arg) - 1));
      call(moveFunc, currentElem, arg, 0);
    }
 exit:
  return (ret);
}

Entity	*Map::getElemAt(unsigned int pos)
{
  return (getEntity(_elems, pos));
}

Entity	*Map::getElemAt(unsigned int x, unsigned int y)
{
  return (getElemAt(x + y * _wMap));
}

int	Map::getCaseLen(int i)
{
  Entity *tmp = getEntity(_elems, i);

  return (getLen(tmp));
}

void	Map::setRefresh(int i, unsigned int pos, int val)
{
  Entity *tmp = getEntity(_elems, i);

  if (pos >= getLen(tmp))
    tmp = getEntity(tmp, getLen(tmp) - 1); // bug if array size of 0 ?
  else if (getLen(tmp) > 0)
    tmp = getEntity(tmp, pos);

  if (tmp == NULL) {
    LOG_ERR("can not retrive entity on map");
    return;
  }
  
  tmp = findEntity(getReferencedObj(tmp), "refresh");
  if (tmp == NULL) {
    LOG_ERR("can not retrive entity on map");
    return;
  }

  setInt(tmp, val);
}

bool	Map::hasToRend(int i, unsigned int pos)
{
  Entity *tmp = getEntity(_elems, i);

  if (pos >= getLen(tmp))
    tmp = getEntity(tmp, getLen(tmp) - 1); // bug if array size of 0 ?
  else if (getLen(tmp) > 0)
    tmp = getEntity(tmp, pos);

  if (tmp == NULL) {
    LOG_ERR("can not retrive entity on map");
    return false;
  }
  
  tmp = findEntity(getReferencedObj(tmp), "refresh");
  if (tmp == NULL) {
    LOG_ERR("can not retrive entity on map");
    return false;
  }

  return (getIntVal(tmp));
}

int	Map::getMapId(int i, unsigned int pos)
{
  Entity *tmp = getEntity(_elems, i);
  //DPRINT_INFO("tmp type:%d\n", entityTypeToString(tryGetType(tmp)));

  if (pos >= getLen(tmp))
    tmp = getEntity(tmp, getLen(tmp) - 1); // bug if array size of 0 ?
  else if (getLen(tmp) > 0)
    tmp = getEntity(tmp, pos);

  if (tmp == NULL) {
      LOG_ERR("can not retrive entity on map");
      return -1;
    }
  //DPRINT_INFO("type tmp: %s\n", entityTypeToString(tryGetType(tmp)));
  tmp = findEntity(getReferencedObj(tmp), "id");

  if (tmp == NULL) {
    LOG_ERR("can not retrive entity on map");
    return -2;
  }

  return (getIntVal(tmp));
}
