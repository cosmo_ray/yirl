/*
**Copyright (C) <2013> <YIRL_Team>
**
**This program is free software: you can redistribute it and/or modify
**it under the terms of the GNU Lesser General Public License as published by
**the Free Software Foundation, either version 3 of the License, or
**(at your option) any later version.
**
**This program is distributed in the hope that it will be useful,
**but WITHOUT ANY WARRANTY; without even the implied warranty of
**MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**GNU General Public License for more details.
**
**You should have received a copy of the GNU Lesser General Public License
**along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef	CHECKDECLARATION_HH
#define	CHECKDECLARATION_HH

#include	<iostream>
#include	<string>
#include	<list>
#include	<algorithm>
#include	<unordered_map>
#include	"EntityManager.hh"
#include	"entity.h"
#include	"../tool/parsing/parser.h"
/**!
** \class CheckDec
** \brief Permet de check ldes declaration des entites
**/
class	CheckDec
{
public:
/**!
** \brief   Constructeur de la classe
** \param le char * est le buffer qui sera check par la classe et int le FD du fichier
**/
  CheckDec(char *, int);
  /**!
  ** Destructeur de CheckDec
  **/
  ~CheckDec();

 /**!
 ** \brief verifie si le fichier correspond bien
 ** \return renvoi true si cela correspond et false si c'est mauvais
 **/
  bool	check();
  /*Verifie sur la structure en parametre existe*/
  int	findBasicEntityType(const std::string &nameStruct);
  /*
  ** \brief Verifie si la prochaine structure dans _fileId est bonne
  ** \return renvoi son int ID
  ** Verifie si la prochaine structure dans _fileId est bonne
  */
  int	findInStructDefDictionary();

private:
  EntityStructPrototypes &_structuresPrototypes; /*! < */
  EntityStructPrototypes::StructPrototype *_prototype; /*! < */
  std::string	_buff; /* < buffer servant � recuperer des donnes du parseur*/
  char	*_parserBuffer; /* < buffer du parseur contenant les donnees*/
  int	_fileId; /*< ID du fichier en cours de lecture*/
  bool	_isStatic;/*< verifie si l'entite est static*/

  const std::string	_structDefDictionary[NBR_ENTITYTYPE] = {
    "struct",
    "int",
    "float",
    "string",
    "ref",
    "array",
    "function",
    "gen",
    "gen_array"
  }; /*< dictionnaire des type d'entite*/
/**!
** \brief Constructeur par copie
**/
  CheckDec(const CheckDec &);
  /**!
  ** \brief Surchage de l'operateur '=' pour la copie
  **/
  const CheckDec &operator=(const CheckDec &);
/**!
** \brief Permert verifier une entite static
** \return Renvoie ID de la struct
**/
  int	handleStatic();
  /**!
  ** \brief Permet de verifier la syntaxe de la fonction membre de l'entite
  ** \return Renvoie vrai si l'entite est bien ajouter et false si une erreur � lieu
  **/
  bool	functionDec();
  /**!
  ** \brief Permet de veriifier la syntaxe d'un tableau d'entite
  ** \return Renvoie vrai si l'entite est bien ajouter et false si une erreur � lieu
  **/
  bool	arrayRefDec(EntityType type);
  /*!
  ** \brief Permet de verifier la syntaxe du fichier de conf pour une variable d'entite
  ** \return Renvoie vrai si la variable � une bonne syntaxe et false si une erreur � lieu
  **/
  bool	varDec(EntityType type);
   /**!
  ** \brief Saute les prochains commentaires pour aller dans une partie declarer 
  ** \return void
  **/
  void	jmpNextComments();
  /**!
  ** \brief Saute le prochain commentaire
  ** \return void
  **/
  void	jmpComment();
  /**!
  ** \brief Regarde la syntaxe pour des information
  ** \return Retourne des informations sur le tableau actuellement lu 
  **/
  EntityStructPrototypes::ExtandEntityType *checkArrayInfo();
};

#endif
