/*
**Copyright (C) <2013> <YIRL_Team>
**
**This program is free software: you can redistribute it and/or modify
**it under the terms of the GNU Lesser General Public License as published by
**the Free Software Foundation, either version 3 of the License, or
**(at your option) any later version.
**
**This program is distributed in the hope that it will be useful,
**but WITHOUT ANY WARRANTY; without even the implied warranty of
**MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**GNU General Public License for more details.
**
**You should have received a copy of the GNU Lesser General Public License
**along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include	"ModuleLoader.hh"
#include <fstream> 
#include	"debug.h"

namespace ModuleLoader
{
  /**
   * Add the new script in scriptManager if type script configuration file is correct
   * @see ScriptLua
   * @param moduleName  the name of the module
   * @param file_id     the file_id beeing parsed. Used to get the script file name
   * @param _scriptManager  the script Manager who will contains all scripts
   * @return  0 if success, -1 otherwise
   */
  static int loadScript(const std::string &moduleName, const char* fileName, std::vector<Script *>& _scriptManager)
  {
    DPRINT_INFO("enter load script\n");

    DPRINT_INFO("Get the word '%s' in file\n", fileName);

    std::string scriptTypeFileName = std::string(MODULE_DIRECTORY + moduleName + "/script/type");
    int file_id = parser_open_file(scriptTypeFileName.c_str());
    DPRINT_INFO("%s\n", scriptTypeFileName.c_str());

    if (file_id < 0) {
      DPRINT_ERR("can not open %s\n", scriptTypeFileName.c_str());
      return (-1);
    }
    if (parser_check_str(file_id, "lua1")) {
    	_scriptManager.push_back(new ScriptLua(moduleName, fileName));
    }
    DPRINT_INFO("out of load script\n");
    return (0);
  }

  /**
   * Get the content file names and parse them to get entity definition and implementations
   * @param moduleName  the module's name
   * @param file_id     the file id being parsed. Used to get the content file name to parse
   * @param opt         
   * @return 0
   */
  static int	loadContent(const std::string &moduleName, const char* fileName, int opt)
  {
    DPRINT_INFO("JE PASSE PAR LOAD CONTENT\tmodule: %s\t[%s:%d]\n", moduleName.c_str(), __FILE__, __LINE__);
    int extPos = (int)(strrchr(fileName, '.') - fileName + 1);
    DPRINT_INFO("extension of module '%s' : %s\n", moduleName.c_str(), fileName + extPos);
    if (!strcmp("json", fileName + extPos))
    	LoadJson::loadFile(MODULE_DIRECTORY + moduleName + "/content/" + fileName, *(getMainEntityManager()), opt);
    else
    	checkCnt::loadFile(MODULE_DIRECTORY + moduleName + "/content/" + fileName, *(getMainEntityManager()), opt);
    DPRINT_INFO("JE SORT DE LOAD CONTENT\n");
    return (0);
  }

  /**
   * @param toCheck flag option to check
   * @param with LoadFlag enum to check the option with
   * @return true if toCheck contain the load flag in "with"
   */
  static inline bool checkFlag(int toCheck, LoadFlag with) { return (toCheck & with); }

  /**
   * Parse module starting point file.
   * Check if version is 1,
   * load content defined in the starting point file depending on the option :
   *  - script
   *  - content
   *  - load-nc
   *  - load
   * @param moduleName  the modules to load
   * @param _scriptManager
   * @param options     by default 0
   * @return -1 if fail :
   *  - bad starting point version
   *  - one line is not a valid configuration
   *  return true otherwise
   */
  bool loadGame(const std::string &moduleName,  std::vector<Script *>& _scriptManager, int options)
  {
    DPRINT_INFO("ENTER load game\t[loadGame:%s:%d]\n", __FILE__, __LINE__);
    char	buffer[256];
    int	file_id = parser_open_file(std::string(MODULE_DIRECTORY + moduleName + STARTING_POINT).c_str());

    if (file_id == -1) {
    	DPRINT_ERR("Could not opening the file \"%s\"\n", std::string(MODULE_DIRECTORY + moduleName + STARTING_POINT).c_str());
    	return false;
    }
    DPRINT_INFO("Parsing file \"%s\"\n", std::string(MODULE_DIRECTORY + moduleName + STARTING_POINT).c_str());
    parser_set_jumpable_chars(file_id, " :\n");
    
    DPRINT_INFO("ENTER while in load game\n");
    while (!parser_is_end_file(file_id))
    {
      if (parser_check_str(file_id, "version")) {
        if (!parser_check_char(file_id, '1')) {
          DPRINT_INFO("first fail\n");
          parser_get_next_word(file_id, buffer);
          DPRINT_ERR("bad StartingPoint version\n");
          parser_close_file(file_id);
          return false;
        }
      } else if (parser_check_str(file_id, "script")) {
        parser_get_next_word(file_id, buffer);
        if (checkFlag(options, NO_SCRIPT) == false)
          loadScript(moduleName, buffer, _scriptManager);
      } else if (parser_check_str(file_id, "content")) {
        parser_get_next_word(file_id, buffer);
        if (checkFlag(options, NO_CNT) == false)
          loadContent(moduleName, buffer, options);
      } else if (parser_check_str(file_id, "load-nc")) {
        parser_get_next_word(file_id, buffer);
        loadGame(buffer, _scriptManager, NO_CNT);
      } else if (parser_check_str(file_id, "load")) {
        parser_get_next_word(file_id, buffer);
        loadGame(buffer, _scriptManager);
      } else {
        parser_get_next_word(file_id, buffer);
        DPRINT_ERR("%s is not a valid element of starting point file\n", buffer);
        parser_close_file(file_id);
        return false;
      }
    }
    parser_close_file(file_id);
    return true;
  }
}
