/*
**Copyright (C) <2013> <YIRL_Team>
**
**This program is free software: you can redistribute it and/or modify
**it under the terms of the GNU Lesser General Public License as published by
**the Free Software Foundation, either version 3 of the License, or
**(at your option) any later version.
**
**This program is distributed in the hope that it will be useful,
**but WITHOUT ANY WARRANTY; without even the implied warranty of
**MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**GNU General Public License for more details.
**
**You should have received a copy of the GNU Lesser General Public License
**along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include	"debug.h"
#include	"Widget.hh"
#include	"CallEvent.hh"
#include	"yirlAPI.hh"
#include	"ChangeWidgetEvent.hh"

Widget::Widget(int type, StructEntity *gameElem)
  : _type(type), _x(0), _y(0), _w(1000), _h(1000), _elem(gameElem)
  , _mainEventStack(MainEventStack::getInstance())
  , _guiEventStack(GuiEventStack::getInstance())
{
  init();
}

Widget::Widget(int type, StructEntity *gameElem, unsigned int x, unsigned int y, unsigned int w, unsigned int h)
  : _type(type), _x(x), _y(y), _w(w), _h(h), _elem(gameElem)
  , _mainEventStack(MainEventStack::getInstance())
  , _guiEventStack(GuiEventStack::getInstance())
{
  init();
}

void Widget::uninit()
{
  Entity *func;

  func = findEntity(T_E(_elem), "destroy");
  if (func != NULL)
    {
      DPRINT_INFO("call %s\n", func->name);
      call(T_F(func), _elem);
    }
}


void Widget::finishInit()
{
    Entity *initFunc;
    initFunc = findEntity(T_E(_elem), "init");
    if (initFunc != NULL)
    {
      DPRINT_INFO("call %s\n", initFunc->name);
      call(T_F(initFunc), _elem);
    }
}

void Widget::init()
{
}

/**
 * If not implemented, tgyhggghyujthe widgets have to implemente the render method themselves
 */

void Widget::handleActionLink(Entity *link, va_list *ap)
{
  FunctionEntity *function = ((FunctionEntity *)findEntity(link, "link"));

  //callWithVA(function, ap);
  _mainEventStack.push(new CallEvent(function, ap));
  // DPRINT_INFO("Action %p with the function %s !\n", function, function->values);
}

void Widget::handleDirectCall(Entity *link)
{
  Entity *newEntity = getReferencedObj(findEntity(link, "link"));

  _guiEventStack.push(new ChangeWidgetEvent(_elem, reinterpret_cast<StructEntity *>(newEntity)));
  /*wprintw(_win, "Direct to %p\n"
	  , newEntity
	  );*/
}

void Widget::handleIndirectCall(Entity *link)
{
  Entity *newEntity = getReferencedObj(findEntity(link, "link"));
  Entity *oldEntity = getReferencedObj(findEntity(link, "bind"));

  _guiEventStack.push(new ChangeWidgetEvent(reinterpret_cast<StructEntity *>(oldEntity), reinterpret_cast<StructEntity *>(newEntity)));
  /*wprintw(_win, "Direct to %p\n"
	  , newEntity
	  );*/
}

void Widget::handleEnter(Entity *link, ...)
{
  //Entity *link = getReferencedObj(findEntity(entery, "link"));
  //std::cout << findEntity(entery, "link")->type << std::endl;
  if (link == NULL)
    {
      DPRINT_ERR("no link in %s\n", tryGetStructEntityName(T_E(getElem())));
      return ;
    }
  if (tryGetType(link) != STRUCT)
    {
      DPRINT_ERR("Link %s of type %s instead of Struct in %s\n", tryGetEntityName(link), entityTypeToString(tryGetType(link)), tryGetStructEntityName(T_E(getElem())));
      return ;
    }

  const std::string linkType = ((StructEntity *)link)->structName;
  if ("ActionLink" == linkType || "ActionLinkWithString"  == linkType)
    {
      va_list ap;
      
      va_start(ap, link);
      return handleActionLink(link, &ap);
    }
  else if ("IndirectLink" == linkType)
    {
      return handleIndirectCall(link);
    }
  else if ("DirectLink" == linkType)
    {
      return handleDirectCall(link);
    }
  else
    {
      DPRINT_ERR("the type of link %s in not handle\n", linkType.c_str());
    }
}

unsigned int Widget::getX() const
{
  return (_x);
}

unsigned int Widget::getY() const
{
  return (_y);
}

unsigned int Widget::getW() const
{
  return (_w);
}

unsigned int Widget::getH() const
{
  return (_h);
}

Widget    *Widget::tryFindElem(StructEntity *elemEntity)
{
  if (elemEntity == _elem)
    return (this);
  return (NULL);
}

int	Widget::getType() const
{
  return (_type);
}

void	Widget::setX(unsigned int x)
{
  _x = x;
}

void	Widget::setY(unsigned int y)
{
  _y = y;
}

void	Widget::setW(unsigned int w)
{
  _w = w;
}

void	Widget::setH(unsigned int h)
{
  _h = h;
}
